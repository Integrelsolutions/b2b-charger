#ifndef B2B_H
#define B2B_H

#define operating_voltage                           12
#define Lithium_12V_bulk_target_voltage             14.4
#define Lithium_12V_float_target_voltage            13.8
#define Lithium_12V_min_voltage                     11.4
#define Lithium_12V_max_voltage                     15
#define Lithium_12V_safe_fallback_voltage           12
#define fixed_12V_charge_voltage                    13.8
#define default_12V_safety_voltage                  12

#define Lithium_24V_bulk_target_voltage             28.8
#define Lithium_24V_float_target_voltage            27.5
#define Lithium_24V_min_voltage                     23
#define Lithium_24V_max_voltage                     30
#define Lithium_24V_safe_fallback_voltage           24
#define fixed_24V_charge_voltage                    27.5
#define default_24V_safety_voltage                  24


// I2C addresses
#define EUI48_address                   0b10100000
#define Hall_DC_address                 0b11010010
#define digital_pot_address             0b01011000
#define io_expander_address             0b10000010


#define input_voltage_channel                   0
#define output_voltage_channel                  1
#define DCDC_voltage_channel                    2
#define DCDC_temperature_1                      8
#define DCDC_temperature_2                      7
#define DCDC_temperature_3                      6
#define DCDC_temperature_4                      5
#define DCDC_temperature_5                      4
#define DCDC_temperature_6                      3


#define PGN_data_message                        0
#define PGN_current_parameter_message           1
#define PGN_new_parameter_message               2
#define PGN_instant_parameter_message           3


#define FRAM_log_start                          80
#define FRAM_log_end                            8191

#define bank_sensor_max                         8
#define B2B_max                                 4

#define bank_data_message_complete              63
#define bank_parameter_message_complete         4095
#define B2B_data_message_complete               63
#define B2B_parameter_message_complete          63
#define alarm_persistence_time                  10
#define thermistor_beta                         3960.0                      //Beta (K) of the thermistor
#define ambient_max                             80
#define ambient_min                             -10
#define emergency_fan_activation_temperature    50
#define emergency_fan_deactivation_temperature  48
#define historic_average                        0x04
#define DC_current_ratio_value                  217
#define bank_sensor_disconnect_timeout          60
#define slave_disconnect_timeout                30
#define boot_start                              0x400
#define DC_voltage_ratio                        51.2
#define default_voltage                         13.2
#define temperature_hysteresis                  10
//Ratios, averages and constants


// Default variables
#define FCY                             16000000UL                   //Internal LRC is PLL to 32MHz, 16MHz FCY is half oscillator frequency. Used by timers, delays and baud calculations

//Useful macros
#define delay_ms(A)                     __delay_ms(A)
#define delay_us(A)                     __delay_us(A)
#define check_bit(number,position)      ((number) & (1<<(position)))
#define hi_word(A)                      (((A) >> 8) & 0xFF)
#define lo_word(A)                      ((A) & 0xFF)
#define square(A)                       (A * A)
#define cube(A)                         (A * A * A)
#define fourth(A)                       (A * A * A * A)
#define fifth(A)                        (A * A * A * A * A)

//Pin assignments constants output


#define TQ_switch                       LATBbits.LATB10
#define LED_power                       LATBbits.LATB11

#define FRAM_select                     LATAbits.LATA2
#define Argo_select                     LATAbits.LATA3
#define BMS_select                      LATAbits.LATA10

#define fan_control                     LATCbits.LATC7
#define master_alarm_out                LATBbits.LATB15
#define shutdown_DC_1                   LATBbits.LATB5
#define shutdown_DC_2                   LATBbits.LATB6
#define shutdown_DC_3                   LATBbits.LATB7
#define shutdown_DC_4                   LATCbits.LATC5
#define shutdown_DC_5                   LATCbits.LATC4
#define shutdown_DC_6                   LATAbits.LATA9

#define bluetooth_power                 LATAbits.LATA7
#define RS485_direction                 LATAbits.LATA8


//Pin assignments constants input



#define master_alarm_in                 PORTBbits.RB15
#define slave_link_button               !PORTCbits.RC6       //Needs the pin updating for the new board
#define control_timer_trigger_flag      IFS0bits.T3IF
#define timer_trigger_flag              IFS1bits.T5IF
#define time_out_flag                   IFS0bits.T1IF

#define receive_timer_flag              IFS1bits.T5IF
//Structs

//CAN data structure
typedef struct {
	unsigned long CAN_PGN;
	unsigned char CAN_byte[9];
} CAN_data_packet;

//Battery monitor Field coil data structure
typedef struct {
    unsigned short int ID_number;
    unsigned char CAN_device_class;
    unsigned char last_message_received;
    unsigned short int complete_data_set;
    unsigned short int complete_parameter_set;
    
	double bank_voltage;
	double DC_current;
    unsigned char actual_SoC;
    unsigned char reference_SoC;
    unsigned short int time_to_empty;
    
    double block_temperature1;
    double block_temperature2;
    double block_temperature3;
    double block_temperature4;
    
    double block_voltage1;
    double block_voltage2;
    double block_voltage3;
    double block_voltage4; 
    
    double absorption_charge_voltage;
    double float_charge_voltage;
    double equalisation_voltage;
    unsigned char charge_mode;
    unsigned char cyclical_counter;
    unsigned char cyclical_counter_compare;
    unsigned short int sensor_time_out;
    
    unsigned short int time_till_float;
    unsigned char sensor_name_index;
    unsigned char updated_parameter_status;
    
    double firmware_version;
    unsigned char current_polarity;
    unsigned char alarm;
    //Parameters
    unsigned char use_pair_index;
    unsigned short int bank_Ah_capacity;
    unsigned char charge_efficiency;
    unsigned char discharge_efficiency;
    unsigned char battery_chemistry;
    double nominal_block_voltage;
    unsigned char rated_discharge_time;
    
    double absorption_voltage;
    double float_voltage;
    double Peukerts_constant;
    double temperature_compensation;

    unsigned char min_SoC;    
    unsigned short int lifetime_kWh;
    unsigned char time_before_OCV_reset;
    unsigned char max_time_at_absorption;   
    unsigned char max_bulk_current;    
    double full_charge_current;

    double min_datum_temperature;
    double max_datum_temperature;
    double OCV_current_limit;
    unsigned char pair_index;
    unsigned char number_of_blocks;
    
    double minimum_absorption_temperature;
    double maximum_absorption_temperature;
    double minimum_float_temperature;
    double maximum_float_temperature;
    
    double minimum_absorption_block_voltage;
    double maximum_absorption_block_voltage;
    double minimum_float_block_voltage;
    double maximum_float_block_voltage;
    
    double centre_block_voltage_trigger;
    double current_threshold_trigger;    
    
    double lifetime_kWh_in;
    double lifetime_kWh_out;
    double kWh_in_since_float;
    double kWh_out_since_float;
    unsigned char device_class_set;
    unsigned char old_device_class_set;

    double warning_voltage;
    double disconnect_voltage;
    double warning_SoC;
    double disconnect_SoC;

// Lithium
    unsigned short int battery_manufacturer;
    double Lithium_bulk_target_voltage;
    double Lithium_float_target_voltage;
    double Lithium_min_charge_temperature;
    double Lithium_max_charge_temperature;
    double Lithium_min_discharge_temperature;
    double Lithium_max_discharge_temperature;
    double Lithium_min_voltage;
    double Lithium_max_voltage;
    double Lithium_max_discharge_current;
    double Lithium_max_charge_current;
    double Lithium_cycle_reset_SoC;
    double Lithium_float_trip_accuracy;
    double Lithium_safe_fallback_voltage;
    unsigned short int battery_model;    
    
} battery_bank_data;

typedef struct {
    //Parameters
    unsigned char use_pair_index;
    unsigned short int ID_number;
    unsigned char sensor_name_index;
    unsigned short int complete_parameter_set;
    unsigned short int bank_Ah_capacity;
    unsigned char charge_efficiency;
    double discharge_efficiency;
    unsigned char battery_chemistry;
    double nominal_block_voltage;
    unsigned char rated_discharge_time;
    
    double absorption_voltage;
    double float_voltage;
    double Peukerts_constant;
    double temperature_compensation;

    unsigned char min_SoC;    
    unsigned short int lifetime_kWh;
    unsigned char time_before_OCV_reset;
    unsigned char max_time_at_absorption;   
    unsigned char max_bulk_current;    
    double full_charge_current;

    double min_datum_temperature;
    double max_datum_temperature;
    double OCV_current_limit;
    unsigned char pair_index;
    unsigned char number_of_blocks;
    
    double minimum_absorption_temperature;
    double maximum_absorption_temperature;
    double minimum_float_temperature;
    double maximum_float_temperature;
    
    double minimum_absorption_block_voltage;
    double maximum_absorption_block_voltage;
    double minimum_float_block_voltage;
    double maximum_float_block_voltage;
    
    double centre_block_voltage_trigger;
    double current_threshold_trigger; 
    unsigned char current_polarity;
    unsigned char set_zero;
    unsigned char reset_factory;
    unsigned char reset_Ah;
    unsigned char reset_kWh;
    unsigned char device_class_set;
    unsigned char old_device_class_set;
    
    double warning_voltage;
    double disconnect_voltage;
    double warning_SoC;
    double disconnect_SoC;

// Lithium
    unsigned short int battery_manufacturer;
    double Lithium_bulk_target_voltage;
    double Lithium_float_target_voltage;
    double Lithium_min_charge_temperature;
    double Lithium_max_charge_temperature;
    double Lithium_min_discharge_temperature;
    double Lithium_max_discharge_temperature;
    double Lithium_min_voltage;
    double Lithium_max_voltage;
    double Lithium_max_discharge_current;
    double Lithium_max_charge_current;
    double Lithium_cycle_reset_SoC;
    double Lithium_float_trip_accuracy;
    double Lithium_safe_fallback_voltage;
    unsigned short int battery_model;           
} battery_bank_parameters;

typedef struct {
    unsigned short int ID_number;
    unsigned char CAN_device_class;
    unsigned char last_message_received;
    unsigned short int complete_data_set;
    unsigned short int complete_parameter_set;    
    unsigned char cyclical_counter;
    unsigned char cyclical_counter_compare;    
    unsigned char updated_parameter_status;
    unsigned char parameter_complete_counter;
    
    double input_voltage;
    double actual_output_voltage;
    double highest_module_temperature;
    double combined_output_current;
    unsigned char number_of_modules;
    unsigned char alarm;   
    double master_DCDC_output_voltage;
    double slave1_DCDC_output_voltage;
    double slave2_DCDC_output_voltage;
    double slave3_DCDC_output_voltage;
    double slave4_DCDC_output_voltage;
    double master_max_chip_temperature;
    double slave1_max_chip_temperature;
    double slave2_max_chip_temperature;
    double slave3_max_chip_temperature;
    double slave4_max_chip_temperature;
    double master_module_current;
    double slave1_module_current;
    double slave2_module_current;
    double slave3_module_current;
    double slave4_module_current;    
    unsigned char name_index;
    unsigned char fan_active;
    double target_voltage;
    
    //Used but not in PGN messages
    double master_stack_current;
    double slave1_stack_current;
    double slave2_stack_current;
    double slave3_stack_current;
    double slave4_stack_current;       
    
    double module_output_current;
    double module_max_chip_temperature;
    double module_DCDC_voltage;
    
    double DCDC_temperature1;
    double DCDC_temperature2;
    double DCDC_temperature3;
    double DCDC_temperature4;
    double DCDC_temperature5;
    double DCDC_temperature6;
    
    unsigned short int digital_pot_setting;
    unsigned char master_DCDC_chips_enabled;
    unsigned char slave1_DCDC_chips_enabled;
    unsigned char slave2_DCDC_chips_enabled;
    unsigned char slave3_DCDC_chips_enabled;
    unsigned char slave4_DCDC_chips_enabled;
    unsigned char charge_mode;    
    unsigned short int firmware_version;

    unsigned char BMS_data_present;    
    double lowest_float_voltage;
    double lowest_absorption_voltage;
    unsigned char stop_charging;
    //parameters

    unsigned short int battery_manufacturer;
    unsigned short int battery_model;     
    double Lithium_bulk_target_voltage;
    double Lithium_float_target_voltage;
    double Lithium_min_charge_temperature;
    double Lithium_max_charge_temperature;
    double Lithium_min_discharge_temperature;
    double Lithium_max_discharge_temperature;
    double Lithium_min_voltage;
    double Lithium_max_voltage;
    double Lithium_max_discharge_current;
    double Lithium_max_charge_current;
    double Lithium_cycle_reset_SoC;
    double Lithium_float_trip_accuracy;
    double Lithium_safe_fallback_voltage;
    double fixed_charge_current;
    double fixed_charge_voltage;
    unsigned char low_SoC;
    unsigned char BMS_interface_to_use;
    double input_voltage_disconnect;
    unsigned char operation_mode;
    double default_safety_voltage;
    double maximum_chip_temperature;
    unsigned char sleep_mode;
} battery_to_battery_data;


//Parameters
typedef struct {
    unsigned short int ID_number;
    unsigned char CAN_device_class;
    unsigned short int complete_parameter_set;  
    unsigned char long_term_reset_required;
    unsigned short int battery_manufacturer;
    unsigned short int battery_model;     
    double Lithium_bulk_target_voltage;
    double Lithium_float_target_voltage;
    double Lithium_min_charge_temperature;
    double Lithium_max_charge_temperature;
    double Lithium_min_discharge_temperature;
    double Lithium_max_discharge_temperature;
    double Lithium_min_voltage;
    double Lithium_max_voltage;
    double Lithium_max_discharge_current;
    double Lithium_max_charge_current;
    double Lithium_cycle_reset_SoC;
    double Lithium_float_trip_accuracy;
    double Lithium_safe_fallback_voltage;
    double fixed_charge_current;
    double fixed_charge_voltage;
    unsigned char low_SoC;
    unsigned char BMS_interface_to_use;
    double input_voltage_disconnect;
    unsigned char name_index;
    unsigned char operation_mode;
    double default_safety_voltage;
    double maximum_chip_temperature;    
    unsigned char factory_reset;
    unsigned char stop_charging;
    unsigned char start_charging;
    unsigned char sleep_mode;
    unsigned short int parameter_set_complete;
    unsigned char parameter_complete_counter;
} battery_to_battery_parameters;

typedef struct {
    unsigned short int ID_number;
    unsigned char CAN_device_class;
    unsigned char last_message_received;
    unsigned char slave_module_position;
    unsigned char status;
    double maximum_DCDC_chip_temperature;
    unsigned char number_DCDC_chips_enabled;
    double DCDC_output_voltage;
    double stack_output_current;      
    unsigned char slave_timeout;    
    unsigned char cyclical_counter;
    unsigned char cyclical_counter_compare;
} slave_modules;

typedef struct {
    unsigned char stop_generator;
    unsigned char start_generator;
    unsigned char disconnect_battery_bank;
    unsigned char reconnect_battery_bank;
    unsigned char system_sleep;
    unsigned char pause_CAN_bus_activity;
    unsigned char resume_CAN_bus_activity;
    unsigned char reconnect_battery_bank_alarm_override;    
} panic_data;

//Procedure and function definitions
int main(void);
void n_second_delay (unsigned char length);
double FRAM_variable_double(unsigned int FRAM_location, double data_value, unsigned char write_enable, double default_value);
unsigned long long int FRAM_variable_RW(unsigned long FRAM_location, unsigned long long int data_size, unsigned long long int data_value, unsigned char write_enable, unsigned long long int defaults);
unsigned char FRAM_read_byte(unsigned int FRAM_address);
void FRAM_write_byte(unsigned int FRAM_address, unsigned char byte);
void check_virgin_FRAM(void);

void UART_init(unsigned long baud);

void RS485_send_char(unsigned char send_byte);
unsigned short int RS485_receive_char(unsigned short int timeout);
void RS485_send_string(const char *data_string, unsigned int length);
unsigned char RS485_receive_string(char *data_string, unsigned int length);
void PC_send_char(unsigned char send_byte);
unsigned short int PC_receive_char(unsigned short int timeout);
void PC_send_string(const char *data_string, unsigned int length);
unsigned char PC_receive_string(char *data_string, unsigned int length);



void CAN_select(unsigned char CAN_interface, unsigned char select_set);
void CAN_reset(unsigned char CAN_interface);

unsigned char CAN_buffer_check(unsigned char CAN_interface);
void CAN_buffer(unsigned char buffer_number, unsigned char CAN_interface);
void CAN_send(CAN_data_packet CAN_send_data, unsigned char CAN_interface);
void SPI_init (void);
unsigned char SPI_write(unsigned char data);
unsigned char SPI_read(void);


void I2C1_init(void);
void I2C1_start(void);
void I2C1_restart(void);
void I2C1_stop(void);
void I2C1_idle(void);
unsigned char I2C1_ackstatus(void);
void I2C1_ack(void);
void I2C1_notack(void);
void I2C1_write(unsigned char byte);
unsigned char I2C1_read(void);


void PIC_ADC_init(void);
double PIC_ADC_voltage_read(unsigned char channel);
double PIC_ADC_temperature_read(unsigned char channel);
double TEMP_read(unsigned char channel);

void timer_setup(double seconds);
void control_timer_setup(double seconds);

void B2B_init(void);
void Bluetooth_init(unsigned char power);
unsigned long long int EUI48_read(void);
void HTFS200_setup(void);

signed long HTFS200_sample(void);

double HTFS200_read(unsigned char rawflag);


void factory_reset(void);
void time_out_trap(void);

unsigned char incoming_Argo_link_poll(void);
unsigned char incoming_Argo_poll(void);

void CAN_link_process(CAN_data_packet CAN_incoming_data);
unsigned char CAN_data_process(CAN_data_packet CAN_incoming_data);

void extract_bank_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index);
void sensor_data_process(void);
void extract_B2B_new_parameters(CAN_data_packet PGN_parameter_extract, unsigned short int buffer_index);

void send_B2B_master_data(void);
void send_B2B_slave_data(void);
void send_B2B_parameters(void);

void load_FRAM_into_globals(void);
void send_B2B_slave_instant_parameter(void);
void copy_new_parameters(void);

void process_panic(CAN_data_packet PGN_data_extract);
void dig_pot_set(unsigned short int wiper_position, double voltage);
void setup_io_expander(void);
unsigned char read_io_expander(void);
void sample_and_write_current_zero_offset(void);
void voltage_control_loop(void);
void chip_set_power(unsigned char command);
void extract_slave_data(CAN_data_packet CAN_incoming_data, unsigned short int buffer_index);
void slave_data_process(void);
void collect_attached_modules(unsigned char role);
void set_volatile(volatile void *data, char fill, unsigned short int size);
void listen_for_commands(void);
void extract_host_B2B_data(unsigned char buffer_index);
void extract_B2B_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index);
unsigned char parse_command_string(char *data_string, unsigned int length);
unsigned short int extract_16bit_ID(char *data_string);
void process_bank_sensor_commands(char *data_string, unsigned short int ID);
double range_check(char *value_string, double minimum_value, double maximum_value, unsigned char test_set);
void send_bank_sensor_parameters(battery_bank_parameters new_parameters, unsigned short int PGN_ID);
void process_charger_commands(char *data_string, unsigned short int ID);
void send_charger_parameters(battery_to_battery_parameters new_parameters, unsigned short int PGN_ID);
void extract_slave_parameters(CAN_data_packet PGN_parameter_extract, unsigned short int buffer_index);
void extract_slave_instant_parameters(CAN_data_packet PGN_parameter_extract, unsigned short int buffer_index);
void send_test_serial();
void start_boot(uint16_t applicationAddress);

#endif
