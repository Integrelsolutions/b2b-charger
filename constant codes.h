#ifndef CONCODE_H
#define	CONCODE_H

//Device classes
#define fcoil_device_class                      13          
#define bank_48V_device_class                   14
#define LCD_device_class                        15
#define sentinel_device_class                   16
#define bank_24V_device_class                   17
#define bank_12V_device_class                   18
#define BPG_device_class                        19
#define panic_device_class                      20
#define B2B_master_12V_device_class             22
#define B2B_master_24V_device_class             23
#define B2B_slave_12V_device_class              26
#define B2B_slave_24V_device_class              27

//Operation
#define OP_FIXED_VOLTAGE                0
#define OP_FIXED_CURRENT                1
#define OP_BANK_SENSOR                  2
#define OP_BMS_CONTROL                  3


//Module commands
#define MOD_SLAVE                       0
#define MOD_MASTER                      1

//UART comms
#define UART_OK                         0
#define UART_MODULE_COUNT               1
#define UART_MODULE_ENABLE_MODE         2
#define UART_MODULE_STATUS              3
#define UART_MODULE_SLEEP               4

//DC/DC chips
#define DCDC_ALL_OFF                    0
#define DCDC_ALL_ON                     1

//Charge modes
#define CHARGE_NONE                     0
#define CHARGE_ABSORPTION               1
#define CHARGE_FLOAT                    2
#define CHARGE_EQUAL                    3
#define CHARGE_ERROR                    255

//Result codes for panic messages
#define PANIC_CAN_STOP                  0
#define PANIC_CAN_START                 1
#define PANIC_SLEEP                     2
#define PANIC_WAKE                      3

//Current sampling
#define NO_VALID_DATA                   0
#define NEW_DATA_AVAILABLE              1

//SPI bus speed constants
#define SPI_FAST                        0
#define SPI_NORMAL                      1
#define SPI_SLOW                        2
#define SPI_MAX                         3

//Result codes for "CAN_buffer_check"
#define CAN_BUFFER_EMPTY                0
#define CAN_BUFFER0_FULL                1
#define CAN_BUFFER1_FULL                2
#define CAN_BUFFER0_1_FULL              3

//Result codes for "CAN_interval_accumulator
#define CAN_ACC_UPDATED                 0
#define CAN_ACC_ADDED                   1
#define CAN_ACC_FULL                    2
#define CAN_ACC_INVALID_DATA            3
#define CAN_ACC_SUCCESS                 4

//Result codes for serial streams
#define STRING_BUFFER_EMPTY             256
#define STRING_BUFFER_FULL              1

// CAN available
#define Argo_CAN                        0
#define J1939_CAN                       1
#define Lithium_CAN                     2

//Battery chemistry list
#define CHEM_LION                       0
#define CHEM_AGM                        1
#define CHEM_FLOOD                      2
#define CHEM_GEL                        3
#define CHEM_CF_AGM                     4
#define CHEM_TPPL_AGM                   5

#define UART_RS232                      1
#define UART_RS485                      2

#define INTERFACE_NONE                  0
#define INTERFACE_CAN                   1
#define INTERFACE_RS232                 2
#define INTERFACE_RS485                 3
#define INTERFACE_IO                    4

#define VICTRON                         1
    #define SMART_256V                  1

#define TORQEEDO                        2
    #define POWER26_104                 1
    #define POWER3500                   2

#define LITHIONICS                      3
    #define NEVERDIE                    1

#define GENZ                            4
#define MASTERVOLT                      5
#define SUPERB                          6

#define CURRENT_MIN                     0
#define CURRENT_MAX                     1

#define CHARGE_START                    0
#define CHARGE_STOP                     1


#define ALARM_NONE                      0



#endif	

