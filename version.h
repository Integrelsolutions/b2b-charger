#ifndef VERSION_H
#define	VERSION_H


// ____  _  _  ____  ____  ___  ____  ____  __   
//(_  _)( \( )(_  _)( ___)/ __)(  _ \( ___)(  )  
// _)(_  )  (   )(   )__)( (_-. )   / )__)  )(__ 
//(____)(_)\_) (__) (____)\___/(_)\_)(____)(____)

//HW1.002
//5V to hall effect missing. 5V to AMIS3306 chip missing on slave devices


//Firmware version. Update this every time a new version control entry is added. This data is displayed on the LCD.
// To fix unresolved identifiers delete the contents of this folder: \AppData\Local\mplab_ide\Cache\dev\v3.00\var
#define firmware_revision 1001

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 14:40 21/07/20
// Firmware version: 1.001
// Major addition: Bug fixes to PC interface and voltage control loop
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 14:40 18/11/19
// Firmware version: 1.000
// Major addition: Start based on BPG code
// Notes: None
//
//****************************************************************************************************************************



#endif	

