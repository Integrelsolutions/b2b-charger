#ifndef FRAM_VARIABLES_H
#define FRAM_VARIABLES_H

// FRAM location macros, A=data value, B= 0-read, 1-write, 2-restore default

#define FRAM_virgin(A,B)                                   FRAM_variable_RW(0,1,A,B,0)
// FRAM location 1 is reserved

#define FRAM_battery_manufacturer(A,B)                            FRAM_variable_RW(2,2,A,B,0)
#define FRAM_battery_model(A,B)                                   FRAM_variable_RW(4,2,A,B,0)

#define FRAM_Lithium_bulk_target_voltage(A,B)                     FRAM_variable_double(6,A,B,14.4) //59.0 Torqeedo //57.8 Victron
#define FRAM_Lithium_float_target_voltage(A,B)                    FRAM_variable_double(10,A,B,13.8) //55.8 Torqeedo // 55 Victron
#define FRAM_Lithium_min_charge_temperature(A,B)                  FRAM_variable_double(14,A,B,0.0)
#define FRAM_Lithium_max_charge_temperature(A,B)                  FRAM_variable_double(18,A,B,45.0)
#define FRAM_Lithium_min_discharge_temperature(A,B)               FRAM_variable_double(22,A,B,-10.0)
#define FRAM_Lithium_max_discharge_temperature(A,B)               FRAM_variable_double(26,A,B,55.0)
#define FRAM_Lithium_min_voltage(A,B)                             FRAM_variable_double(30,A,B,11.2)
#define FRAM_Lithium_max_voltage(A,B)                             FRAM_variable_double(34,A,B,15)
#define FRAM_Lithium_max_discharge_current(A,B)                   FRAM_variable_double(38,A,B,300.0)
#define FRAM_Lithium_max_charge_current(A,B)                      FRAM_variable_double(42,A,B,170.0)
#define FRAM_Lithium_cycle_reset_SoC(A,B)                         FRAM_variable_double(46,A,B,90.0)
#define FRAM_Lithium_float_trip_accuracy(A,B)                     FRAM_variable_double(50,A,B,99.7)
#define FRAM_Lithium_safe_fallback_voltage(A,B)                   FRAM_variable_double(54,A,B,12)
#define FRAM_fixed_charge_current(A,B)                            FRAM_variable_double(58,A,B,20)
#define FRAM_fixed_charge_voltage(A,B)                            FRAM_variable_double(62,A,B,13.8)
#define FRAM_low_SoC(A,B)                                         FRAM_variable_RW(66,1,A,B,0)  
#define FRAM_BMS_interface_to_use(A,B)                            FRAM_variable_RW(67,1,A,B,0)  
#define FRAM_input_voltage_disconnect(A,B)                        FRAM_variable_double(68,A,B,44.0)
#define FRAM_name_index(A,B)                                      FRAM_variable_RW(72,1,A,B,0)  
#define FRAM_operation_mode(A,B)                                  FRAM_variable_RW(73,1,A,B,0)  
#define FRAM_default_safety_voltage(A,B)                          FRAM_variable_double(74,A,B,13.8)
#define FRAM_maximum_chip_temperature(A,B)                        FRAM_variable_double(78,A,B,90)  

//81 -> 128 reserved


//Not parameters but internal use

#define FRAM_current_zero_offset(A,B)                             FRAM_variable_RW(128,4,A,B,0)  
#define FRAM_slave1_ID(A,B)                                       FRAM_variable_RW(132,2,A,B,0)
#define FRAM_slave2_ID(A,B)                                       FRAM_variable_RW(134,2,A,B,0)
#define FRAM_slave3_ID(A,B)                                       FRAM_variable_RW(136,2,A,B,0)
#define FRAM_slave4_ID(A,B)                                       FRAM_variable_RW(138,2,A,B,0)
#define FRAM_number_slaves_attached(A,B)                          FRAM_variable_RW(140,1,A,B,0)

#define FRAM_unique_ID(A,B)                                       FRAM_variable_RW(141,2,A,B,0)
#define FRAM_update_active(A,B)                                   FRAM_variable_RW(143,1,A,B,0)
//256 bytes is the start of storage space for logging data

#endif
