/*
Battery protection gateway HV1.000
Author: W.G
Company: Triskel Marine Ltd
Date: 01/03/19
*/ 

//Compiler directives
#include <p24FJ128GA204.h>

#include "xc.h"
#include "B2B.h"
#include "FRAM_variables.h"
#include "constant_codes.h"
#include "commands.h"
#include "Version.h"
#include "math.h"
#include "libpic30.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
//config

// CONFIG4
#pragma config DSWDTPS = DSWDTPS1F      // Deep Sleep Watchdog Timer Postscale Select bits (1:68719476736 (25.7 Days))
#pragma config DSWDTOSC = LPRC          // DSWDT Reference Clock Select (DSWDT uses LPRC as reference clock)
#pragma config DSBOREN = ON             // Deep Sleep BOR Enable bit (DSBOR Enabled)
#pragma config DSWDTEN = ON             // Deep Sleep Watchdog Timer Enable (DSWDT Enabled)
#pragma config DSSWEN = ON              // DSEN Bit Enable (Deep Sleep is controlled by the register bit DSEN)
#pragma config PLLDIV = PLL4X           // USB 96 MHz PLL Prescaler Select bits (PLL x4)
#pragma config I2C1SEL = DISABLE        // Alternate I2C1 enable bit (I2C1 uses SCL1 and SDA1 pins)
#pragma config IOL1WAY = ON             // PPS IOLOCK Set Only Once Enable bit (Once set, the IOLOCK bit cannot be cleared)

// CONFIG3
#pragma config WPFP = WPFP127           // Write Protection Flash Page Segment Boundary (Page 127 (0x1FC00))
#pragma config SOSCSEL = OFF             // SOSC Selection bits (SOSC circuit selected)
#pragma config WDTWIN = PS25_0          // Window Mode Watchdog Timer Window Width Select (Watch Dog Timer Window Width is 25 percent)
#pragma config PLLSS = PLL_PRI          // PLL Secondary Selection Configuration bit (PLL is fed by the Primary oscillator)
#pragma config BOREN = ON               // Brown-out Reset Enable (Brown-out Reset Enable)
#pragma config WPDIS = WPDIS            // Segment Write Protection Disable (Disabled)
#pragma config WPCFG = WPCFGDIS         // Write Protect Configuration Page Select (Disabled)
#pragma config WPEND = WPENDMEM         // Segment Write Protection End Page Select (Write Protect from WPFP to the last page of memory)

// CONFIG2
#pragma config POSCMD = NONE            // Primary Oscillator Select (Primary Oscillator Disabled)
#pragma config WDTCLK = LPRC            // WDT Clock Source Select bits (WDT uses LPRC)
#pragma config OSCIOFCN = ON           // OSCO Pin Configuration (OSCO/CLKO/RA3 functions as CLKO (FOSC/2))
#pragma config FCKSM = CSDCMD           // Clock Switching and Fail-Safe Clock Monitor Configuration bits (Clock switching and Fail-Safe Clock Monitor are disabled)
#pragma config FNOSC = FRCPLL           // Initial Oscillator Select (Fast RC Oscillator with Postscaler (FRCDIV))
#pragma config ALTCMPI = CxINC_RB       // Alternate Comparator Input bit (C1INC is on RB13, C2INC is on RB9 and C3INC is on RA0)
#pragma config WDTCMX = WDTCLK          // WDT Clock Source Select bits (WDT clock source is determined by the WDTCLK Configuration bits)
#pragma config IESO = ON                // Internal External Switchover (Enabled)

// CONFIG1
#pragma config WDTPS = PS32768          // Watchdog Timer Postscaler Select (1:32,768)
#pragma config FWPSA = PR128            // WDT Prescaler Ratio Select (1:128)
#pragma config WINDIS = OFF             // Windowed WDT Disable (Standard Watchdog Timer)
#pragma config FWDTEN = OFF             // Watchdog Timer Disable (WDT enabled in hardware)
#pragma config ICS = PGx2               // Emulator Pin Placement Select bits (Emulator functions are shared with PGEC1/PGED1)
#pragma config LPCFG = OFF              // Low power regulator control (Disabled - regardless of RETEN)
#pragma config GWRP = OFF               // General Segment Write Protect (Write to program memory allowed)
#pragma config GCP = OFF                 // General Segment Code Protect (Code protection is enabled)
#pragma config JTAGEN = OFF             // JTAG Port Enable (Enabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.



//Variable declaration
unsigned long long int EUI48;                           //EUI48 number
unsigned short int unique_16bit_ID;                     //16bit unique ID for PGN field
unsigned char updated_parameter_status, sleep_mode, alarm_condition, master_module, system_shutdown, low_voltage_shutdown;
signed long DC_current_zero_offset;      
unsigned char device_class, slave_device_class, stop_charging;
unsigned short int power_save_mode;
unsigned long current_zero_offset;
unsigned char module_number, module_status;
signed char current_polarity;
double last_DC_current[historic_average];
unsigned char DC_current_average_loop;
unsigned short int master_ID;


volatile double input_voltage, DCDC_voltage, output_voltage;
volatile unsigned char interrupt_currently_running;
volatile unsigned char check_for_valid_data;
volatile short int current_average_loop, digital_pot_value;
unsigned short int slaves_attached[5];
unsigned char number_of_slaves_attached;
CAN_data_packet CAN_buffer_store[2][2], CAN_send_packet;                    //CAN data storage for the two onboard MCP2515 buffers
slave_modules slave_data[5];
battery_bank_data bank_data[bank_sensor_max];
battery_bank_parameters bank_temp_storage[bank_sensor_max];
battery_to_battery_parameters B2B_temp_storage[B2B_max];
battery_to_battery_data B2B_local_data;
battery_to_battery_data B2B_data[B2B_max];
battery_to_battery_parameters new_parameters;
panic_data panic_commands;
//--------------------------------------------
// Main loop
// Entry: None
// Exit: Never
//--------------------------------------------
int main(void)
{    
    double current_check;
    
    unsigned char buffer_state, PC_response;
    unsigned char new_parameters_received, current_parameter_counter;
    unsigned char updated_parameter_status;
    unsigned long truncate;
    double max_temperature;
    char serial_data;
    char buffer_string[16];
    
        CLKDIVbits.RCDIV = 0;
        CLKDIVbits.DOZEN = 0;
                                                                //Turn off un-used PIC peripherals
        AD1CON1bits.ADON = 0;                                   //All analogue inputs off (they're on by default)                            
        AD1CSSL = 0;
        ANSA = 0x00;
        ANSB = 0x00;
        ANSC = 0x00;
        LATA = 0;                                               //Latches off
        LATB = 0;
        LATC = 0;
        //CNPU2bits.CN24PUE = 1;
        //CNPU2bits.CN23PUE = 1;        
                                                                //Chip pin setup and descriptions
        TRISBbits.TRISB9 = 0;                                   // 1. SDA 1  (out)
        TRISCbits.TRISC6 = 1;                                   // 2. Reset switch (in)
        TRISCbits.TRISC7 = 0;                                   // 3. Fan control (out)
        TRISCbits.TRISC8 = 1;                                   // 4. Bluetooth RX (in)
        TRISCbits.TRISC9 = 0;                                   // 5. Bluetooth TX (out)
                                                                // 6. Vbat
                                                                // 7. Vcap
        TRISBbits.TRISB10 = 0;                                  // 8. PGD2/TQ on/off (out)
        TRISBbits.TRISB11 = 0;                                  // 9. PGC2/Power LED (out)
        TRISBbits.TRISB12 = 1;                                  // 10. DC/DC temp sense 1 ANA8 (in)
        TRISBbits.TRISB13 = 1;                                  // 11. DC/DC temp sense 2 ANA7 (in)
        TRISAbits.TRISA10 = 0;                                  // 12. BMS CAN CS (out)
        TRISAbits.TRISA7 = 0;                                   // 13. Bluetooth power (out)
        TRISBbits.TRISB14 = 1;                                  // 14. DC/DC temp sense 3 ANA6 (in)
        TRISBbits.TRISB15 = 1;                                  // 15. Master alarm i/o (in)
                                                                // 16. AVss
                                                                // 17. AVdd
                                                                // 18. MCLR
        TRISAbits.TRISA0 = 1;                                   // 19. Input voltage ANA0 (in)
        TRISAbits.TRISA1 = 1;                                   // 20. Output voltage ANA1 (in)
        TRISBbits.TRISB0 = 1;                                   // 21. DC/DC voltage ANA2 (in)
        TRISBbits.TRISB1 = 1;                                   // 22. DC/DC temp sense 6 ANA3 (in)
        TRISBbits.TRISB2 = 1;                                   // 23. DC/DC temp sense 5 ANA4 (in)
        TRISBbits.TRISB3 = 1;                                   // 24. DC/DC temp sense 4 ANA5 (in)
        TRISCbits.TRISC0 = 0;                                   // 25. MOSI (out)
        TRISCbits.TRISC1 = 0;                                   // 26. MCLK (out)
        TRISCbits.TRISC2 = 1;                                   // 27. MISO (in)
                                                                // 28. Vdd
                                                                // 29. Vss
        TRISAbits.TRISA2 = 0;                                   // 30. FRAM select (out)    
        TRISAbits.TRISA3 = 0;                                   // 31. Argo CAN select (out)
        TRISAbits.TRISA8 = 0;                                   // 32. RS485 direction (out)
        //TRISBbits.TRISB4 = 1;                                   // 33. RS485 RX RPI4 (in)
        //TRISAbits.TRISA4 = 1;                                   // 34. N/C (in)
        TRISAbits.TRISA9 = 0;                                   // 35. Shut down DC/DC 6 (out)
        TRISCbits.TRISC3 = 0;                                   // 36. RS485 TX RP19 (out)
        TRISCbits.TRISC4 = 0;                                   // 37. Shutdown DC/DC 5 (out)
        TRISCbits.TRISC5 = 0;                                   // 38. Shutdown DC/DC 4 (out)
                                                                // 39. Vss
                                                                // 40. Vdd
        TRISBbits.TRISB5 = 0;                                   // 41. Shutdown DC/DC 1 (out)
        TRISBbits.TRISB6 = 0;                                   // 42. Shutdown DC/DC 2 (out)
        TRISBbits.TRISB7 = 0;                                   // 43. Shutdown DC/DC 3 (out)
        TRISBbits.TRISB8 = 0;                                   // 44. SCL 1 (out)

        shutdown_DC_1 = 0;
        shutdown_DC_2 = 0;
        shutdown_DC_3 = 0;
        shutdown_DC_4 = 0;
        shutdown_DC_5 = 0;
        shutdown_DC_6 = 0;         
        
        //Set up PIC peripherals we are using
        __builtin_write_OSCCONL(OSCCON & 0xBF);                 //Unlock to allow pin changes

            RPINR18bits.U1RXR = 4;					//Maps RS485 UART1 RX to RP4 (Pin 33 RX)
            RPOR9bits.RP19R = 3;             		//Maps RS485 UART1 TX to RP19 (Pin 36 TX)
            
            RPINR19bits.U2RXR = 24;					//Maps Bluetooth UART2 RX to RP24 (Pin 4 RX)
            RPOR12bits.RP25R = 5;             		//Maps Bluetooth UART2 TX to RP25 (Pin 5 TX)
            

            RPINR20bits.SDI1R = 18;					//Maps SPI1 SDI to RP18 (Pin 27 SDI)
            RPOR8bits.RP16R = 7;					//Maps SPI1 SDO to RP16 (Pin 25 SDO)
            RPOR8bits.RP17R = 8;                    //Maps SPI1 SCK to RP17 (Pin 26 SCK)
           
            
                        
        __builtin_write_OSCCONL(OSCCON & 0x40);                 //Lock to protect pin changes        
        

        B2B_init();

        memset(&bank_data,0,sizeof(bank_data));
        memset(&B2B_local_data,0,sizeof(B2B_local_data));
        memset(&B2B_data,0,sizeof(B2B_data));
        load_FRAM_into_globals();
        B2B_local_data.ID_number = unique_16bit_ID;
        current_parameter_counter = 0;
        sleep_mode = 0;
        digital_pot_value = 1023;
        current_check = 0;
        stop_charging = 0;
        new_parameters_received = 0;
        updated_parameter_status = 0;
        master_ID = 0;
        alarm_condition = ALARM_NONE;
        sample_and_write_current_zero_offset();
       
        //Take set zero reading with all DC/DC off
        //Set default voltage
        //Start voltage loop
        //harvest bank sensor / Battery BMS data
        //Set charge voltage
        //Send data
        //Check for parameters
        
        //Interrupt
        // Too hot, derate the voltage
        // Too much current, derate the voltage until it drops
        // Master current is a combination of slaves above, need to count and see if their spread evenly


        dig_pot_set(1000, 0);
        shutdown_DC_1 = 1;
        shutdown_DC_2 = 1;
        shutdown_DC_3 = 1;
        shutdown_DC_4 = 1;
        shutdown_DC_5 = 1;
        shutdown_DC_6 = 1;
        strcpy(buffer_string,"\fReady...\r\n");        
        PC_send_string(buffer_string, strlen(buffer_string)); 

        PC_response = U2RXREG;
        PC_response = U2RXREG;
        PC_response = U2RXREG;
//        factory_reset();
        timer_setup(1);                       
        control_timer_setup(0.1);        
        Nop();
        while(1) 
        {            
            //Harvest running data
            B2B_local_data.module_DCDC_voltage = PIC_ADC_voltage_read(DCDC_voltage_channel);
            B2B_local_data.input_voltage = PIC_ADC_voltage_read(input_voltage_channel);
            B2B_local_data.actual_output_voltage = PIC_ADC_voltage_read(output_voltage_channel);     
            
            truncate = B2B_local_data.actual_output_voltage * 10;
            B2B_local_data.actual_output_voltage = (double)truncate / 10;

            truncate = B2B_local_data.input_voltage * 10;
            B2B_local_data.input_voltage = (double)truncate / 10;

            truncate = B2B_local_data.module_DCDC_voltage * 10;
            B2B_local_data.module_DCDC_voltage = (double)truncate / 10;

            
            Nop();
            
            current_check = HTFS200_read(0);
            if (check_for_valid_data != NO_VALID_DATA) B2B_local_data.combined_output_current = current_check;
            max_temperature = -40;
            B2B_local_data.DCDC_temperature1 = PIC_ADC_temperature_read(DCDC_temperature_1);            
            B2B_local_data.DCDC_temperature2 = PIC_ADC_temperature_read(DCDC_temperature_2);            
            B2B_local_data.DCDC_temperature3 = PIC_ADC_temperature_read(DCDC_temperature_3);            
            B2B_local_data.DCDC_temperature4 = PIC_ADC_temperature_read(DCDC_temperature_4);            
            B2B_local_data.DCDC_temperature5 = PIC_ADC_temperature_read(DCDC_temperature_5);            
            B2B_local_data.DCDC_temperature6 = PIC_ADC_temperature_read(DCDC_temperature_6);            
            if (B2B_local_data.DCDC_temperature1 > max_temperature) max_temperature = B2B_local_data.DCDC_temperature1;
            if (B2B_local_data.DCDC_temperature2 > max_temperature) max_temperature = B2B_local_data.DCDC_temperature2;
            if (B2B_local_data.DCDC_temperature3 > max_temperature) max_temperature = B2B_local_data.DCDC_temperature3;
            if (B2B_local_data.DCDC_temperature4 > max_temperature) max_temperature = B2B_local_data.DCDC_temperature4;
            if (B2B_local_data.DCDC_temperature5 > max_temperature) max_temperature = B2B_local_data.DCDC_temperature5;
            if (B2B_local_data.DCDC_temperature6 > max_temperature) max_temperature = B2B_local_data.DCDC_temperature6;
            B2B_local_data.module_max_chip_temperature = max_temperature;
            
            if (master_module) 
            {
                B2B_local_data.master_max_chip_temperature = B2B_local_data.module_max_chip_temperature;
                B2B_local_data.master_DCDC_output_voltage = B2B_local_data.module_DCDC_voltage;
                if (number_of_slaves_attached == 0) 
                {
                    B2B_local_data.module_output_current = B2B_local_data.combined_output_current;
                    B2B_local_data.highest_module_temperature = B2B_local_data.module_max_chip_temperature;                    
                } //Highest temperature across all modules needs to be worked out
                if (number_of_slaves_attached == 1) 
                {
                    B2B_local_data.module_output_current = (B2B_local_data.combined_output_current / 2);
                    B2B_local_data.highest_module_temperature = B2B_local_data.module_max_chip_temperature;                    
                } //Highest temperature across all modules needs to be worked out
                
            } else {   
                if ((number_of_slaves_attached == 0) && (master_module == 0)) // We are the only slave 1
                {
                    B2B_local_data.module_output_current = B2B_local_data.combined_output_current;
                }            
            }
            
            buffer_state = incoming_Argo_poll(); //Check for bank data and parameters                                 
            voltage_control_loop();
         
            //if ((slave_link_button) && (master_module)) collect_attached_modules(MOD_MASTER);                              
            //if ((master_alarm_in) && (master_module == 0)) collect_attached_modules(MOD_SLAVE);
  
            //1 second loop starts here   
            if (timer_trigger_flag)
            {               
                timer_setup(1);               
                sensor_data_process();
                //PC_send_char('@');
                if (new_parameters.complete_parameter_set == B2B_parameter_message_complete)
                {   
                    if (B2B_local_data.updated_parameter_status == 0) copy_new_parameters();
                    B2B_local_data.updated_parameter_status = 1;
                    B2B_local_data.parameter_complete_counter++;
                }
                if ((new_parameters.complete_parameter_set == B2B_parameter_message_complete) && (B2B_local_data.parameter_complete_counter == 10))
                {
                    //update FRAM and globals with new_parameters;

                    B2B_local_data.updated_parameter_status = 0;
                    B2B_local_data.parameter_complete_counter = 0;
                    memset(&new_parameters,0,sizeof(new_parameters));
                }
                if ((new_parameters.complete_parameter_set != 0) && (new_parameters.complete_parameter_set != B2B_parameter_message_complete))
                {
                   
                    timer_setup(15);
                    do {
                        incoming_Argo_poll();                                        
                    } while ((timer_trigger_flag == 0) && (new_parameters.complete_parameter_set != B2B_parameter_message_complete));
                  
                    if ((timer_trigger_flag) && (new_parameters.complete_parameter_set != B2B_parameter_message_complete))
                    {
                        
                        new_parameters.complete_parameter_set = 0;
                        memset(&new_parameters,0,sizeof(new_parameters));
                    }
                    timer_setup(1);
                }                   
                
                if (alarm_condition != ALARM_NONE) panic_commands.system_sleep = 0;
                
                if (panic_commands.system_sleep == 0)
                {
                    if (master_module)
                    {
                        send_B2B_master_data();    
                        send_B2B_slave_instant_parameter();
                    } else {                       
                        send_B2B_slave_data();               
                    }
                }
                
                current_parameter_counter++;
                if (current_parameter_counter == 10)
                {
                    if ((panic_commands.system_sleep == 0) && (master_module)) send_B2B_parameters();
                    current_parameter_counter = 0;
                }
                if ((U2STAbits.URXDA != 0) && (master_module))
                {
                    serial_data = PC_receive_char(0);
                    if (serial_data == '*') listen_for_commands();
                }
                send_test_serial();
            }                                                                                          
        }
}


//************************* FRAM routines *************************

//------------------------------------
// Read from FRAM
// Entry: FRAM address to read
// Exit: Contents at the FRAM address
//------------------------------------
unsigned char FRAM_read_byte(unsigned int FRAM_address)
{
    unsigned char general;
    union {
        unsigned long address_long;
        unsigned char address_bytes[4];
    } FRAM_local_address;

        FRAM_local_address.address_long = FRAM_address;
        FRAM_select = 0;
        SPI_write(0x03);
        SPI_write(FRAM_local_address.address_bytes[1]);
        SPI_write(FRAM_local_address.address_bytes[0]);
        general = SPI_read();
        FRAM_select = 1;

        return (general);	
}

//---------------------------------------------------------
// Write to FRAM (one byte)
// Entry: FRAM address to write and the byte to be written
// Exit: None
//---------------------------------------------------------
void FRAM_write_byte(unsigned int FRAM_address, unsigned char byte)
{
   union {
      unsigned long address_long;
      unsigned char address_bytes[4];
   } FRAM_local_address;

        FRAM_local_address.address_long = FRAM_address;
        FRAM_select = 0;
        SPI_write(0x06);
        FRAM_select = 1;

        FRAM_select = 0;
        SPI_write(0x02);
        SPI_write(FRAM_local_address.address_bytes[1]);
        SPI_write(FRAM_local_address.address_bytes[0]);
        SPI_write(byte);
        FRAM_select = 1;
}


//-----------------------------------------------------------------------------------------
// Retrieve data from specified FRAM address
// Entry: FRAM variable location, length of data, data to be written if any, write or read)
// Exit: Data at the location / Data written
//-----------------------------------------------------------------------------------------
double FRAM_variable_double(unsigned int FRAM_location, double data_value, unsigned char write_enable, double default_value)
{
    unsigned int h;
    union {
        double variable_32bit;
	unsigned char variable_bytes[4];
    } FRAM_variable;

        FRAM_variable.variable_32bit = 0;

        if (write_enable > 0)
        {
            if (write_enable == 1) FRAM_variable.variable_32bit = data_value;
            if (write_enable == 2) FRAM_variable.variable_32bit = default_value;
            for (h = 0; h < 4; h++)
            {
                FRAM_write_byte(FRAM_location,FRAM_variable.variable_bytes[h]);
                FRAM_location++;
            }
        } else {
            for (h = 0; h < 4; h++)
            {
                FRAM_variable.variable_bytes[h] = FRAM_read_byte(FRAM_location);
                FRAM_location++;
            }
        }

        return FRAM_variable.variable_32bit;
}


//------------------------------------------------------------------------------------------
// Retrieve data from specified FRAM address
// Entry: FRAM variable location, length of data, data to be written if any, write or read)
// Exit: Data at the location / Data written
//------------------------------------------------------------------------------------------
unsigned long long int FRAM_variable_RW(unsigned long FRAM_location, unsigned long long int data_size, unsigned long long int data_value, unsigned char write_enable, unsigned long long int defaults)
{
    unsigned long long int h;
    union {
        unsigned long long int variable_64bit;
        unsigned char variable_bytes[8];
    } FRAM_variable;

        FRAM_variable.variable_64bit = 0;

        if (write_enable>0)
        {
            if (write_enable == 1) FRAM_variable.variable_64bit = data_value;
            if (write_enable == 2) FRAM_variable.variable_64bit = defaults;

            for (h = 0; h < data_size; h++)
            {
                FRAM_write_byte(FRAM_location,FRAM_variable.variable_bytes[h]);
                FRAM_location++;
            }
        } else {

            for (h = 0; h < data_size; h++)
            {
                FRAM_variable.variable_bytes[h] = FRAM_read_byte(FRAM_location);
                FRAM_location++;
            }
        }

        return FRAM_variable.variable_64bit;
}


//------------------------------------------------------------------------------
// Reset Ah, SoC and current offset if this is the first time the board has run
// Entry: None
// Exit: None
//------------------------------------------------------------------------------
void check_virgin_FRAM(void)
{
	unsigned char virgin_board;

        virgin_board = FRAM_virgin(0,0);

        if (virgin_board != 0x77)
        {
                factory_reset();
                FRAM_virgin(0x77,1);
        }        
}



//------------------
// Setup 2x UART
// Entry: Baud rate
// Exit: None
//------------------
void UART_init(unsigned long baud)
{
        U1MODEbits.UARTEN = 0;					//Disable UART1
        U1BRG = (FCY / (4 * baud)) - 1;    		//115200bps
        U1MODEbits.USIDL = 0;					//Continue in idle mode
        U1MODEbits.IREN = 0;					//IrDA disabled
        U1MODEbits.UEN = 0b00;					//RX and TX rest not used
        U1MODEbits.ABAUD = 0;					//Auto baud off
        U1MODEbits.RXINV = 0;					//RX idle state is high
        U1MODEbits.BRGH = 1;					//BRGH is 4 cycles per bit
        U1MODEbits.PDSEL = 0b00;				//8bit no parity
        U1MODEbits.STSEL = 0b00;				//1 stop bit
        U1STA = 0;
        U1MODEbits.UARTEN = 1;					//Enable UART1
        U1STAbits.UTXEN = 1;					//TX enabled

        baud = 115200;
        
        U2MODEbits.UARTEN = 0;					//Disable UART2
        U2BRG = (FCY / (4 * baud)) - 1;    		//9600bps
        U2MODEbits.USIDL = 0;					//Continue in idle mode
        U2MODEbits.IREN = 0;					//IrDA disabled
        U2MODEbits.UEN = 0b00;					//RX and TX rest not used
        U2MODEbits.ABAUD = 0;					//Auto baud off
        U2MODEbits.RXINV = 0;					//RX idle state is high
        U2MODEbits.BRGH = 1;					//BRGH is 4 cycles per bit
        U2MODEbits.PDSEL = 0b00;				//8bit no parity
        U2MODEbits.STSEL = 0b00;				//1 stop bit
        U2STA = 0;
        U2MODEbits.UARTEN = 1;					//Enable UART2
        U2STAbits.UTXEN = 1;					//TX enabled        
        
        
}



//-----------------------
// Send a byte to the PC
// Entry: Byte to send
// Exit: None
//-----------------------
void RS485_send_char(unsigned char send_byte)
{
        while (U1STAbits.UTXBF == 1);
        U1TXREG = send_byte;
        RS485_receive_char(0);
}


//------------------------------------------
// Receive a byte from the PC
// Entry: UART timeout in seconds
// Exit: Char received or 256 if a time out
//------------------------------------------
unsigned short int RS485_receive_char(unsigned short int timeout)
{
	unsigned short int return_value;

        return_value = 256;

        if (U1STAbits.OERR == 1)
        {
            U1STAbits.OERR = 0;
            return_value = U1RXREG;
            return_value = U1RXREG;
            return_value = U1RXREG;
        }
        if ((U1STAbits.FERR == 1) || (U1STAbits.PERR == 1)) return_value = U1RXREG;
        if (timeout != 0)
        {
            timer_setup(timeout);            
        } else {
            if (U1STAbits.URXDA) 
            {
                return_value = U1RXREG;
                RS485_send_char(return_value);
                return return_value;
            }
        }
        

		while ((U1STAbits.URXDA==0) && (receive_timer_flag == 0));

		if (U1STAbits.URXDA) return_value = U1RXREG;
        //if (return_value != 256) RS232_send_char(return_value);
        if (return_value == 0) return_value = 256;
        if (return_value != 256) 
        {
            B2B_local_data.BMS_data_present = 1;
        } else {
            B2B_local_data.BMS_data_present = 0;
        }
        return return_value;
}



//------------------------------------------------------------------------
// Send a string to the PC, terminated with a null (/0)
// Entry: String to fill, max length of string generally "sizeof(string)"
// Exit: None
//------------------------------------------------------------------------
void RS485_send_string(const char *data_string, unsigned int length)
{
        while ((*data_string) && (length>0))	
        {
            RS485_send_char(*data_string++);
            length--;
        }
}

//--------------------------------------------------------------------------------------
// Receive a string from the PC
// Entry: String, max length of string generally "sizeof(string)", terminator to detect
// Exit: Returns 0 if nothing received and 1 if the string is filled
//     : Fills the string with what's received
//--------------------------------------------------------------------------------------
unsigned char RS485_receive_string(char *data_string, unsigned int length)
{
	unsigned short int returned_character;

        returned_character = RS485_receive_char(0);	
        if (returned_character == 256)
        {
            *data_string = 0;
            return 0;
        }

        do {
            if (returned_character != 256) *data_string = returned_character;
            if (returned_character == 0x0D) 
            {
                *data_string = 0;
                return 1;
            }
            returned_character = RS485_receive_char(1);
            
            if (returned_character != 256) 
            {
                data_string++;
                length--;
            }
        } while (length > 1);

        *data_string = 0;			
        return 1;
}


//-----------------------
// Send a byte to the PC
// Entry: Byte to send
// Exit: None
//-----------------------
void PC_send_char(unsigned char send_byte)
{
        while (U2STAbits.UTXBF == 1);
        U2TXREG = send_byte;
        //PC_receive_char(0);
}


//------------------------------------------
// Receive a byte from the PC
// Entry: UART timeout in seconds
// Exit: Char received or 256 if a time out
//------------------------------------------
unsigned short int PC_receive_char(unsigned short int timeout)
{
	unsigned short int return_value;

        return_value = 256;

        if (U2STAbits.OERR == 1)
        {
            U2STAbits.OERR = 0;
            return_value = U2RXREG;
            return_value = U2RXREG;
            return_value = U2RXREG;
        }
        if ((U2STAbits.FERR == 1) || (U2STAbits.PERR == 1)) return_value = U2RXREG;
        if (timeout != 0)
        {
            timer_setup(timeout);            
        } else {
            if (U1STAbits.URXDA) 
            {
                return_value = U1RXREG;
                PC_send_char(return_value);
                return return_value;
            }
        }
        

		while ((U2STAbits.URXDA==0) && (receive_timer_flag == 0));

		if (U2STAbits.URXDA) return_value = U2RXREG;
        if (return_value != 256) PC_send_char(return_value);
        if (return_value == 0) return_value = 256;
        return return_value;
}



//------------------------------------------------------------------------
// Send a string to the PC, terminated with a null (/0)
// Entry: String to fill, max length of string generally "sizeof(string)"
// Exit: None
//------------------------------------------------------------------------
void PC_send_string(const char *data_string, unsigned int length)
{
        while ((*data_string) && (length>0))	
        {
            PC_send_char(*data_string++);
            length--;
        }
}

//--------------------------------------------------------------------------------------
// Receive a string from the PC
// Entry: String, max length of string generally "sizeof(string)", terminator to detect
// Exit: Returns 0 if nothing received and 1 if the string is filled
//     : Fills the string with what's received
//--------------------------------------------------------------------------------------
unsigned char PC_receive_string(char *data_string, unsigned int length)
{
	unsigned short int returned_character;

        returned_character = PC_receive_char(0);	
        if (returned_character == 256)
        {
            *data_string = 0;
            return 0;
        }

        do {
            if (returned_character != 256) *data_string = returned_character;
            if (returned_character == 0x0D) 
            {
                *data_string = 0;
                return 1;
            }
            returned_character = PC_receive_char(1);
            
            if (returned_character != 256) 
            {
                data_string++;
                length--;
            }
        } while (length > 1);

        *data_string = 0;			
        return 1;
}



//****************************************************************** CAN routines ************************************************************

//------------------------------------------------------------
// Set or unset the select line from a CAN bus
// Entry: Can interface number 0=Aux, 1=N2K, 2=J1939, 3=Argo.
//        Line state
// Exit: None but the correct line is set/unset
//------------------------------------------------------------
void CAN_select(unsigned char CAN_interface, unsigned char select_set)
{
        if (CAN_interface == Argo_CAN) Argo_select = select_set;
}


//--------------------------------------
// Reset and setup the MCP2515
// Entry: Can bus to reset
// Exit: None. The CAN bus is activated
//--------------------------------------
void CAN_reset(unsigned char CAN_interface)
{

        CAN_select(CAN_interface, 0);
        SPI_write(0xC0);
        CAN_select(CAN_interface, 1);

        delay_ms(50);

        CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x28);                                    //CNF3 address      10MHz @ 250Kb/s     16MHz @ 250Kb/s     16MHz @ 1Mb/s
        SPI_write(0x05);                                    //CNF3              0x07                0x05                0x00
        SPI_write(0xB8);                                    //CNF2              0xBA                0xB8                0x90
        SPI_write(0x01);                                    //CNF1              0x00                0x01                0x02
        SPI_write(0x00);                                    //CANINTE off
        SPI_write(0x00);	                            //CANINTF off
        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x0D);                                    //TXRTSCTRL
        SPI_write(0x00);                                    //Turn off
        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x60);                                    //RXB0CNTRL address
        //FILHIT = 0 BUKT1 = 0 BUKT = 0 RXRTR = 0 N/A = 0 RXM = 10 N/A = 0
        SPI_write(0b01100100);
		CAN_select(CAN_interface, 1);

        delay_ms(1);

		CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x70);                                    //RXB1CNTRL address
        SPI_write(0b01100000);                              //Extended only, BUKT on
		CAN_select(CAN_interface, 1);
 
        delay_ms(1);

		CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x0F);                                    //CANCTRL
        SPI_write(0x00);                                    //resume normal mode and set retry
		CAN_select(CAN_interface, 1);
}



//-----------------------------------------------------------------------------
// CAN buffer check
// Entry: CAN interface to check
// Exit: Fills up to 2 buffers and flags which ones have data, returns success
//-----------------------------------------------------------------------------
unsigned char CAN_buffer_check(unsigned char CAN_interface)
{
	unsigned char CAN_status, CAN_data_available;

        CAN_data_available = CAN_BUFFER_EMPTY;
        CAN_select(CAN_interface, 0);
        SPI_write(0xB0);
        CAN_status = SPI_read();
        CAN_select(CAN_interface, 1);
        CAN_buffer_store[0][CAN_interface].CAN_PGN = 0;                                                                //Clear buffer 0 and 1 contents
        CAN_buffer_store[1][CAN_interface].CAN_PGN = 0;
        if (CAN_status & 0x40)
        {
            CAN_buffer(0, CAN_interface);
            CAN_data_available = CAN_BUFFER0_FULL;                                                 //Return buffer 0 is filled           
        }                                                                                                            //test bit 7 to check if buffer 1 is full
        if (CAN_status & 0x80)
        {
            CAN_buffer(1, CAN_interface);
            if (CAN_data_available == CAN_BUFFER_EMPTY) CAN_data_available = CAN_BUFFER1_FULL;     //Return Buffer 0 is empty but buffer 1 is filled
            if (CAN_data_available == CAN_BUFFER0_FULL) CAN_data_available = CAN_BUFFER0_1_FULL;   //Return Buffer 0 and buffer 1 are filled
        }


        
        return CAN_data_available;
}



//------------------------------------------------
// CAN buffer retrieval
// Entry: CAN interface and buffer that is filled
// Exit: None but the global buffers are filled
//------------------------------------------------
void CAN_buffer(unsigned char buffer_number, unsigned char CAN_interface)
{
	unsigned char i, ID_byte_1, ID_byte_2, ID_byte_3, ID_byte_4, ID_byte_assembly;
    unsigned short int SID_assembly;
    unsigned long EID_assembly, PGN_assembly, temp_assembly;
   
        CAN_select(CAN_interface, 0);

        if (buffer_number == 0) SPI_write(0x90);
        if (buffer_number == 1) SPI_write(0x94);

        ID_byte_1 = SPI_read();                 //SID10, SID9, SID8, SID7, SID6, SID5, SID4, SID3
        ID_byte_2 = SPI_read();                 //SID2, SID1, SID0, SRR, IDE, -, EID17, EID16
        ID_byte_3 = SPI_read();                 //EID15, EID14, EID13, EID12, EID11, EID10, EID9, EID8
        ID_byte_4 = SPI_read();                 //EID7, EID6, EID5, EID4, EID3, EID2, EID1, EID0
    
        SID_assembly = ID_byte_1;
        SID_assembly = SID_assembly << 3;
        ID_byte_assembly = (ID_byte_2 & 0xE0);
        ID_byte_assembly = ID_byte_assembly >> 5;
        SID_assembly = SID_assembly | ID_byte_assembly;

        EID_assembly = (ID_byte_2 & 0x03);
        EID_assembly = EID_assembly * 0x10000;
        temp_assembly = ID_byte_3;
        EID_assembly = EID_assembly + (temp_assembly * 0x100);
        EID_assembly = EID_assembly + ID_byte_4;

        PGN_assembly = SID_assembly;
        PGN_assembly = PGN_assembly * 0x40000;
        PGN_assembly = PGN_assembly + EID_assembly;  
        
        if (buffer_number == 0) CAN_buffer_store[0][CAN_interface].CAN_PGN = PGN_assembly;
        if (buffer_number == 1) CAN_buffer_store[1][CAN_interface].CAN_PGN = PGN_assembly;

        for (i = 0; i < 9; i++)
        {
            if (buffer_number == 0) CAN_buffer_store[0][CAN_interface].CAN_byte[i] = SPI_read();
            if (buffer_number == 1) CAN_buffer_store[1][CAN_interface].CAN_byte[i] = SPI_read();
        }

        CAN_select(CAN_interface, 1);
        

}


//-------------------------------------------------------------
// Transmit CAN message (PGN))
// Entry: CAN data packet to send, CAN interface to send it to
// Exit: None
//-------------------------------------------------------------
void CAN_send(CAN_data_packet CAN_send_data, unsigned char CAN_interface)
{
	unsigned char general, transmit_buffer;
    unsigned long SID11, EID18, EID_calculation;
    unsigned char SID_byte_1, SID_byte_2, EID_byte_1, EID_byte_2, EID_byte_3;   
    union {
        unsigned long PGN;
        unsigned char bytes[4];
    } long_construct;

        if (panic_commands.pause_CAN_bus_activity) return;
    
        transmit_buffer = 0x00;
        CAN_select(CAN_interface, 0);
        SPI_write(0xA0);                                                    //Read Status
        general = SPI_read();
        general = SPI_read();
        CAN_select(CAN_interface, 1);
        if (check_bit(general,2) == 0)                                      //Check to see if the buffer0 is full
        {
                transmit_buffer = 0x40;
        } else {
                if (check_bit(general,4) == 0)                                  //Check to see if the buffer1 is full
                {
                    transmit_buffer = 0x42;
                } else {
                    if (check_bit(general,6) == 0)                              //Check to see if the buffer2 is full
                    {
                        transmit_buffer = 0x44;
                    } else {
                       return;                                                  //Skip the send if it is, something clearly not right on the bus
                    }
                }
            }
        delay_ms(1);
        CAN_select(CAN_interface, 0);
        SPI_write(transmit_buffer);                                         //TX buffer, start at 0x31 (standard ID high), page 54 MCP2515 datasheet
        
        long_construct.PGN = CAN_send_data.CAN_PGN;                     //11 bits of SID + 18bit EID
        SID11 = long_construct.PGN / 0x40000; //>> 18;
        EID18 = (long_construct.PGN & 0x3FFFF);
        SID_byte_1 = SID11 >> 3;
        SID_byte_2 = SID11 << 5;
        SID_byte_2 |= 1 << 3;                                           //Extended header set
        EID_calculation = EID18 / 0x10000;
        EID_byte_1 = (unsigned char)EID_calculation;
        SID_byte_2 = SID_byte_2 | EID_byte_1;
        EID_byte_2 = (EID18 & 0xFF00) >> 8;
        EID_byte_3 = (EID18 & 0xFF);       
        
        SPI_write(SID_byte_1);
        SPI_write(SID_byte_2);
        SPI_write(EID_byte_2);
        SPI_write(EID_byte_3);
    
        SPI_write(0x08);                                                    //8 bytes to be sent (part of the standard header)
                                                                            
        for (general = 0; general < 8; general++)
        {
            SPI_write(CAN_send_data.CAN_byte[general]);
        }

        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x81);
        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x03);
        SPI_write(0x2D);
        general = SPI_read();
        CAN_select(CAN_interface, 1);

        if (!(check_bit(general,5)))
        {
            incoming_Argo_poll();
            return;                                                     //No error occured, assume success
        }
        
        CAN_reset(CAN_interface);                                           //Error sending, try reseting the CAN chip just in case
}


//**************************************************** SPI routines ********************************************************

//----------------------------------
// Setup the hardware SPI interface
// Entry: None
// Exit: None
//----------------------------------
void SPI_init(void)
{
    unsigned long system_frequency_KHz;
    
        system_frequency_KHz = FCY / 1000;
        SPI1CON1L = 0;
        SPI1CON1H = 0;
        SPI1CON2L = 0;
        SPI1CON2H = 0;

        SPI1IMSKL = 0;
        SPI1IMSKH = 0;
        SPI1STATL = 0;
        SPI1STATH = 0;
        
        SPI1CON1Lbits.SPIEN = 0;		//DisableSPI
        SPI1CON1Lbits.SPISIDL = 1;	//Idle mode
        SPI1BUFL = 0;			//Clear SPI buffer
        SPI1BUFH = 0;			//Clear SPI buffer
        SPI1CON1Lbits.DISSCK = 0;		//Enabled SCK
        SPI1CON1Lbits.DISSDO = 0;	//Enabled SDO
        SPI1CON1Lbits.DISSDI = 0;	//Enabled SDI
        SPI1CON1Lbits.MODE16 = 0;	//8 bits
        SPI1CON1Lbits.SMP = 0;		//Sample at end
        SPI1CON1Lbits.CKE = 1;		//Clock -> Idle to active
        SPI1CON1Lbits.CKP = 0;		//Idle state for clock is high
        SPI1CON1Lbits.SSEN = 0;		//Slave pin not used
        SPI1CON1Lbits.MODE = 0;		//8 bit mode
        SPI1BRGL = (system_frequency_KHz / (2 * 4000)) - 1;
        SPI1BRGH = 0;
        SPI1CON1bits.MSTEN = 1;		//Master mode
        SPI1CON1Lbits.SPIEN = 1;		//Enable SPI	

            
}

//-------------------------------------------------------
// Write 'data' to the SPI, enable line already asserted
// Entry: Data to write to the bus
// Exit: Data transfered asynchronously by the write
//-------------------------------------------------------
unsigned char SPI_write(unsigned char data)
{
        SPI1BUFL = data;
        while(!SPI1STATLbits.SPIRBF);	// wait for transfer to complete
        return SPI1BUFL; 
}

//------------------------------------------------------
// Read 'data' to the SPI, enable line already asserted
// Entry: None
// Exit: Data read from the bus
//------------------------------------------------------
unsigned char SPI_read(void)
{
        SPI1STATLbits.SPIROV = 0;
        SPI1BUFL = 0x00;                       // initiate bus cycle
        while(!SPI1STATLbits.SPIRBF);
        /* Check for Receive buffer full status bit of status register*/
        if (SPI1STATLbits.SPIRBF)
        { 
            SPI1STATLbits.SPIROV = 0;
            return (SPI1BUFL);                 /* return word read */
        }
        return 0;
}




//***************************************************** I2C routines ************************************************************

//--------------------------
// Setup and initialise I2C
// Entry: None
// Exit: None
//--------------------------
void I2C1_init(void)
{
	unsigned char buffer;
                                        //Setup hardware I2C
        I2C1CONLbits.I2CEN = 0;		//Disable I2C
        I2C1CONLbits.DISSLW = 1;
        I2C1CONLbits.A10M = 0;		//7 bits
        I2C1CONLbits.SCLREL = 1;		//No clock stretch	
        I2C1BRG = 0x9D;			//100KHz @ 8MHz
        I2C1ADD = 0;
        I2C1MSK = 0;
        buffer = I2C1RCV;
        I2C1TRN = 0;
        I2C1STAT = 0;
        I2C1CONLbits.ACKDT = 0;		//Sends an ACK
        I2C1CONLbits.I2CEN = 1;		//Enable I2C
}

//-----------------------------------------------------------
// Start I2C, this function generates an I2C start condition
// Entry: None
// Exit: None
//-----------------------------------------------------------
void I2C1_start(void)
{
        I2C1CONLbits.ACKDT = 0;		//Reset any previous Ack
        //delay_us(10);
        I2C1CONLbits.SEN = 1;		//Generate Start COndition
        time_out_trap();
        while ((I2C1CONLbits.SEN) && (time_out_flag == 0));	//Wait for Start COndition       
}

//--------------------------------------------------------------------------------
//Restart I2C, this function generates an I2C Restart condition (second start bit)
// Entry: None
// Exit: None
//--------------------------------------------------------------------------------
void I2C1_restart(void)
{
        I2C1CONLbits.RSEN = 1;		//Generate Restart		
        time_out_trap();
        while ((I2C1CONLbits.RSEN)  && (time_out_flag == 0));	//Wait for restart	
}


//-------------------------------------------------------------
// Stop bit I2C, this function generates an I2C stop condition
// Entry: None
// Exit: None
//-------------------------------------------------------------
void I2C1_stop(void)
{
        I2C1CONLbits.PEN = 1;		//Generate Stop Condition
        time_out_trap();
        while ((I2C1CONLbits.PEN)  && (time_out_flag == 0));	//Wait for Stop
        I2C1CONLbits.RCEN = 0;
        I2C1STATbits.IWCOL = 0;
        I2C1STATbits.BCL = 0;
        //delay_us(10);
}

//---------------------------------
// Wait for the I2C bus to be idle
// Entry: None
// Exit: None
//---------------------------------
void I2C1_idle(void)
{
        time_out_trap();
        while ((I2C1STATbits.TRSTAT) && (time_out_flag == 0));		//Wait for bus Idle        
}

//-------------------------------
// Listen for an ACK on the line
// Entry: None
// Exit: None
//-------------------------------
unsigned char I2C1_ackstatus(void)
{
        return (!I2C1STATbits.ACKSTAT);		//Return Ack Status
}

//-----------------------------
// Generate an ACK on the line
// Entry: None
// Exit: None
//-----------------------------
void I2C1_ack(void)
{
	unsigned int general;
	
        general = 0;
        I2C1CONLbits.ACKDT = 0;			//Set for ACk
        I2C1CONLbits.ACKEN = 1;
        time_out_trap();
        while((I2C1CONLbits.ACKEN)  && (time_out_flag == 0));		//wait for ACK to complete
        I2C1CONLbits.ACKEN = 0;
}

//--------------------------------
// Generate a NOT ACK on the line
// Entry: None
// Exit: None
//--------------------------------
void I2C1_notack(void)
{
        I2C1CONLbits.ACKDT = 1;			//Set for NotACk
        I2C1CONLbits.ACKEN = 1;
        time_out_trap();
        while((I2C1CONLbits.ACKEN) && (time_out_flag == 0));		//wait for ACK to complete
        I2C1CONLbits.ACKEN = 0;
        I2C1CONLbits.ACKDT = 0;			//Set for NotACk
}

//----------------------------------------------------------------------------------
// Write a byte to the I2C, this function transmits the byte passed to the function
// Entry: Byte to send to the bus
// Exit: None
//----------------------------------------------------------------------------------
void I2C1_write(unsigned char byte)
{
       // time_out_trap();
       // while ((I2C1STATbits.TBF)  && (time_out_flag == 0));		//wait for data transmission
        I2C1TRN = byte;				//Load byte to I2C1 Transmit buffer
        time_out_trap();
        while ((I2C1STATbits.TRSTAT) && (time_out_flag == 0));		//wait for data transmission
        //delay_us(2);
}

//----------------------------------------------------------------------------------
// Read a byte to the I2C, this function reads a byte fromt eh bus
// Entry: None
// Exit: Byte sampled on the bus
//----------------------------------------------------------------------------------
unsigned char I2C1_read(void)
{
        I2C1CONLbits.RCEN = 1;			//Enable Master receive
        time_out_trap();
        while((!I2C1STATbits.RBF) && (time_out_flag == 0));	//Wait for receive buffer to be full
        
        if ((time_out_flag) || (I2C1_ackstatus() == 0))
        {
            return 0;
        } else {
            return(I2C1RCV);			//Return data in buffer
        }
}






//*********************************************************** I2C devices ********************************************************


//*************************************************************** PIC ADC routines *******************************************************

//-----------------------------------------------------
//Intialise Analogue channels for temperature sampling
//Entry: None
//Exit: None
//-----------------------------------------------------
void PIC_ADC_init(void)
{
      
       ANSA = 0;
       ANSB = 0;
       ANSC = 0;

       ANSAbits.ANSA0 = 1; // ANA0 Input voltage
       ANSAbits.ANSA1 = 1;  // ANA1 Output voltage
       ANSBbits.ANSB0 = 1;  // ANA2 DC/DC voltage
       ANSBbits.ANSB1 = 1;  // ANA3 DC/DC Temp 6
       ANSBbits.ANSB2 = 1;  // ANA4 DC/DC Temp 5
       ANSBbits.ANSB3 = 1;  // ANA5 DC/DC Temp 4                          
       ANSBbits.ANSB14 = 1; // ANA6 DC/DC Temp 3
       ANSBbits.ANSB13 = 1;  // ANA7 DC/DC Temp 2
       ANSBbits.ANSB12 = 1;  // ANA8 DC/DC Temp 1
       
       AD1CHS = 0;
       AD1CON1 = 0;
       AD1CON1bits.ADSIDL = 0;
       AD1CON1bits.SSRC = 0b0111;
       AD1CON1bits.MODE12 = 1;   
       
       AD1CON2 = 0;
       AD1CON3 = 0;
       AD1CON3bits.SAMC = 0b11111;
       AD1CON3bits.ADCS = 0b00000100;
       //AD1CON3 = 0x1F02;
       AD1CON5 = 0;
       AD1CSSL = 0;
       AD1CON1bits.ADON = 1;
       

}

//------------------
// Read the PIC ADC
// Entry: None
// Exit: Raw Value
//------------------
double PIC_ADC_voltage_read(unsigned char channel)
{
    unsigned long voltage_average;
    unsigned char counter;
    double voltage_result;
    
        AD1CON1bits.ADON = 0;
        AD1CHS = channel;
        delay_us(10);
        AD1CON1bits.ADON = 1;
        delay_us(100);
        voltage_average = 0;
        counter = 16;
        do {
            AD1CON1bits.SAMP = 1;
            while (!AD1CON1bits.DONE);
            
            voltage_average = voltage_average + ADC1BUF0;
            counter--;

        } while (counter > 0);
        
        voltage_average = voltage_average / 16;
        voltage_result = (double)voltage_average / DC_voltage_ratio;
        return voltage_result;
}

double PIC_ADC_temperature_read(unsigned char channel)
{
    unsigned long temperature_average;
    unsigned char counter;
    double Kelvin, Celcius, invT0, log_value, t_average, invBeta;
    
        AD1CON1bits.ADON = 0;
        AD1CHS = channel;
        delay_us(10);
        AD1CON1bits.ADON = 1;
        delay_us(100);
        temperature_average = 0;
        counter = 16;
        do {
            AD1CON1bits.SAMP = 1;
            while (!AD1CON1bits.DONE);
            
            temperature_average = temperature_average + ADC1BUF0;
            counter--;

        } while (counter > 0);                
        temperature_average = temperature_average / 16;
        
        invBeta = 1 / thermistor_beta;
        t_average = (double)(4095 - temperature_average);
        invT0 = 1 / 298.15;
        log_value = log(4095 / t_average - 1.00);
        Kelvin = 1.00 / (invT0 + invBeta * log_value);
        Celcius = Kelvin - 273.15;                      // convert to Celsius
      
        return Celcius;
}




//**************************************************** Timer routines *************************************************

//-----------------------------------------------------
// Delay in seconds (0-255)
// Entry: Number of seconds to delay
// Exit: None but no return until the time has elapsed
//-----------------------------------------------------
void n_second_delay(unsigned char length)
{
	unsigned char loop;

        for (loop = 0; loop < length; loop++)
        {
            delay_ms(1000);
        }
}


//----------------------------------
// Timer setup
// Entry: Number of seconds to wait
// Exit: None but timer is started
//----------------------------------
void timer_setup(double seconds)
{
    unsigned long maximum_seconds_check;
	union {
			unsigned long timer_count;
			unsigned int timer[2];
	} PR_timer;

        T4CON = 0;
        T5CON = 0;
        TMR4 = 0;
        TMR5 = 0;
        T4CONbits.T32 = 1;                                      //32 bit timer
        T4CONbits.TCKPS = 0b00;                          	//Divide by 1

        maximum_seconds_check = 0xFFFFFFFF / FCY;
        if (seconds > maximum_seconds_check) seconds = maximum_seconds_check;
        PR_timer.timer_count = (unsigned long)(seconds * FCY);

        PR4 = PR_timer.timer[0];
        PR5 = PR_timer.timer[1];

        TMR5HLD = 0;
        //reset the trigger flag
        timer_trigger_flag = 0;
        T4CONbits.TON = 1;                                      //Start timer
}

//----------------------------------
// Timer setup
// Entry: Number of seconds to wait
// Exit: None but timer is started
//----------------------------------
void control_timer_setup(double seconds)
{
    unsigned long maximum_seconds_check;
	union {
			unsigned long timer_count;
			unsigned int timer[2];
	} PR_timer;

        T2CON = 0;
        T3CON = 0;
        TMR2 = 0;
        TMR3 = 0;
        T2CONbits.T32 = 1;                                      //32 bit timer
        T2CONbits.TCKPS = 0b00;                          	//Divide by 1

        maximum_seconds_check = 0xFFFFFFFF / FCY;
        if (seconds > maximum_seconds_check) seconds = maximum_seconds_check;
        PR_timer.timer_count = (unsigned long)(seconds * FCY);

        PR2 = PR_timer.timer[0];
        PR3 = PR_timer.timer[1];

        TMR3HLD = 0;
        //reset the trigger flag
        control_timer_trigger_flag = 0;
        T2CONbits.TON = 1;                                      //Start timer
}


//************************************************************** Misc routines *************************************************************

//-----------------------------
// Initialise the power sensor
// Entry: None
// Exit: None
//-----------------------------
void B2B_init(void)
{
        //Default power on pin setup
        Argo_select = 1;             
        BMS_select = 1;
        FRAM_select = 1;     
        LED_power = 1;
        master_alarm_out = 0;
        fan_control = 0;
        TQ_switch = 0;
        bluetooth_power = 0;
        shutdown_DC_1 = 0;
        shutdown_DC_2 = 0;
        shutdown_DC_3 = 0;
        shutdown_DC_4 = 0;
        shutdown_DC_5 = 0;
        shutdown_DC_6 = 0;          
        master_alarm_out = 0;
        RS485_direction = 0;        

        //Initialise the onboard interfaces and hardware
        PIC_ADC_init();
        I2C1_init();          
        SPI_init();        
        CAN_reset(Argo_CAN);
        
        UART_init(115200);
        delay_ms(500);
                         //Sensor initialise
        check_virgin_FRAM();        

        delay_ms(100);                         
        factory_reset();
        HTFS200_setup();
        HTFS200_read(1);
        EUI48 = EUI48_read();
        unique_16bit_ID = (unsigned short int)(EUI48 & 0xFFFF);

        setup_io_expander();
        module_number = read_io_expander();
        
        if (operating_voltage == 12) 
        {
            device_class = B2B_master_12V_device_class;
            slave_device_class = B2B_slave_12V_device_class;
        }
        if (operating_voltage == 24) 
        {
            device_class = B2B_master_24V_device_class;
            slave_device_class = B2B_slave_12V_device_class;
        }
        if (module_number == 0) //we're the master
        {
            master_module = 1;    
            TRISBbits.TRISB15 = 0; // 12. Master alarm (out)           
            Bluetooth_init(1);

        } else {
            master_module = 0;    
            TRISBbits.TRISB15 = 1; // 12. Master alarm (in)                        
        //    if (operating_voltage == 12) device_class = B2B_slave_12V_device_class;
          //  if (operating_voltage == 24) device_class = B2B_slave_24V_device_class;
            
        }

        FRAM_update_active(0,1);
        FRAM_unique_ID(unique_16bit_ID,1);                         

}


void Bluetooth_init(unsigned char power)
{
    unsigned char return_value;
        bluetooth_power = power;
        n_second_delay(1);                
        return_value = U1RXREG;
        return_value = U1RXREG;
        return_value = U1RXREG;           
}
//-----------------------------
// Read 48bit EUI
// Entry: none
// Exit: 48bit value in 64bits
//-----------------------------
unsigned long long int EUI48_read(void)
{
	unsigned char general;
    union {
        unsigned long long int EUI48_64bit;
        unsigned char EUI48_bytes[8];
    } EUI48_value;

        general = EUI48_address;
        general &= ~(1 << 0);		//clear bit to write
        I2C1_idle();
        I2C1_start();
        I2C1_write(general);
        Nop();
        Nop();
        I2C1_write(0xFA);
        Nop();
        Nop();
        I2C1_restart();
        general |= 1 << 0;
        I2C1_write(general);
        Nop();
        Nop();
        EUI48_value.EUI48_bytes[5] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[4] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[3] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[2] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[1] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[0] = I2C1_read();
        I2C1_notack();
        I2C1_stop();
        EUI48_value.EUI48_bytes[6] = 0;
        EUI48_value.EUI48_bytes[7] = 0;

        return EUI48_value.EUI48_64bit;
}

//-------------------
// Setup current ADC
// Entry: None
// Exit: None
//-------------------
void HTFS200_setup(void)
{
	unsigned char general;

        general = Hall_DC_address;
        general &= ~(1 << 0);		//clear bit to write
        I2C1_idle();
        I2C1_start();
        I2C1_write(general);
        I2C1_write(0b00011100);		//set resolution
        I2C1_stop();	
}

//--------------------
// Sample current ADC
// Entry: None
// Exit: Raw sample
//--------------------
signed long HTFS200_sample(void)
{
    unsigned char general, configuration_byte;
    union {
        signed long current_long;
        unsigned char current_bytes[4];
    } current_byte_split;
   
        check_for_valid_data = NO_VALID_DATA;

        general = Hall_DC_address;
		general |= 1 << 0;			//set bit for read
		I2C1_idle();
		I2C1_start();
		I2C1_write(general);
		current_byte_split.current_bytes[2] = I2C1_read();
		I2C1_ack();
		current_byte_split.current_bytes[1] = I2C1_read();
		I2C1_ack();
		current_byte_split.current_bytes[0] = I2C1_read();
		I2C1_ack();
        configuration_byte = I2C1_read();
        I2C1_notack();
		I2C1_stop();
		current_byte_split.current_bytes[3] = 0;

        if ((configuration_byte & 0x80) == 0) check_for_valid_data = NEW_DATA_AVAILABLE;
  		//check the top bit of the 18bit number
		if (current_byte_split.current_bytes[2] & 0x02)
		{
			//bit 2 of the last byte is set which means the 18bit number is negative (using 2's compliment) so pad the last byte of the 32bit number as 0xFF...
			current_byte_split.current_bytes[3] = 0xFF;
			//...top 6 bits of byte 2 have already been set if needed by the ADC for a true negative 32 bit number...
		}           
            
        return current_byte_split.current_long;
}



//-------------------------------------------------------------------------------------------------
// Read current ADC
// Entry: 0 = send a raw uncalculated sample back for zero offset, 1 = Convert sample to real Amps
// Exit: Raw or real sample as specified on entry
//-------------------------------------------------------------------------------------------------
double HTFS200_read(unsigned char rawflag)
{
	unsigned char h;
	long double raw_current, processed_current;
    long double float_current, K;

        check_for_valid_data = NO_VALID_DATA;
        raw_current = HTFS200_sample();

        if (rawflag) return raw_current;
        
        if (check_for_valid_data == NO_VALID_DATA) return raw_current;                                  
        
        processed_current = (raw_current - DC_current_zero_offset);

        
        //Calculate the average current;
        last_DC_current[DC_current_average_loop] = processed_current;
        DC_current_average_loop++;
        if (DC_current_average_loop == historic_average) DC_current_average_loop = 0;
        K = 1.0 / historic_average;
        float_current = 0;
        for (h = 0; h < historic_average; h++)
        {
            float_current = float_current + (K * (last_DC_current[h] - float_current));
        }
        float_current = float_current / DC_current_ratio_value;
        if (current_polarity == 1) float_current = -float_current;
        if ((float_current < 0.1) && (float_current > -0.1)) float_current = 0;              
         
        

        return float_current;
}


//---------------------------------------------
// Reset variables in FRAM and copy to globals
// Entry: None
// Exit: None but FRAM varaibles are reset
//---------------------------------------------
void factory_reset(void)
{
        FRAM_battery_manufacturer(0,2);                            
        FRAM_battery_model(0,2);                                   

        if (operating_voltage == 12)
        {
            FRAM_Lithium_bulk_target_voltage(Lithium_12V_bulk_target_voltage,1);                     
            FRAM_Lithium_float_target_voltage(Lithium_12V_float_target_voltage,1);                    
            FRAM_Lithium_min_voltage(Lithium_12V_min_voltage,1);                             
            FRAM_Lithium_max_voltage(Lithium_12V_max_voltage,1);                             
            FRAM_Lithium_safe_fallback_voltage(Lithium_12V_safe_fallback_voltage,1);                   
            FRAM_fixed_charge_voltage(fixed_12V_charge_voltage,1);                            
            FRAM_default_safety_voltage(default_12V_safety_voltage,1);                          
        }       
        if (operating_voltage == 24)
        {
            FRAM_Lithium_bulk_target_voltage(Lithium_24V_bulk_target_voltage,1);                     
            FRAM_Lithium_float_target_voltage(Lithium_24V_float_target_voltage,1);                    
            FRAM_Lithium_min_voltage(Lithium_24V_min_voltage,1);                             
            FRAM_Lithium_max_voltage(Lithium_24V_max_voltage,1);                             
            FRAM_Lithium_safe_fallback_voltage(Lithium_24V_safe_fallback_voltage,1);                   
            FRAM_fixed_charge_voltage(fixed_24V_charge_voltage,1);                            
            FRAM_default_safety_voltage(default_24V_safety_voltage,1);                          
        }       

        FRAM_Lithium_min_charge_temperature(0,2);                  
        FRAM_Lithium_max_charge_temperature(0,2);                  
        FRAM_Lithium_min_discharge_temperature(0,2);               
        FRAM_Lithium_max_discharge_temperature(0,2);               
        FRAM_Lithium_max_discharge_current(0,2);                   
        FRAM_Lithium_max_charge_current(0,2);                      
        FRAM_Lithium_cycle_reset_SoC(0,2);                         
        FRAM_Lithium_float_trip_accuracy(0,2);                     
        FRAM_fixed_charge_current(0,2);                            
        FRAM_low_SoC(0,2);                                         
        FRAM_BMS_interface_to_use(0,2);                            
        FRAM_input_voltage_disconnect(0,2);                        
        FRAM_name_index(0,2);                                     
        FRAM_operation_mode(0,2);                                  
        FRAM_maximum_chip_temperature(0,2);                        

        //81 -> 128 reserved


        //Not parameters but internal use

        FRAM_current_zero_offset(0,2);      
        FRAM_slave1_ID(0,2);                
        FRAM_slave2_ID(0,2);                
        FRAM_slave3_ID(0,2);                
        FRAM_slave4_ID(0,2);                
        FRAM_number_slaves_attached(0,2);   

        load_FRAM_into_globals();
}



//-------------
// I2C traps
// Entry: None
// Exit: None
//-------------
void time_out_trap(void)
{
        T1CON = 0;
        T1CONbits.TCKPS = 0b01;
        TMR1 = 0;
        PR1 = 0xFFFF;
        IFS0bits.T1IF = 0;       
        T1CONbits.TON = 1;
}


//-------------------------------
// Check for Argo interface data
// Entry: None
// Exit: Buffer status
//-------------------------------
unsigned char incoming_Argo_link_poll(void)
{
    unsigned char buffer_status;    
    
        buffer_status = CAN_buffer_check(Argo_CAN);    

        if (buffer_status != CAN_BUFFER_EMPTY)
        {
            
            if (CAN_buffer_store[0][Argo_CAN].CAN_PGN != 0) CAN_link_process(CAN_buffer_store[0][Argo_CAN]);
            if (CAN_buffer_store[1][Argo_CAN].CAN_PGN != 0) CAN_link_process(CAN_buffer_store[1][Argo_CAN]);
        }

        return buffer_status;
}

//-------------------------------
// Check for Argo interface data
// Entry: None
// Exit: Buffer status
//-------------------------------
unsigned char incoming_Argo_poll(void)
{
    unsigned char buffer_status;    
    
        buffer_status = CAN_buffer_check(Argo_CAN);    

        if (buffer_status != CAN_BUFFER_EMPTY)
        {
            
            if (CAN_buffer_store[0][Argo_CAN].CAN_PGN != 0) CAN_data_process(CAN_buffer_store[0][Argo_CAN]);
            if (CAN_buffer_store[1][Argo_CAN].CAN_PGN != 0) CAN_data_process(CAN_buffer_store[1][Argo_CAN]);
        }

        return buffer_status;
}



//------------------------------------------
// Check for battery bank data and harvest
// Entry: CAN data to use in the extraction
// Exit: Global struct updated
//------------------------------------------
void extract_bank_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index)
{
    double general_calculation, general_maths;   
    union both1 {
        unsigned short int short_int;
        struct {
            unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
        } bits;
    } full_data_set;
    
    union {
        signed short int data_int;
    	unsigned char data_bytes[2];
    } signed_byte_construct;
    union {
			unsigned short int data_int;
			unsigned char data_bytes[2];
	} unsigned_byte_construct;        
    unsigned long message_address;
    unsigned char message_number;
    unsigned long PGN_number;
    unsigned char bank_device_class;
    
    PGN_number = PGN_data_extract.CAN_PGN;

       if (PGN_data_extract.CAN_PGN != 0)
        {            
           full_data_set.short_int = bank_data[buffer_index].complete_data_set;
           bank_device_class = bank_data[buffer_index].CAN_device_class;
           bank_data[buffer_index].device_class_set = bank_device_class;
           //Data harvest
            message_number = 0;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].bank_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                bank_data[buffer_index].DC_current = general_calculation;                
                
                general_calculation = (double)PGN_data_extract.CAN_byte[5] * 0.5;
                bank_data[buffer_index].actual_SoC = general_calculation;
                general_calculation = (double)PGN_data_extract.CAN_byte[6] * 0.5;
                bank_data[buffer_index].reference_SoC = general_calculation;  
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                bank_data[buffer_index].time_to_empty = unsigned_byte_construct.data_int;    
                
                full_data_set.bits.b0 = 1;          //1
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }         
            
            message_number = 1;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature1 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature2 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature3 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature4 = general_calculation; 

                full_data_set.bits.b1 = 1;          //3
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;                      
            }
            
            message_number = 2;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage1 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage2 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage3 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage4 = general_calculation;  
                
                full_data_set.bits.b2 = 1;      //7
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                 
            
            message_number = 3;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
               
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].absorption_charge_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].float_charge_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].equalisation_voltage = general_calculation;
                               
                bank_data[buffer_index].charge_mode = PGN_data_extract.CAN_byte[7];
                bank_data[buffer_index].cyclical_counter = PGN_data_extract.CAN_byte[8];

                full_data_set.bits.b3 = 1;      //15
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                              
            
            message_number = 4;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                bank_data[buffer_index].time_till_float = unsigned_byte_construct.data_int;
                bank_data[buffer_index].sensor_name_index = PGN_data_extract.CAN_byte[3];
                bank_data[buffer_index].updated_parameter_status = PGN_data_extract.CAN_byte[4];
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                bank_data[buffer_index].firmware_version = general_calculation;

                bank_data[buffer_index].current_polarity = PGN_data_extract.CAN_byte[7];
                bank_data[buffer_index].alarm = PGN_data_extract.CAN_byte[8];

                full_data_set.bits.b4 = 1;      //31
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                        
            
            message_number = 5;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {               
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.1;
                bank_data[buffer_index].lifetime_kWh_in = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.1;
                bank_data[buffer_index].lifetime_kWh_out = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                bank_data[buffer_index].kWh_in_since_float = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                bank_data[buffer_index].kWh_out_since_float = general_calculation;

                full_data_set.bits.b5 = 1;      //63
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }             
            // Parameter harvest
            full_data_set.short_int = bank_data[buffer_index].complete_parameter_set;
            message_number = 0;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                bank_data[buffer_index].bank_Ah_capacity = unsigned_byte_construct.data_int;
                general_calculation = (double)PGN_data_extract.CAN_byte[3] * 0.5;
                bank_data[buffer_index].charge_efficiency = (unsigned char)general_calculation;
                general_calculation = (double)PGN_data_extract.CAN_byte[4] * 0.5;
                bank_data[buffer_index].discharge_efficiency = (unsigned char)general_calculation;
                bank_data[buffer_index].battery_chemistry = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[6];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[7];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].nominal_block_voltage = general_calculation;
                bank_data[buffer_index].rated_discharge_time = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b0 = 1;      //1
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }
            
            message_number = 1;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].absorption_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].float_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.00003;
                bank_data[buffer_index].Peukerts_constant = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.015;
                bank_data[buffer_index].temperature_compensation = general_calculation;
                full_data_set.bits.b1 = 1;      //3
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      

            }
            
            message_number = 2;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                general_calculation = (double)PGN_data_extract.CAN_byte[1] * 0.5;
                bank_data[buffer_index].min_SoC = (unsigned char)general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[2];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[3];                
                bank_data[buffer_index].lifetime_kWh = unsigned_byte_construct.data_int;
                bank_data[buffer_index].time_before_OCV_reset = PGN_data_extract.CAN_byte[4];                
                bank_data[buffer_index].max_time_at_absorption = PGN_data_extract.CAN_byte[5];                
                bank_data[buffer_index].max_bulk_current = PGN_data_extract.CAN_byte[6];                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                bank_data[buffer_index].full_charge_current = general_calculation;
                full_data_set.bits.b2 = 1;      //7
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                                      
            }     
            
            message_number = 3;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].min_datum_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].max_datum_temperature = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.000153;
                bank_data[buffer_index].OCV_current_limit = general_calculation;
                bank_data[buffer_index].pair_index = PGN_data_extract.CAN_byte[7];
                bank_data[buffer_index].number_of_blocks = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b3 = 1;      //15
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
                
            }
            
            message_number = 4;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].minimum_absorption_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].maximum_absorption_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].minimum_float_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].maximum_float_temperature = general_calculation;
                full_data_set.bits.b4 = 1;      //31
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }
       
            message_number = 5;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].minimum_absorption_block_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].maximum_absorption_block_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].minimum_float_block_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].maximum_float_block_voltage = general_calculation;
                full_data_set.bits.b5 = 1;      //63
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }       

            message_number = 6;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].centre_block_voltage_trigger = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                bank_data[buffer_index].current_threshold_trigger = general_calculation;
                bank_data[buffer_index].sensor_name_index = PGN_data_extract.CAN_byte[5];
                bank_data[buffer_index].current_polarity = PGN_data_extract.CAN_byte[6];                
                bank_data[buffer_index].device_class_set = PGN_data_extract.CAN_byte[8];
                
                full_data_set.bits.b6 = 1;      //127
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
                
            }
            
            message_number = 7;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {            
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                bank_data[buffer_index].battery_manufacturer = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];            
                bank_data[buffer_index].battery_model = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_bulk_target_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_float_target_voltage = general_maths;
                full_data_set.bits.b7 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
            }

            message_number = 8;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {    
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_min_charge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_max_charge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_min_discharge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_max_discharge_temperature = general_maths;
                full_data_set.bits.b8 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
            }

            message_number = 9;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {    
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_min_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_max_voltage = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)signed_byte_construct.data_int * 0.025;
                bank_data[buffer_index].Lithium_max_discharge_current = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_construct.data_int * 0.025;
                bank_data[buffer_index].Lithium_max_charge_current = general_maths;
                full_data_set.bits.b9 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }    

            message_number = 10;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_cycle_reset_SoC = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_float_trip_accuracy = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_safe_fallback_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].warning_voltage = general_maths;
                full_data_set.bits.b10 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }    

            message_number = 11;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].disconnect_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].warning_SoC = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].disconnect_SoC = general_maths;
                bank_data[buffer_index].use_pair_index = PGN_data_extract.CAN_byte[7];
                full_data_set.bits.b11 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }
                              
            
       }    
}

//--------------------------------------------------------------
// Work out target voltage from minimums and charge mode status
// Entry: None
// Exit: None but globals are updated
//--------------------------------------------------------------
void sensor_data_process(void)
{
    double lowest_absorption_voltage_search, lowest_float_voltage_search;
    unsigned char charge_mode_search;
    unsigned short int bank_sensor_search;
    
        lowest_float_voltage_search = 100;
        lowest_absorption_voltage_search = 100; 
        charge_mode_search = CHARGE_FLOAT;
        for (bank_sensor_search = 0; bank_sensor_search < bank_sensor_max; bank_sensor_search++)
        {
            if ((bank_data[bank_sensor_search].ID_number != 0) && (bank_data[bank_sensor_search].CAN_device_class == bank_12V_device_class) && (bank_data[bank_sensor_search].complete_data_set == bank_data_message_complete))
            {
                if (bank_data[bank_sensor_search].cyclical_counter == bank_data[bank_sensor_search].cyclical_counter_compare)
                {
                    bank_data[bank_sensor_search].sensor_time_out++;
                } else {
                    bank_data[bank_sensor_search].cyclical_counter_compare = bank_data[bank_sensor_search].cyclical_counter;
                    bank_data[bank_sensor_search].sensor_time_out = 0;
                }

                if (bank_data[bank_sensor_search].sensor_time_out > bank_sensor_disconnect_timeout)
                {                   
                    bank_data[bank_sensor_search].ID_number = 0;   
                    bank_data[bank_sensor_search].complete_data_set = 0;   
                    bank_data[bank_sensor_search].sensor_time_out = 0;   
                } else {
                    Nop();
                    if (bank_data[bank_sensor_search].float_charge_voltage < lowest_float_voltage_search) lowest_float_voltage_search = bank_data[bank_sensor_search].float_charge_voltage;
                    if (bank_data[bank_sensor_search].absorption_charge_voltage < lowest_absorption_voltage_search) lowest_absorption_voltage_search = bank_data[bank_sensor_search].absorption_charge_voltage;
                    if (bank_data[bank_sensor_search].charge_mode == CHARGE_ABSORPTION) charge_mode_search = CHARGE_ABSORPTION;
                }
            }
        }               
        if ((lowest_float_voltage_search < 60) && (lowest_absorption_voltage_search < 60))
        {
            B2B_local_data.lowest_float_voltage = lowest_float_voltage_search;
            B2B_local_data.lowest_absorption_voltage = lowest_absorption_voltage_search;
            B2B_local_data.charge_mode = charge_mode_search;
            Nop();
        } else {
            B2B_local_data.lowest_float_voltage = B2B_local_data.default_safety_voltage;
            B2B_local_data.charge_mode = CHARGE_FLOAT;
        }
}

void CAN_link_process(CAN_data_packet CAN_incoming_data)
{
    signed short int ID_search;
    unsigned long filtered_unique_ID;
    signed short int ID_already_exists;
    volatile unsigned long CAN_device_class;
    unsigned long message_number, message_type;
    

        if (CAN_incoming_data.CAN_PGN == 0) return;

        filtered_unique_ID = (CAN_incoming_data.CAN_PGN & 0xFFFF);
        CAN_device_class = (CAN_incoming_data.CAN_PGN & 0x007F0000) >> 16;
        message_number = (CAN_incoming_data.CAN_PGN & 0x7800000) >> 23;
        message_type = (CAN_incoming_data.CAN_PGN & 0x18000000) >> 27; 
        if ((CAN_device_class == B2B_master_12V_device_class) || (CAN_device_class == B2B_master_24V_device_class))
        {
            if (master_module == 0)
            {
                master_ID = filtered_unique_ID;
            } 
        }
        
        if ((CAN_device_class == B2B_slave_12V_device_class) || (CAN_device_class == B2B_slave_24V_device_class))
        {
            
            if (master_module)
            {
                
                ID_already_exists = -1;
                for (ID_search = 0; ID_search < 5; ID_search++)
                {
                    if (slave_data[ID_search].ID_number == filtered_unique_ID)
                    {
                        slave_data[ID_search].CAN_device_class = CAN_device_class;
                        ID_already_exists = ID_search;
                        break;
                    }
                }

                if ((ID_already_exists == -1) && (message_number == 0))
                {
                    for (ID_search = 0; ID_search < 5; ID_search++)
                    {
                        if ((slave_data[ID_search].ID_number == 0) && (ID_already_exists == -1))
                        {
                            slave_data[ID_search].ID_number = filtered_unique_ID;
                            slave_data[ID_search].CAN_device_class = CAN_device_class;
                            slave_data[ID_search].last_message_received = (CAN_incoming_data.CAN_PGN & 0x07800000) >> 23;
                            slave_data[ID_search].slave_module_position = CAN_incoming_data.CAN_byte[1];
                            ID_already_exists = ID_search;                           
                            break;
                        }
                    }
                }
            }
        }

}


//-----------------------------------
// Sample and store attached sensors
// Entry: CAN packet
// Exit: Success or not
//-----------------------------------
unsigned char CAN_data_process(CAN_data_packet CAN_incoming_data)
{
    signed short int ID_search;
    unsigned long filtered_unique_ID;
    signed short int ID_already_exists;
    volatile unsigned long CAN_device_class;
    unsigned long message_number, message_type;
    unsigned char attached_device;

        if (CAN_incoming_data.CAN_PGN == 0) return CAN_ACC_INVALID_DATA;

        filtered_unique_ID = (CAN_incoming_data.CAN_PGN & 0xFFFF);
        CAN_device_class = (CAN_incoming_data.CAN_PGN & 0x007F0000) >> 16;
        message_number = (CAN_incoming_data.CAN_PGN & 0x7800000) >> 23;
        message_type = (CAN_incoming_data.CAN_PGN & 0x18000000) >> 27; 
        ID_already_exists = -1;                          
        if ((CAN_device_class == bank_24V_device_class) || (CAN_device_class == bank_12V_device_class))
        {
            ID_already_exists = -1;   
 
            for (ID_search = 0; ID_search < bank_sensor_max; ID_search++)
            {
                if (bank_data[ID_search].ID_number == filtered_unique_ID)
                {
                    bank_data[ID_search].CAN_device_class = CAN_device_class;
                    ID_already_exists = ID_search;
                    break;
                }
            }

            if (ID_already_exists == -1)
            {
                for (ID_search = 0; ID_search < bank_sensor_max; ID_search++)
                {
                    if ((bank_data[ID_search].ID_number == 0) && (ID_already_exists == -1))
                    {
                        bank_data[ID_search].ID_number = filtered_unique_ID;
                        bank_data[ID_search].CAN_device_class = CAN_device_class;
                        bank_data[ID_search].last_message_received = (CAN_incoming_data.CAN_PGN & 0x07800000) >> 23;
                        ID_already_exists = ID_search;                           
                        break;
                    }
                }
            }
            if (ID_already_exists == -1) return CAN_ACC_FULL;
            extract_bank_data(CAN_incoming_data, ID_already_exists);
        }
        

        
        if ((CAN_device_class == B2B_master_12V_device_class) || (CAN_device_class == B2B_master_24V_device_class))
        {
            if (B2B_local_data.ID_number != filtered_unique_ID)
            {
                ID_already_exists = -1;   

                for (ID_search = 1; ID_search < B2B_max; ID_search++)
                {
                    if (B2B_data[ID_search].ID_number == filtered_unique_ID)
                    {
                        B2B_data[ID_search].CAN_device_class = CAN_device_class;
                        ID_already_exists = ID_search;
                        break;
                    }
                }

                if (ID_already_exists == -1)
                {
                    for (ID_search = 1; ID_search < B2B_max; ID_search++)
                    {
                        if ((B2B_data[ID_search].ID_number == 0) && (ID_already_exists == -1))
                        {
                            B2B_data[ID_search].ID_number = filtered_unique_ID;
                            B2B_data[ID_search].CAN_device_class = CAN_device_class;
                            B2B_data[ID_search].last_message_received = (CAN_incoming_data.CAN_PGN & 0x07800000) >> 23;
                            ID_already_exists = ID_search;          
                            break;
                        }
                    }
                }
                if (ID_already_exists == -1) return CAN_ACC_FULL;
                if (message_type < 2) extract_B2B_data(CAN_incoming_data, ID_already_exists);                                    
            }
           
            if ((message_type == 2) && (CAN_device_class == device_class))
            {
                if (filtered_unique_ID == unique_16bit_ID) extract_B2B_new_parameters(CAN_incoming_data, ID_already_exists);  
                return CAN_ACC_SUCCESS;
            }
                                        
        }           
        
        if ((CAN_device_class == panic_device_class) && (message_type == 2))
        {      
            process_panic(CAN_incoming_data);
        }

        if ((CAN_device_class == B2B_slave_12V_device_class) || (CAN_device_class == B2B_slave_24V_device_class))
        {
            ID_already_exists = -1;   
            attached_device = 0;
            for (ID_search = 0; ID_search < 5; ID_search++)
            {
                if (slaves_attached[ID_search] == filtered_unique_ID) attached_device = 1;
            }
            
            if (attached_device == 1)
            {

                for (ID_search = 0; ID_search < 5; ID_search++)
                {
                    if (slave_data[ID_search].ID_number == filtered_unique_ID)
                    {
                        slave_data[ID_search].CAN_device_class = CAN_device_class;
                        ID_already_exists = ID_search;
                        break;
                    }
                }

                if (ID_already_exists == -1)
                {
                    for (ID_search = 0; ID_search < 5; ID_search++)
                    {
                        if ((slave_data[ID_search].ID_number == 0) && (ID_already_exists == -1))
                        {
                            slave_data[ID_search].ID_number = filtered_unique_ID;
                            slave_data[ID_search].CAN_device_class = CAN_device_class;
                            slave_data[ID_search].last_message_received = (CAN_incoming_data.CAN_PGN & 0x07800000) >> 23;
                            ID_already_exists = ID_search;                           
                            break;
                        }
                    }
                }

                if (ID_already_exists == -1) return CAN_ACC_FULL;
            }
            if (message_type < 1) extract_slave_data(CAN_incoming_data, ID_already_exists);
            if ((message_type == 2) && (unique_16bit_ID == filtered_unique_ID) && (master_module == 0)) extract_slave_parameters(CAN_incoming_data, ID_already_exists);
            if ((message_type == 3) && (unique_16bit_ID == filtered_unique_ID) && (master_module == 0)) extract_slave_instant_parameters(CAN_incoming_data, ID_already_exists);
        }        
       
    return CAN_ACC_SUCCESS;
}



void extract_B2B_new_parameters(CAN_data_packet PGN_parameter_extract, unsigned short int buffer_index)
{
    union {
        unsigned short int data_short;
        unsigned char data_bytes[2];
    } int_unsigned;

    union {
        signed short int data_short;
        unsigned char data_bytes[2];
    } int_signed;

    union both2 {
	unsigned short int byte;
	struct {
		unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
	} bits;
    } parameter_set;
    double general_maths;
    unsigned long message_number;
    
        message_number = (PGN_parameter_extract.CAN_PGN & 0x7800000) >> 23;        
        parameter_set.byte = new_parameters.parameter_set_complete;

        if (message_number == 0)
        {        
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[1];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[2];
            new_parameters.battery_manufacturer = int_unsigned.data_short;
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[3];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[4];            
            new_parameters.battery_model = int_unsigned.data_short;
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[5];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[6];
            general_maths = (double)int_unsigned.data_short * 0.002;
            new_parameters.Lithium_bulk_target_voltage = general_maths;
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[7];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[8];
            general_maths = (double)int_unsigned.data_short * 0.002;
            new_parameters.Lithium_float_target_voltage = general_maths;
            parameter_set.bits.b0 = 1;
        }

        if (message_number == 1)
        {
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[1];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[2];
            general_maths = (double)int_signed.data_short * 0.005;
            new_parameters.Lithium_min_charge_temperature = general_maths;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[3];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[4];
            general_maths = (double)int_signed.data_short * 0.005;
            new_parameters.Lithium_max_charge_temperature = general_maths;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[5];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[6];
            general_maths = (double)int_signed.data_short * 0.005;
            new_parameters.Lithium_min_discharge_temperature = general_maths;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[7];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[8];
            general_maths = (double)int_signed.data_short * 0.005;
            new_parameters.Lithium_max_discharge_temperature = general_maths;
            parameter_set.bits.b1 = 1;
        }

        if (message_number == 2)
        {
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[1];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[2];
            general_maths = (double)int_unsigned.data_short * 0.002;
            new_parameters.Lithium_min_voltage = general_maths;
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[3];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[4];
            general_maths = (double)int_unsigned.data_short * 0.002;
            new_parameters.Lithium_max_voltage = general_maths;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[5];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[6];
            general_maths = (double)int_signed.data_short * 0.025;
            new_parameters.Lithium_max_discharge_current = general_maths;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[7];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[8];
            general_maths = (double)int_signed.data_short * 0.025;
            new_parameters.Lithium_max_charge_current = general_maths;
            parameter_set.bits.b2 = 1;
        }    

        if (message_number == 3)
        {
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[1];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[2];
            general_maths = (double)int_unsigned.data_short * 0.002;
            new_parameters.Lithium_cycle_reset_SoC = general_maths;
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[3];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[4];
            general_maths = (double)int_unsigned.data_short * 0.002;
            new_parameters.Lithium_float_trip_accuracy = general_maths;
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[5];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[6];
            general_maths = (double)int_unsigned.data_short * 0.002;
            new_parameters.Lithium_safe_fallback_voltage = general_maths;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[7];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[8];
            general_maths = (double)int_signed.data_short * 0.025;
            new_parameters.fixed_charge_current = general_maths;
            parameter_set.bits.b3 = 1;
        }    

        if (message_number == 4)
        {
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[1];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[2];
            general_maths = (double)int_unsigned.data_short * 0.01;
            new_parameters.fixed_charge_voltage = general_maths;
            new_parameters.low_SoC = PGN_parameter_extract.CAN_byte[3];
            new_parameters.BMS_interface_to_use = PGN_parameter_extract.CAN_byte[4];                       
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[5];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[6];
            general_maths = (double)int_unsigned.data_short * 0.01;
            new_parameters.input_voltage_disconnect = general_maths;
            new_parameters.name_index = PGN_parameter_extract.CAN_byte[7];
            new_parameters.operation_mode = PGN_parameter_extract.CAN_byte[8];
            parameter_set.bits.b4 = 1;
        }        
        if (message_number == 5)
        {
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[1];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[2];
            general_maths = (double)int_unsigned.data_short * 0.01;
            new_parameters.default_safety_voltage = general_maths;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[3];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[4];
            general_maths = (double)int_signed.data_short * 0.005;
            new_parameters.maximum_chip_temperature = general_maths;
            parameter_set.bits.b5 = 1;
        }    
        
        if (message_number == 6)
        {
            if (PGN_parameter_extract.CAN_byte[1]) factory_reset();
            if (PGN_parameter_extract.CAN_byte[2]) stop_charging = 1;
            if (PGN_parameter_extract.CAN_byte[3]) stop_charging = 0;
            B2B_local_data.sleep_mode = PGN_parameter_extract.CAN_byte[4];
            if (PGN_parameter_extract.CAN_byte[5])
            {             
                LED_power = 0;             
                FRAM_update_active(4,1);
                start_boot(boot_start);                            
            }
        }            
        
        new_parameters.complete_parameter_set = parameter_set.byte;
}




void send_B2B_master_data(void)
{
	unsigned char message_number;
    double general_maths;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;
       
   
        message_number = 0;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        general_maths = B2B_local_data.input_voltage * 100;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];

        general_maths = B2B_local_data.actual_output_voltage * 100;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];

        general_maths = B2B_local_data.highest_module_temperature * 20;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[4] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = signed_byte_split.data_bytes[1];
        
        general_maths = B2B_local_data.combined_output_current * 40;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = signed_byte_split.data_bytes[1];             
        CAN_send(CAN_send_packet, Argo_CAN);
}


void send_B2B_slave_data(void)
{
	unsigned char message_number;
    double general_maths;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;

   
        message_number = 0;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (slave_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = module_number;
        CAN_send_packet.CAN_byte[1] = module_status;
        
        
        general_maths = B2B_local_data.module_max_chip_temperature * 200;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[2] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = signed_byte_split.data_bytes[1];

        general_maths = B2B_local_data.module_DCDC_voltage * 100;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[1];
       
        general_maths = B2B_local_data.combined_output_current * 40;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = signed_byte_split.data_bytes[1];             
        CAN_send(CAN_send_packet, Argo_CAN);
        
        message_number = 1;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (slave_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = B2B_local_data.cyclical_counter;
        B2B_local_data.cyclical_counter++;
        
        CAN_send_packet.CAN_byte[1] = 0;
        CAN_send_packet.CAN_byte[2] = 0;
        CAN_send_packet.CAN_byte[3] = 0;
        CAN_send_packet.CAN_byte[4] = 0;
        CAN_send_packet.CAN_byte[5] = 0;
        CAN_send_packet.CAN_byte[6] = 0;
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet, Argo_CAN);        
}


void send_B2B_parameters(void)
{
 	unsigned char message_number;
    double general_calculation;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;

        
        message_number = 0;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                  
        
        unsigned_byte_split.data_int = (unsigned short int)B2B_local_data.battery_manufacturer;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];
        unsigned_byte_split.data_int = (unsigned short int)B2B_local_data.battery_model;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_bulk_target_voltage * 500;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_float_target_voltage * 500;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = unsigned_byte_split.data_bytes[1];        
        CAN_send(CAN_send_packet,Argo_CAN);        

        message_number = 1;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);  

        general_calculation = (double)B2B_local_data.Lithium_min_charge_temperature * 200;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = signed_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_max_charge_temperature * 200;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = signed_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_min_discharge_temperature * 200;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = signed_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_max_discharge_temperature * 200;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = signed_byte_split.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 2;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                  
        
        general_calculation = (double)B2B_local_data.Lithium_min_voltage * 500;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_max_voltage * 500;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_max_discharge_current * 40;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = signed_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_max_charge_current * 40;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = signed_byte_split.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);
        
        message_number = 3;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);  

        general_calculation = (double)B2B_local_data.Lithium_cycle_reset_SoC * 500;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_float_trip_accuracy * 500;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.Lithium_safe_fallback_voltage * 500;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[1];
        general_calculation = (double)B2B_local_data.fixed_charge_current * 40;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = signed_byte_split.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 4;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);  

        general_calculation = (double)B2B_local_data.fixed_charge_voltage * 100;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];                
        CAN_send_packet.CAN_byte[2] = B2B_local_data.low_SoC;
        CAN_send_packet.CAN_byte[3] = B2B_local_data.BMS_interface_to_use;
        general_calculation = (double)B2B_local_data.input_voltage_disconnect * 100;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[1];        
        CAN_send_packet.CAN_byte[6] = B2B_local_data.name_index;
        CAN_send_packet.CAN_byte[7] = B2B_local_data.operation_mode;
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 5;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);  
        general_calculation = (double)B2B_local_data.default_safety_voltage * 100;
        unsigned_byte_split.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];                
        general_calculation = (double)B2B_local_data.maximum_chip_temperature * 200;
        signed_byte_split.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = signed_byte_split.data_bytes[1];
        CAN_send_packet.CAN_byte[4] = 0;
        CAN_send_packet.CAN_byte[5] = 0;
        CAN_send_packet.CAN_byte[6] = 0;
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet,Argo_CAN);
}


void load_FRAM_into_globals(void)
{
        B2B_local_data.battery_manufacturer = FRAM_battery_manufacturer(0,0);
        B2B_local_data.battery_model = FRAM_battery_model(0,0);        
        B2B_local_data.Lithium_bulk_target_voltage = FRAM_Lithium_bulk_target_voltage(0,0);             
        B2B_local_data.Lithium_float_target_voltage =FRAM_Lithium_float_target_voltage(0,0);            
        B2B_local_data.Lithium_min_charge_temperature = FRAM_Lithium_min_charge_temperature(0,0);          
        B2B_local_data.Lithium_max_charge_temperature = FRAM_Lithium_max_charge_temperature(0,0);          
        B2B_local_data.Lithium_min_discharge_temperature = FRAM_Lithium_min_discharge_temperature(0,0);       
        B2B_local_data.Lithium_max_discharge_temperature = FRAM_Lithium_max_discharge_temperature(0,0);       
        B2B_local_data.Lithium_min_voltage = FRAM_Lithium_min_voltage(0,0);                     
        B2B_local_data.Lithium_max_voltage = FRAM_Lithium_max_voltage(0,0);                     
        B2B_local_data.Lithium_max_discharge_current = FRAM_Lithium_max_discharge_current(0,0);           
        B2B_local_data.Lithium_max_charge_current = FRAM_Lithium_max_charge_current(0,0);              
        B2B_local_data.Lithium_cycle_reset_SoC = FRAM_Lithium_cycle_reset_SoC(0,0);                 
        B2B_local_data.Lithium_float_trip_accuracy = FRAM_Lithium_float_trip_accuracy(0,0);             
        B2B_local_data.Lithium_safe_fallback_voltage = FRAM_Lithium_safe_fallback_voltage(0,0);           
        B2B_local_data.fixed_charge_current = FRAM_fixed_charge_current(0,0);                    
        B2B_local_data.fixed_charge_voltage = FRAM_fixed_charge_voltage(0,0);                    
        B2B_local_data.low_SoC = FRAM_low_SoC(0,0);                                 
        B2B_local_data.BMS_interface_to_use = FRAM_BMS_interface_to_use(0,0);                    
        B2B_local_data.input_voltage_disconnect = FRAM_input_voltage_disconnect(0,0);                
        B2B_local_data.name_index = FRAM_name_index(0,0);                              
        B2B_local_data.operation_mode = FRAM_operation_mode(0,0);                          
        B2B_local_data.default_safety_voltage = FRAM_default_safety_voltage(0,0);                  
        B2B_local_data.maximum_chip_temperature = FRAM_maximum_chip_temperature(0,0);                
        if ((operating_voltage == 12) && (master_module)) B2B_local_data.CAN_device_class = B2B_master_12V_device_class;
        if ((operating_voltage == 24) && (master_module)) B2B_local_data.CAN_device_class = B2B_master_24V_device_class;
        if ((operating_voltage == 12) && (master_module == 0)) B2B_local_data.CAN_device_class = B2B_slave_12V_device_class;
        if ((operating_voltage == 24) && (master_module == 0)) B2B_local_data.CAN_device_class = B2B_slave_24V_device_class;
        
        DC_current_zero_offset = FRAM_current_zero_offset(0,0);       
        slaves_attached[0] = FRAM_slave1_ID(0,0);
        slaves_attached[1] = FRAM_slave2_ID(0,0);
        slaves_attached[2] = FRAM_slave3_ID(0,0);
        slaves_attached[3] = FRAM_slave4_ID(0,0);
        number_of_slaves_attached = FRAM_number_slaves_attached(0,0);
        B2B_local_data.number_of_modules = number_of_slaves_attached;
        //number_of_slaves_attached = 0;
}

void send_B2B_slave_instant_parameter(void)
{
	unsigned char message_number;
    double general_maths;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;
    unsigned char slave_cycle, slave_voltage_device;
   
    if (operating_voltage == 12) slave_voltage_device = B2B_slave_12V_device_class;
    if (operating_voltage == 24) slave_voltage_device = B2B_slave_24V_device_class;
    
        for (slave_cycle = 0; slave_cycle < 4; slave_cycle++)
        {
            if (slaves_attached[slave_cycle] != 0)
            {
                message_number = 0;
                CAN_send_packet.CAN_PGN = slaves_attached[slave_cycle] + (slave_voltage_device * 0x10000) + (message_number * 0x800000) + (PGN_instant_parameter_message * 0x8000000);

                general_maths = B2B_local_data.fixed_charge_voltage * 100;
                unsigned_byte_split.data_int = (unsigned short int)general_maths;
                CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
                CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];

                general_maths = B2B_local_data.fixed_charge_current * 40;
                signed_byte_split.data_int = (signed short int)general_maths;
                CAN_send_packet.CAN_byte[2] = signed_byte_split.data_bytes[0];
                CAN_send_packet.CAN_byte[3] = signed_byte_split.data_bytes[1];

                CAN_send_packet.CAN_byte[4] = B2B_local_data.operation_mode;
                CAN_send_packet.CAN_byte[5] = B2B_local_data.sleep_mode;

                CAN_send_packet.CAN_byte[6] = B2B_local_data.stop_charging;
                if (B2B_local_data.stop_charging == 0) CAN_send_packet.CAN_byte[7] = 1;             
                CAN_send(CAN_send_packet, Argo_CAN);
            }
        }
}


void copy_new_parameters(void)
{
        FRAM_battery_manufacturer((unsigned short int)new_parameters.battery_manufacturer,1);
        FRAM_battery_model((unsigned short int)new_parameters.battery_model,1);
        FRAM_Lithium_bulk_target_voltage((double)new_parameters.Lithium_bulk_target_voltage,1);
        FRAM_Lithium_float_target_voltage((double)new_parameters.Lithium_float_target_voltage,1);
        FRAM_Lithium_min_charge_temperature((double)new_parameters.Lithium_min_charge_temperature,1);
        FRAM_Lithium_max_charge_temperature((double)new_parameters.Lithium_max_charge_temperature,1);
        FRAM_Lithium_min_discharge_temperature((double)new_parameters.Lithium_min_discharge_temperature,1);
        FRAM_Lithium_max_discharge_temperature((double)new_parameters.Lithium_max_discharge_temperature,1);
        FRAM_Lithium_min_voltage((double)new_parameters.Lithium_min_voltage,1);
        FRAM_Lithium_max_voltage((double)new_parameters.Lithium_max_voltage,1);
        FRAM_Lithium_max_discharge_current((double)new_parameters.Lithium_max_discharge_current,1);
        FRAM_Lithium_max_charge_current((double)new_parameters.Lithium_max_charge_current,1);
        FRAM_Lithium_cycle_reset_SoC((double)new_parameters.Lithium_cycle_reset_SoC,1);
        FRAM_Lithium_float_trip_accuracy((double)new_parameters.Lithium_float_trip_accuracy,1);
        FRAM_Lithium_safe_fallback_voltage((double)new_parameters.Lithium_safe_fallback_voltage,1);
        FRAM_fixed_charge_current((double)new_parameters.fixed_charge_current,1);
        FRAM_fixed_charge_voltage((double)new_parameters.fixed_charge_voltage,1);
        FRAM_low_SoC((unsigned char)new_parameters.low_SoC,1);
        FRAM_BMS_interface_to_use((unsigned char)new_parameters.BMS_interface_to_use,1);
        FRAM_input_voltage_disconnect((double)new_parameters.input_voltage_disconnect,1);
        FRAM_name_index((unsigned char)new_parameters.name_index,1);
        FRAM_operation_mode((unsigned char)new_parameters.operation_mode,1);
        FRAM_default_safety_voltage((double)new_parameters.default_safety_voltage,1);
        FRAM_maximum_chip_temperature((double)new_parameters.maximum_chip_temperature,1);
        
        load_FRAM_into_globals();
}


void process_panic(CAN_data_packet PGN_data_extract)
{
    unsigned long message_address;
    unsigned char message_number;
    unsigned long PGN_number;

    
        PGN_number = PGN_data_extract.CAN_PGN;

       if (PGN_data_extract.CAN_PGN != 0)
        {                                  
            message_number = 0;
            message_address = (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000); 
            
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                panic_commands.stop_generator = PGN_data_extract.CAN_byte[1];
                panic_commands.start_generator = PGN_data_extract.CAN_byte[2];
                panic_commands.disconnect_battery_bank = PGN_data_extract.CAN_byte[3];
                panic_commands.reconnect_battery_bank = PGN_data_extract.CAN_byte[4];
                panic_commands.system_sleep = PGN_data_extract.CAN_byte[5];
                panic_commands.pause_CAN_bus_activity = PGN_data_extract.CAN_byte[6];
                panic_commands.resume_CAN_bus_activity = PGN_data_extract.CAN_byte[7];
                panic_commands.reconnect_battery_bank_alarm_override = PGN_data_extract.CAN_byte[8];
                sleep_mode = panic_commands.system_sleep;
            }
           
       }
}


void dig_pot_set(unsigned short int wiper_position, double voltage)
{
    union both {
    	unsigned short int data_int;
        unsigned char data_byte[2];
    	struct {
        	unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
        } bits;
    } resistor_value;    
    double calculated_wiper_position;
    
    
    unsigned char general;
    
        if (wiper_position > 1023) wiper_position = 1023;
        
        
        resistor_value.data_int = 0;
        resistor_value.bits.b1 = 1;
        resistor_value.bits.b10 = 1;
        resistor_value.bits.b11 = 1;
        resistor_value.bits.b12 = 1;
        
        general = digital_pot_address;
        general &= ~(1 << 0);		//clear bit to write
        I2C1_idle();
        I2C1_start();
        I2C1_write(general);
        Nop();
        Nop();
        I2C1_write(resistor_value.data_byte[1]);       
        Nop();
        Nop();

        I2C1_write(resistor_value.data_byte[0]);
        Nop();
        Nop();

        I2C1_stop();
        
        if (voltage != 0)
        {
            calculated_wiper_position = 1023 - ((voltage - 9.462) * 157.72);
            resistor_value.data_int = (unsigned short int)calculated_wiper_position;
            if (resistor_value.data_int > 1023) resistor_value.data_int = 1023;
        } else {
            resistor_value.data_int = wiper_position;    
        }
        
        
        resistor_value.bits.b10 = 1;
        
        general = digital_pot_address;
        general &= ~(1 << 0);		//clear bit to write
        I2C1_idle();
        I2C1_start();
        I2C1_write(general);
        Nop();
        Nop();
        I2C1_write(resistor_value.data_byte[1]);       
        Nop();
        Nop();

        I2C1_write(resistor_value.data_byte[0]);
        Nop();
        Nop();

        I2C1_stop();   
    
    
}


void setup_io_expander(void)
{
    unsigned char general;
        general = io_expander_address;
        general &= ~(1 << 0);
		I2C1_idle();
		I2C1_start();
		I2C1_write(general);
        I2C1_write(0x03);
		I2C1_write(0xFF);

        I2C1_stop();
    
}

unsigned char read_io_expander(void)
{
    unsigned char general;

        general = io_expander_address;
        general &= ~(1 << 0);
		I2C1_idle();
		I2C1_start();
        I2C1_write(general);
        I2C1_write(0x00);
		I2C1_restart();
        general = io_expander_address;
		general |= 1 << 0;			//set bit for read
        I2C1_write(general);
        general = I2C1_read();
        I2C1_notack();
        I2C1_stop();
        general = (general & 0x0F);
        //zero is master
        if (general == 8) general = 4;
        if (general == 4) general = 3;
        if (general == 2) general = 2;
        if (general == 1) general = 1;
        if (general == 15) general = 5;   
        
        if (general > 6) general = 1; // Make sure a random setting is a slave
        return general;
}

//---------------------------------------------
// Offset current calculation write
// Entry: None
// Exit: None but global variables are updated
//---------------------------------------------
void sample_and_write_current_zero_offset(void)
{
    signed long average;
    unsigned short int average_counter;
    average = 0;
    average_counter = 0;
        do {
            check_for_valid_data = NO_VALID_DATA;
            DC_current_zero_offset = HTFS200_read(1);
            if (check_for_valid_data == NEW_DATA_AVAILABLE)
            {
                average = average + DC_current_zero_offset;
                average_counter++;
            }
        } while (average_counter < 16);
        DC_current_zero_offset = average / 16;              
               
        FRAM_current_zero_offset(DC_current_zero_offset,1);

}

void voltage_control_loop(void)
{
    double actual_power, target_voltage, temperature_compensation, current_limit;
    
    
        if (B2B_local_data.stop_charging)
        {
            digital_pot_value = 1023;
            dig_pot_set(digital_pot_value,0);
            return;            
        }
    
        if (B2B_local_data.input_voltage < B2B_local_data.input_voltage_disconnect) 
        {
            chip_set_power(DCDC_ALL_OFF);
            //set alarm
        }
        if ((B2B_local_data.input_voltage > B2B_local_data.input_voltage_disconnect + 1) && (system_shutdown)) 
        {
            chip_set_power(DCDC_ALL_ON);
            //clear alarm
        }
    
        actual_power = B2B_local_data.module_DCDC_voltage * B2B_local_data.module_output_current;   
        current_limit = 45;
        target_voltage = 0;
        if (B2B_local_data.operation_mode == OP_BANK_SENSOR)
        {
            
            if (B2B_local_data.charge_mode == CHARGE_FLOAT) target_voltage = B2B_local_data.lowest_float_voltage;
            if (B2B_local_data.charge_mode == CHARGE_ABSORPTION) target_voltage = B2B_local_data.lowest_absorption_voltage;
            if ((B2B_local_data.charge_mode == CHARGE_NONE) || (B2B_local_data.charge_mode == CHARGE_ERROR)) target_voltage = 0;
            B2B_local_data.target_voltage = target_voltage;
        }
        if (B2B_local_data.operation_mode == OP_FIXED_VOLTAGE) 
        {
            B2B_local_data.target_voltage = B2B_local_data.fixed_charge_voltage;
            target_voltage = B2B_local_data.fixed_charge_voltage;
        }
        if (B2B_local_data.operation_mode == OP_FIXED_CURRENT) 
        {            
            if (operating_voltage == 12) 
            {
                B2B_local_data.target_voltage = 15;
                target_voltage = 15;
            }
            if (operating_voltage == 24) 
            {
                B2B_local_data.target_voltage = 30;
                target_voltage = 30;
            }
            current_limit = B2B_local_data.fixed_charge_current;   
            if (current_limit > 47) current_limit = 47;
        }               
        if (B2B_local_data.operation_mode == OP_BMS_CONTROL)
        {
            B2B_local_data.target_voltage = 0;
            target_voltage = 0;
        }
        

        if (B2B_local_data.module_max_chip_temperature > (B2B_local_data.maximum_chip_temperature - temperature_hysteresis))
        {
            temperature_compensation = B2B_local_data.maximum_chip_temperature - B2B_local_data.module_max_chip_temperature;
            if (target_voltage > 0) target_voltage = target_voltage - (temperature_compensation / 10);     //Pure guess but adjust voltage down 0.1V per degree C towards maximum
        }
       
        if ((B2B_local_data.module_DCDC_voltage > (target_voltage + 0.1)) || (B2B_local_data.module_output_current > current_limit))
        {
            if (digital_pot_value < 1023) digital_pot_value++;
        } else {
            if (B2B_local_data.module_DCDC_voltage < target_voltage)
            {
                if (digital_pot_value > 0) digital_pot_value--;
            }    
        }
        
        //We need to override if current is too high or temperature
        
        if (B2B_local_data.module_max_chip_temperature >= B2B_local_data.maximum_chip_temperature)
        {
            digital_pot_value = 1023;
            dig_pot_set(digital_pot_value,0);
            chip_set_power(DCDC_ALL_OFF);
        }
        if ((B2B_local_data.module_max_chip_temperature < (B2B_local_data.maximum_chip_temperature - temperature_hysteresis)) && (system_shutdown))
        {
            digital_pot_value = 1023;
            dig_pot_set(digital_pot_value,0);
            chip_set_power(DCDC_ALL_ON);
        }
        
        
        //commented out due to 300mA weirdness
        dig_pot_set(digital_pot_value,0);
}


void chip_set_power(unsigned char command)
{
    if (command == DCDC_ALL_OFF)
    {
        shutdown_DC_1 = 0;
        shutdown_DC_2 = 0;
        shutdown_DC_3 = 0;
        shutdown_DC_4 = 0;
        shutdown_DC_5 = 0;
        shutdown_DC_6 = 0;         
        system_shutdown = 1;
    }
    if (command == DCDC_ALL_ON)
    {
        shutdown_DC_1 = 1;
        shutdown_DC_2 = 1;
        shutdown_DC_3 = 1;
        shutdown_DC_4 = 1;
        shutdown_DC_5 = 1;
        shutdown_DC_6 = 1;         
        system_shutdown = 0;
    }

}

void extract_slave_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index)
{
 	unsigned char message_number, slave_device_class;
    double general_maths;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;

    unsigned long message_address;
    unsigned long PGN_number;
    

        PGN_number = PGN_data_extract.CAN_PGN;
        if (PGN_data_extract.CAN_PGN != 0)
        {            
           slave_device_class = slave_data[buffer_index].CAN_device_class;
           //Data harvest
            message_number = 0;
            message_address = (slave_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                slave_data[buffer_index].slave_module_position = PGN_data_extract.CAN_byte[1];
                slave_data[buffer_index].status = PGN_data_extract.CAN_byte[2];

                signed_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)signed_byte_split.data_int * 0.05;
                slave_data[buffer_index].maximum_DCDC_chip_temperature = general_maths;                

                unsigned_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_split.data_int * 0.01;
                slave_data[buffer_index].DCDC_output_voltage = general_maths;                
                
                signed_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_split.data_int * 0.025;
                slave_data[buffer_index].stack_output_current = general_maths;                
            }     
            
            message_number = 1;
            message_address = (slave_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                slave_data[buffer_index].cyclical_counter = PGN_data_extract.CAN_byte[1];
            }     

        }
}

void slave_data_process(void)
{
    unsigned char slave_search;
        //work out currents and check cyclical counters
        // count boards
       for (slave_search = 0; slave_search < 5; slave_search++)
        {
            if ((slave_data[slave_search].ID_number != 0) && ((slave_data[slave_search].CAN_device_class == B2B_slave_12V_device_class) || (slave_data[slave_search].CAN_device_class == B2B_slave_24V_device_class)))
            {
                if (slave_data[slave_search].cyclical_counter == slave_data[slave_search].cyclical_counter_compare)
                {
                    slave_data[slave_search].slave_timeout++;
                } else {
                    slave_data[slave_search].cyclical_counter_compare = slave_data[slave_search].cyclical_counter;
                    slave_data[slave_search].slave_timeout = 0;
                }

                if (slave_data[slave_search].slave_timeout > slave_disconnect_timeout)
                {                   
                    slave_data[slave_search].ID_number = 0;                       
                    slave_data[slave_search].slave_timeout = 0;   
                } else {
                   //Need to extract the current data from all slave_data
                   if (slave_data[slave_search].slave_module_position == 1) 
                   {
                       B2B_local_data.slave1_DCDC_chips_enabled = slave_data[slave_search].number_DCDC_chips_enabled;
                       B2B_local_data.slave1_DCDC_output_voltage = slave_data[slave_search].DCDC_output_voltage;
                       B2B_local_data.slave1_max_chip_temperature = slave_data[slave_search].maximum_DCDC_chip_temperature;
                       B2B_local_data.slave1_stack_current = slave_data[slave_search].stack_output_current;                       
                   }
                   if (slave_data[slave_search].slave_module_position == 2) 
                   {
                       B2B_local_data.slave2_DCDC_chips_enabled = slave_data[slave_search].number_DCDC_chips_enabled;
                       B2B_local_data.slave2_DCDC_output_voltage = slave_data[slave_search].DCDC_output_voltage;
                       B2B_local_data.slave2_max_chip_temperature = slave_data[slave_search].maximum_DCDC_chip_temperature;
                       B2B_local_data.slave2_stack_current = slave_data[slave_search].stack_output_current;                       
                   }
                   if (slave_data[slave_search].slave_module_position == 3) 
                   {
                       B2B_local_data.slave3_DCDC_chips_enabled = slave_data[slave_search].number_DCDC_chips_enabled;
                       B2B_local_data.slave3_DCDC_output_voltage = slave_data[slave_search].DCDC_output_voltage;
                       B2B_local_data.slave3_max_chip_temperature = slave_data[slave_search].maximum_DCDC_chip_temperature;
                       B2B_local_data.slave3_stack_current = slave_data[slave_search].stack_output_current;       //Might not be stack, might be calcd                
                   }
                   if (slave_data[slave_search].slave_module_position == 4) 
                   {
                       B2B_local_data.slave4_DCDC_chips_enabled = slave_data[slave_search].number_DCDC_chips_enabled;
                       B2B_local_data.slave4_DCDC_output_voltage = slave_data[slave_search].DCDC_output_voltage;
                       B2B_local_data.slave4_max_chip_temperature = slave_data[slave_search].maximum_DCDC_chip_temperature;
                       B2B_local_data.slave4_stack_current = slave_data[slave_search].stack_output_current;                       
                   }                    
                }
            }
        } 
//    if ((number_of_slaves_attached > 0) && (master_module)) //We are the master. Need to count the current from modules above this one
//    {
//        B2B_local_data.slave4_module_current = B2B_local_data.slave4_stack_current;
//        B2B_local_data.slave3_module_current = B2B_local_data.slave3_stack_current - B2B_local_data.slave4_stack_current;
//        B2B_local_data.slave2_module_current = B2B_local_data.slave2_stack_current - B2B_local_data.slave3_stack_current;
//        B2B_local_data.slave1_module_current = B2B_local_data.slave1_stack_current - B2B_local_data.slave2_stack_current;
//        B2B_local_data.module_output_current = B2B_local_data.combined_output_current - B2B_local_data.slave1_stack_current;
//    }
//
//    if ((number_of_slaves_attached > 0) && (master_module == 0)) //We are a slave. Need to count the current from modules above this one
//    {
//        if (module_number == 4) B2B_local_data.module_output_current = B2B_local_data.combined_output_current;
//        if (module_number == 3) B2B_local_data.module_output_current = B2B_local_data.combined_output_current - B2B_local_data.slave4_stack_current;
//        if (module_number == 2) B2B_local_data.module_output_current = B2B_local_data.combined_output_current - B2B_local_data.slave3_stack_current;
//        if (module_number == 1) B2B_local_data.module_output_current = B2B_local_data.combined_output_current - B2B_local_data.slave2_stack_current;
//    }
    

}

void collect_attached_modules(unsigned char role)
{
    unsigned char buffer_state, slave_search, message_cycle;
    
            //Link button pressed. Harvest slave module replies
            if (role == MOD_MASTER) master_alarm_out = 1;
            memset(&slave_data,0,sizeof(slave_data));
            memset(&slaves_attached,0,sizeof(slaves_attached));        
            message_cycle = 0;
            do {
                timer_setup(0.5);
                do {                
                //Sampled the Argo bus for 10 seconds so should have all the IDs in
                buffer_state = incoming_Argo_link_poll();
                } while (timer_trigger_flag == 0);   
                if (role == MOD_SLAVE) send_B2B_slave_data(); 
                if (role == MOD_MASTER) send_B2B_master_data(); 
                message_cycle++;
            } while (message_cycle < 10);   
            
            number_of_slaves_attached = 0;
            for (slave_search = 0; slave_search < 5; slave_search++)
            {                    
                if (slave_data[slave_search].ID_number != 0) 
                {
                    number_of_slaves_attached++;
                    if (slave_data[slave_search].slave_module_position > 0) slaves_attached[slave_data[slave_search].slave_module_position - 1] = slave_data[slave_search].ID_number;
                }
            }                               
            if (role == MOD_MASTER) master_alarm_out = 0;
            timer_setup(1);                                   
            FRAM_slave1_ID(slaves_attached[0],1);
            FRAM_slave2_ID(slaves_attached[1],1);
            FRAM_slave3_ID(slaves_attached[2],1);
            FRAM_slave4_ID(slaves_attached[3],1);
            FRAM_number_slaves_attached(number_of_slaves_attached,1);
            B2B_local_data.number_of_modules = number_of_slaves_attached;
    
}

void set_volatile(volatile void *data, char fill, unsigned short int size)
{
    volatile char *pointer = data;
        while (size-- > 0) {
            *pointer++ = fill;
        }
}

//-------------------------------------------------------------------
// Listen for commands over USB, triggered when a character is found
// Entry: None
// Exit: None but the command is acted upon
//-------------------------------------------------------------------

void listen_for_commands(void)
{
    char PC_string[255];
    char buffer_string[16];
    unsigned char exit_code, PC_response;
    unsigned char parameter_copy;

        strcpy(PC_string,"\fSmart charger HW1.002, FW");
        sprintf(buffer_string, "%d\r\n", firmware_revision);
        strcat(PC_string, buffer_string);
        PC_send_string(PC_string, strlen(PC_string));  
        strcpy(PC_string,"   PC command interface\r\n---------------------------\r\n");                                 
        PC_send_string(PC_string, strlen(PC_string));  
        
        set_volatile(&bank_temp_storage,0,sizeof(bank_temp_storage));
        set_volatile(&B2B_temp_storage,0,sizeof(B2B_temp_storage));
        
        PC_response = U2RXREG;
        PC_response = U2RXREG;
        PC_response = U2RXREG;
        PC_string[0] = 0;
        
        do {

            PC_response = PC_receive_string(PC_string,strlen(PC_string));
            if (PC_response == 1)
            {
                exit_code = parse_command_string(PC_string,strlen(PC_string));
                PC_string[0] = 0;
            }
            incoming_Argo_poll(); //Check for update parameters and messages from the bank sensor   
            extract_host_B2B_data(0);             //Host has no incoming CAN data to process so update live feed
            for (parameter_copy = 0; parameter_copy < B2B_max; parameter_copy++)
            {
                if ((B2B_data[parameter_copy].complete_parameter_set == B2B_parameter_message_complete) && (B2B_temp_storage[parameter_copy].complete_parameter_set != B2B_parameter_message_complete))
                {
                    //copy parameters across   
                    B2B_temp_storage[parameter_copy].CAN_device_class = B2B_data[parameter_copy].CAN_device_class;
                    B2B_temp_storage[parameter_copy].ID_number = B2B_data[parameter_copy].ID_number;
                    B2B_temp_storage[parameter_copy].battery_manufacturer = B2B_data[parameter_copy].battery_manufacturer;
                    B2B_temp_storage[parameter_copy].battery_model = B2B_data[parameter_copy].battery_model;
                    B2B_temp_storage[parameter_copy].Lithium_bulk_target_voltage = B2B_data[parameter_copy].Lithium_bulk_target_voltage;
                    B2B_temp_storage[parameter_copy].Lithium_float_target_voltage = B2B_data[parameter_copy].Lithium_float_target_voltage;
                    B2B_temp_storage[parameter_copy].Lithium_min_charge_temperature = B2B_data[parameter_copy].Lithium_min_charge_temperature;
                    B2B_temp_storage[parameter_copy].Lithium_max_charge_temperature = B2B_data[parameter_copy].Lithium_max_charge_temperature;
                    B2B_temp_storage[parameter_copy].Lithium_min_discharge_temperature = B2B_data[parameter_copy].Lithium_min_discharge_temperature;
                    B2B_temp_storage[parameter_copy].Lithium_max_discharge_temperature = B2B_data[parameter_copy].Lithium_max_discharge_temperature;
                    B2B_temp_storage[parameter_copy].Lithium_min_voltage = B2B_data[parameter_copy].Lithium_min_voltage;
                    B2B_temp_storage[parameter_copy].Lithium_max_voltage = B2B_data[parameter_copy].Lithium_max_voltage;
                    B2B_temp_storage[parameter_copy].Lithium_max_discharge_current = B2B_data[parameter_copy].Lithium_max_discharge_current;
                    B2B_temp_storage[parameter_copy].Lithium_max_charge_current = B2B_data[parameter_copy].Lithium_max_charge_current;
                    B2B_temp_storage[parameter_copy].Lithium_cycle_reset_SoC = B2B_data[parameter_copy].Lithium_cycle_reset_SoC;
                    B2B_temp_storage[parameter_copy].Lithium_float_trip_accuracy = B2B_data[parameter_copy].Lithium_float_trip_accuracy;
                    B2B_temp_storage[parameter_copy].Lithium_safe_fallback_voltage = B2B_data[parameter_copy].Lithium_safe_fallback_voltage;
                    B2B_temp_storage[parameter_copy].fixed_charge_current = B2B_data[parameter_copy].fixed_charge_current;
                    B2B_temp_storage[parameter_copy].fixed_charge_voltage = B2B_data[parameter_copy].fixed_charge_voltage;
                    B2B_temp_storage[parameter_copy].low_SoC = B2B_data[parameter_copy].low_SoC;
                    B2B_temp_storage[parameter_copy].BMS_interface_to_use = B2B_data[parameter_copy].BMS_interface_to_use;
                    B2B_temp_storage[parameter_copy].input_voltage_disconnect = B2B_data[parameter_copy].input_voltage_disconnect;
                    B2B_temp_storage[parameter_copy].name_index = B2B_data[parameter_copy].name_index;
                    B2B_temp_storage[parameter_copy].operation_mode = B2B_data[parameter_copy].operation_mode;
                    B2B_temp_storage[parameter_copy].default_safety_voltage = B2B_data[parameter_copy].default_safety_voltage;
                    B2B_temp_storage[parameter_copy].maximum_chip_temperature = B2B_data[parameter_copy].maximum_chip_temperature;
                    B2B_temp_storage[parameter_copy].sleep_mode = B2B_data[parameter_copy].sleep_mode;
                    B2B_temp_storage[parameter_copy].complete_parameter_set = B2B_data[parameter_copy].complete_parameter_set;
                }
            }
            
            for (parameter_copy = 0; parameter_copy < bank_sensor_max; parameter_copy++)
            {
                if ((bank_data[parameter_copy].complete_parameter_set == bank_parameter_message_complete) && (bank_temp_storage[parameter_copy].complete_parameter_set != bank_parameter_message_complete))
                {
                    //copy parameters across
                    
                    bank_temp_storage[parameter_copy].ID_number = bank_data[parameter_copy].ID_number;
                    bank_temp_storage[parameter_copy].sensor_name_index = bank_data[parameter_copy].sensor_name_index;
                    bank_temp_storage[parameter_copy].bank_Ah_capacity = bank_data[parameter_copy].bank_Ah_capacity;
                    bank_temp_storage[parameter_copy].complete_parameter_set = bank_data[parameter_copy].complete_parameter_set;
                    bank_temp_storage[parameter_copy].bank_Ah_capacity = bank_data[parameter_copy].bank_Ah_capacity;
                    bank_temp_storage[parameter_copy].charge_efficiency = bank_data[parameter_copy].charge_efficiency;
                    bank_temp_storage[parameter_copy].discharge_efficiency = bank_data[parameter_copy].discharge_efficiency;
                    bank_temp_storage[parameter_copy].battery_chemistry = bank_data[parameter_copy].battery_chemistry;
                    bank_temp_storage[parameter_copy].nominal_block_voltage = bank_data[parameter_copy].nominal_block_voltage;
                    bank_temp_storage[parameter_copy].rated_discharge_time = bank_data[parameter_copy].rated_discharge_time;

                    bank_temp_storage[parameter_copy].absorption_voltage = bank_data[parameter_copy].absorption_voltage;
                    bank_temp_storage[parameter_copy].float_voltage = bank_data[parameter_copy].float_voltage;
                    bank_temp_storage[parameter_copy].Peukerts_constant = bank_data[parameter_copy].Peukerts_constant;
                    bank_temp_storage[parameter_copy].temperature_compensation = bank_data[parameter_copy].temperature_compensation;

                    bank_temp_storage[parameter_copy].min_SoC = bank_data[parameter_copy].min_SoC;
                    bank_temp_storage[parameter_copy].lifetime_kWh = bank_data[parameter_copy].lifetime_kWh;
                    bank_temp_storage[parameter_copy].time_before_OCV_reset = bank_data[parameter_copy].time_before_OCV_reset;
                    bank_temp_storage[parameter_copy].max_time_at_absorption = bank_data[parameter_copy].max_time_at_absorption;
                    bank_temp_storage[parameter_copy].max_bulk_current = bank_data[parameter_copy].max_bulk_current;
                    bank_temp_storage[parameter_copy].full_charge_current = bank_data[parameter_copy].full_charge_current;

                    bank_temp_storage[parameter_copy].min_datum_temperature = bank_data[parameter_copy].min_datum_temperature;
                    bank_temp_storage[parameter_copy].max_datum_temperature = bank_data[parameter_copy].max_datum_temperature;
                    bank_temp_storage[parameter_copy].OCV_current_limit = bank_data[parameter_copy].OCV_current_limit;
                    bank_temp_storage[parameter_copy].pair_index = bank_data[parameter_copy].pair_index;
                    bank_temp_storage[parameter_copy].number_of_blocks = bank_data[parameter_copy].number_of_blocks;

                    bank_temp_storage[parameter_copy].minimum_absorption_temperature = bank_data[parameter_copy].minimum_absorption_temperature;
                    bank_temp_storage[parameter_copy].maximum_absorption_temperature = bank_data[parameter_copy].maximum_absorption_temperature;
                    bank_temp_storage[parameter_copy].minimum_float_temperature = bank_data[parameter_copy].minimum_float_temperature;
                    bank_temp_storage[parameter_copy].maximum_float_temperature = bank_data[parameter_copy].maximum_float_temperature;

                    bank_temp_storage[parameter_copy].minimum_absorption_block_voltage = bank_data[parameter_copy].minimum_absorption_block_voltage;
                    bank_temp_storage[parameter_copy].maximum_absorption_block_voltage = bank_data[parameter_copy].maximum_absorption_block_voltage;
                    bank_temp_storage[parameter_copy].minimum_float_block_voltage = bank_data[parameter_copy].minimum_float_block_voltage;
                    bank_temp_storage[parameter_copy].maximum_float_block_voltage = bank_data[parameter_copy].maximum_float_block_voltage;

                    bank_temp_storage[parameter_copy].centre_block_voltage_trigger = bank_data[parameter_copy].centre_block_voltage_trigger;
                    bank_temp_storage[parameter_copy].current_threshold_trigger = bank_data[parameter_copy].current_threshold_trigger;
                    bank_temp_storage[parameter_copy].current_polarity = bank_data[parameter_copy].current_polarity;
                    
                    bank_temp_storage[parameter_copy].device_class_set = bank_data[parameter_copy].device_class_set;
                    bank_temp_storage[parameter_copy].old_device_class_set = bank_data[parameter_copy].device_class_set;
                    bank_temp_storage[parameter_copy].battery_manufacturer = bank_data[parameter_copy].battery_manufacturer;
                    bank_temp_storage[parameter_copy].battery_model = bank_data[parameter_copy].battery_model;
                    bank_temp_storage[parameter_copy].Lithium_bulk_target_voltage = bank_data[parameter_copy].Lithium_bulk_target_voltage;
                    bank_temp_storage[parameter_copy].Lithium_float_target_voltage = bank_data[parameter_copy].Lithium_float_target_voltage;                    
                    bank_temp_storage[parameter_copy].Lithium_min_charge_temperature = bank_data[parameter_copy].Lithium_min_charge_temperature;
                    bank_temp_storage[parameter_copy].Lithium_max_charge_temperature = bank_data[parameter_copy].Lithium_max_charge_temperature;
                    bank_temp_storage[parameter_copy].Lithium_min_discharge_temperature = bank_data[parameter_copy].Lithium_min_discharge_temperature;
                    bank_temp_storage[parameter_copy].Lithium_max_discharge_temperature = bank_data[parameter_copy].Lithium_max_discharge_temperature;
                    bank_temp_storage[parameter_copy].Lithium_min_voltage = bank_data[parameter_copy].Lithium_min_voltage;
                    bank_temp_storage[parameter_copy].Lithium_max_voltage = bank_data[parameter_copy].Lithium_max_voltage;
                    bank_temp_storage[parameter_copy].Lithium_max_discharge_current = bank_data[parameter_copy].Lithium_max_discharge_current;
                    bank_temp_storage[parameter_copy].Lithium_max_charge_current = bank_data[parameter_copy].Lithium_max_charge_current;
                    bank_temp_storage[parameter_copy].Lithium_cycle_reset_SoC = bank_data[parameter_copy].Lithium_cycle_reset_SoC;
                    bank_temp_storage[parameter_copy].Lithium_float_trip_accuracy = bank_data[parameter_copy].Lithium_float_trip_accuracy;                    
                    bank_temp_storage[parameter_copy].Lithium_safe_fallback_voltage = bank_data[parameter_copy].Lithium_safe_fallback_voltage;
                    bank_temp_storage[parameter_copy].warning_voltage = bank_data[parameter_copy].warning_voltage;
                    bank_temp_storage[parameter_copy].disconnect_voltage = bank_data[parameter_copy].disconnect_voltage;
                    bank_temp_storage[parameter_copy].warning_SoC = bank_data[parameter_copy].warning_SoC;
                    bank_temp_storage[parameter_copy].disconnect_SoC = bank_data[parameter_copy].disconnect_SoC;
                    bank_temp_storage[parameter_copy].use_pair_index = bank_data[parameter_copy].use_pair_index;
                }
            }
            
        } while (exit_code == 0);

        PC_response = U2RXREG;
        PC_response = U2RXREG;
        PC_response = U2RXREG;
           

}

void extract_host_B2B_data(unsigned char buffer_index)
{
        B2B_local_data.firmware_version = firmware_revision;
        B2B_data[buffer_index] = B2B_local_data;
        B2B_data[buffer_index].ID_number = unique_16bit_ID;
        B2B_data[buffer_index].CAN_device_class = device_class; 
        B2B_data[buffer_index].complete_data_set = B2B_data_message_complete;
        B2B_data[buffer_index].complete_parameter_set = B2B_parameter_message_complete;
        
        B2B_data[buffer_index].cyclical_counter++;        
        
}

//------------------------------------------
// Check for B2B data and harvest
// Entry: CAN data to use in the extraction
// Exit: Global struct updated
//------------------------------------------
void extract_B2B_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index)
{
    double general_calculation, general_maths;   
    union both1 {
        unsigned short int short_int;
        struct {
            unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
        } bits;
    } full_data_set;
    
    union {
        signed short int data_int;
    	unsigned char data_bytes[2];
    } signed_byte_construct;
    union {
			unsigned short int data_int;
			unsigned char data_bytes[2];
	} unsigned_byte_construct;        
    unsigned long message_address;
    unsigned char message_number;
    unsigned long PGN_number;
    unsigned char incoming_device_class;
    
       PGN_number = PGN_data_extract.CAN_PGN;

       if (PGN_data_extract.CAN_PGN != 0)
        {            
           full_data_set.short_int = B2B_data[buffer_index].complete_data_set;
           incoming_device_class = B2B_data[buffer_index].CAN_device_class;
           
           //Data harvest
            message_number = 0;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].input_voltage = general_calculation;

                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].actual_output_voltage = general_calculation;                

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                B2B_data[buffer_index].highest_module_temperature = general_calculation;                

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].master_stack_current = general_calculation;                
                               
                full_data_set.bits.b0 = 1;          //1
                B2B_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }         
            
            message_number = 1;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                B2B_data[buffer_index].number_of_modules = PGN_data_extract.CAN_byte[1];
                B2B_data[buffer_index].alarm = PGN_data_extract.CAN_byte[2];

                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].master_DCDC_output_voltage = general_calculation;                

                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].slave1_DCDC_output_voltage = general_calculation;                
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].slave2_DCDC_output_voltage = general_calculation;                                
                
                full_data_set.bits.b1 = 1;          //3
                B2B_data[buffer_index].complete_data_set = full_data_set.short_int;                      
            }
            
            message_number = 2;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].slave3_DCDC_output_voltage = general_calculation;                
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].slave4_DCDC_output_voltage = general_calculation;                                

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                B2B_data[buffer_index].master_max_chip_temperature = general_calculation;                

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                B2B_data[buffer_index].slave1_max_chip_temperature = general_calculation;                
                
                full_data_set.bits.b2 = 1;      //7
                B2B_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                 
            
            message_number = 3;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
               
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                B2B_data[buffer_index].slave2_max_chip_temperature = general_calculation;                

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                B2B_data[buffer_index].slave3_max_chip_temperature = general_calculation;        

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                B2B_data[buffer_index].slave4_max_chip_temperature = general_calculation;   
                
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].master_module_current = general_calculation;                                  
                
                full_data_set.bits.b3 = 1;      //15
                B2B_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                              
            
            message_number = 4;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].slave1_module_current = general_calculation;     

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].slave2_module_current = general_calculation;     

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].slave3_module_current = general_calculation;     

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].slave4_module_current = general_calculation;     
                
                full_data_set.bits.b4 = 1;      //31
                B2B_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                        
            
            message_number = 5;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {               
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int;
                B2B_data[buffer_index].firmware_version = general_calculation;    

                B2B_data[buffer_index].name_index = PGN_data_extract.CAN_byte[3];
                B2B_data[buffer_index].fan_active = PGN_data_extract.CAN_byte[4];
                B2B_data[buffer_index].cyclical_counter = PGN_data_extract.CAN_byte[5];
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[6];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[7];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].target_voltage = general_calculation;                                
                
                full_data_set.bits.b5 = 1;      //63
                B2B_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }             
            // Parameter harvest
            full_data_set.short_int = B2B_data[buffer_index].complete_parameter_set;
            message_number = 0;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {            
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                B2B_data[buffer_index].battery_manufacturer = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];            
                B2B_data[buffer_index].battery_model = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                B2B_data[buffer_index].Lithium_bulk_target_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                B2B_data[buffer_index].Lithium_float_target_voltage = general_maths;
                full_data_set.bits.b0 = 1;
                B2B_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
            }

            message_number = 1;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {    
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                B2B_data[buffer_index].Lithium_min_charge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                B2B_data[buffer_index].Lithium_max_charge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                B2B_data[buffer_index].Lithium_min_discharge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                B2B_data[buffer_index].Lithium_max_discharge_temperature = general_maths;
                full_data_set.bits.b1 = 1;
                B2B_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
            }

            message_number = 2;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {    
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                B2B_data[buffer_index].Lithium_min_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                B2B_data[buffer_index].Lithium_max_voltage = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].Lithium_max_discharge_current = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].Lithium_max_charge_current = general_maths;
                full_data_set.bits.b2 = 1;
                B2B_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }    

            message_number = 3;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                B2B_data[buffer_index].Lithium_cycle_reset_SoC = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                B2B_data[buffer_index].Lithium_float_trip_accuracy = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                B2B_data[buffer_index].Lithium_safe_fallback_voltage = general_maths;               
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_construct.data_int * 0.025;
                B2B_data[buffer_index].fixed_charge_current = general_maths;
                
                full_data_set.bits.b3 = 1;
                B2B_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }    

            message_number = 4;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].fixed_charge_voltage = general_maths;
                B2B_data[buffer_index].low_SoC = PGN_data_extract.CAN_byte[3];
                B2B_data[buffer_index].BMS_interface_to_use = PGN_data_extract.CAN_byte[4];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].input_voltage_disconnect = general_maths;
                B2B_data[buffer_index].name_index = PGN_data_extract.CAN_byte[7];
                B2B_data[buffer_index].operation_mode = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b4 = 1;
                B2B_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }
                              
            message_number = 5;
            message_address = (incoming_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].default_safety_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.01;
                B2B_data[buffer_index].maximum_chip_temperature = general_maths;

                full_data_set.bits.b5 = 1;
                B2B_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }
            
       }    
}



//-------------------------------------------------------
// Look through a string for commands and CRC validation
// Entry: String to search through and length
// Exit: None
//-------------------------------------------------------
unsigned char parse_command_string(char *data_string, unsigned int length)
{

    char exit_id[] = "exit";
    char help_commands[] = "help command";
    char help_bank[] = "help bank";
    char help_charger[] = "help charger";

    char star[] = "*";
    unsigned char number_of_banks_found, sensor_search, B2B_search, number_of_chargers_found;
    char incoming_data[255];
    char buffer_string[8];
    unsigned char action_search, bank_voltage_setting, B2B_voltage_setting;
    unsigned short int sensor_ID;

        if (strstr(data_string, exit_id))       //Exit PC mode
        {
            strcpy(incoming_data,"\nOK\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            return 1;
        }
        if (strstr(data_string, help_commands))             //Process help 
        {            
            strcpy(incoming_data,"\n\r\nCommands supported:\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"\r\n");
            for (action_search = 0; action_search < command_strings; action_search++)
            {
                PC_send_string(command_string[action_search], strlen(command_string[action_search]));
                PC_send_string(incoming_data, strlen(incoming_data));                        
            }
            
            strcpy(incoming_data,"\r\nCommands must follow the format <command>,<optional setting>,<16bit ID>\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"Example: read_bank_settings,0x45F2\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"         write_bank_settings,minimum_soc=50,0x45F2\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"\r\nFurther help regarding settings can be found by typing \"help bank\" or \"help charger\". \r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            
            
            strcpy(incoming_data,"OK\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));                        
            return 0;
        }
        if (strstr(data_string, help_bank))
        {            
            strcpy(incoming_data,"\n\r\nBank parameters supported:\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"\r\n");
            for (action_search = 0; action_search < bank_parameter_strings; action_search++)
            {
                PC_send_string(bank_parameter_string[action_search], strlen(bank_parameter_string[action_search]));
                PC_send_string(incoming_data, strlen(incoming_data));                        
            }           
            strcpy(incoming_data,"\r\nBank parameter commands must follow the format <command>,<setting>,<16bit ID>\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"Example: write_bank_setting,minimum_soc=50,0x45F2\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"OK\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));                        
            return 0;
        }
        
        if (strstr(data_string, help_charger))
        {            
            strcpy(incoming_data,"\n\r\nCharger settings supported:\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"\r\n");
            for (action_search = 0; action_search < charger_parameter_strings; action_search++)
            {
                PC_send_string(B2B_parameter_string[action_search], strlen(B2B_parameter_string[action_search]));
                PC_send_string(incoming_data, strlen(incoming_data));                        
            }           
            strcpy(incoming_data,"\r\nCharger setting commands must follow the format <command>,<setting>,<16bit ID>\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"Example: write_charger_setting,safety_voltage=12.8,0x78DC\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));
            strcpy(incoming_data,"OK\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));                        
            return 0;
        }       
        //Process other commands
    
        for (action_search = 0; action_search < command_strings; action_search++)
        {
            if (strstr(data_string, command_string[action_search]))
            {                 
                
                if (strstr(command_string[action_search],"charger"))
                {
                    if (strstr(command_string[action_search],"detect"))
                    {
                        number_of_chargers_found = 0;
                        for (B2B_search = 0; B2B_search < B2B_max; B2B_search++)
                        {
                            if (B2B_data[B2B_search].ID_number != 0) number_of_chargers_found++;
                        }                                                               
                        strcpy(incoming_data,"\nChargers found: ");
                        sprintf(buffer_string, "%d\r\n", number_of_chargers_found);
                        strcat(incoming_data, buffer_string);                               
                        PC_send_string(incoming_data, strlen(incoming_data));                                
                        if (number_of_chargers_found > 0)
                        {
                            for (B2B_search = 0; B2B_search < B2B_max; B2B_search++)
                            {
                                if (B2B_data[B2B_search].ID_number != 0)
                                {
                                    sprintf(incoming_data,"0x%04X,",B2B_data[B2B_search].ID_number);
                                    if (B2B_data[B2B_search].name_index < number_of_names) strcat(incoming_data, device_names_string[B2B_data[B2B_search].name_index]);
                                    B2B_voltage_setting = 12;
                                    if (B2B_data[B2B_search].CAN_device_class == B2B_master_24V_device_class) B2B_voltage_setting = 24;
                                    if (B2B_data[B2B_search].CAN_device_class == B2B_master_12V_device_class) B2B_voltage_setting = 12;
                                    sprintf(buffer_string,", %dV ",B2B_voltage_setting);
                                    strcat(incoming_data, buffer_string);
                                    
                                    strcat(incoming_data,"\r\n");
                                    PC_send_string(incoming_data, strlen(incoming_data));                                
                                }
                            }                                                                                                   
                        }
                        strcpy(incoming_data,"OK\r\n");
                        PC_send_string(incoming_data, strlen(incoming_data));                                 
                        return 0;                          
                        
                    } else {
                        sensor_ID = extract_16bit_ID(data_string);   
                        process_charger_commands(data_string, sensor_ID);
                        return 0;
                    }
                }
                if (strstr(command_string[action_search],"bank"))
                {
                    if (strstr(command_string[action_search],"detect"))
                    {
                        number_of_banks_found = 0;
                        for (sensor_search = 0; sensor_search < bank_sensor_max; sensor_search++)
                        {
                            if (bank_data[sensor_search].ID_number != 0) number_of_banks_found++;
                        }                                                               
                        strcpy(incoming_data,"\nBank sensors found: ");
                        sprintf(buffer_string, "%d\r\n", number_of_banks_found);
                        strcat(incoming_data, buffer_string);                               
                        PC_send_string(incoming_data, strlen(incoming_data));                                
                        if (number_of_banks_found > 0)
                        {
                            for (sensor_search = 0; sensor_search < bank_sensor_max; sensor_search++)
                            {
                                if (bank_data[sensor_search].ID_number != 0)
                                {
                                    sprintf(incoming_data,"0x%04X,",bank_data[sensor_search].ID_number);
                                    bank_voltage_setting = 48;
                                    if (bank_data[sensor_search].CAN_device_class == bank_24V_device_class) bank_voltage_setting = 24;
                                    if (bank_data[sensor_search].CAN_device_class == bank_12V_device_class) bank_voltage_setting = 12;
                                    sprintf(buffer_string,"%d, ",bank_voltage_setting);
                                    strcat(incoming_data, buffer_string);
                                    sprintf(buffer_string,"%dV ",bank_voltage_setting);
                                    strcat(incoming_data, buffer_string);
                                    if (bank_data[sensor_search].sensor_name_index < number_of_names) strcat(incoming_data, device_names_string[bank_data[sensor_search].sensor_name_index]);
                                    strcat(incoming_data,"\r\n");
                                    PC_send_string(incoming_data, strlen(incoming_data));                                
                                }
                            }                                                                                                   
                        }
                        strcpy(incoming_data,"OK\r\n");
                        PC_send_string(incoming_data, strlen(incoming_data));                                 
                        return 0;                                                                                                
                    } else {
                        //Extract ID number and act on command
                        sensor_ID = extract_16bit_ID(data_string);   
                        process_bank_sensor_commands(data_string, sensor_ID);
                        return 0;
                    }
                }                                                                       
                strcpy(incoming_data,"\nUnknown parameter\r\n");
                PC_send_string(incoming_data, strlen(incoming_data));
                return 0;   
            }
        }
        if ( (strstr(data_string,"fuck")) || (strstr(data_string,"shit")) || (strstr(data_string,"piss")) )
        {
            strcpy(incoming_data,"\nHey! It\'s not my fault. Go and read the manual!\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));            
            return 0;
        }
        if (strstr(data_string,"nuclear"))
        {
            strcpy(incoming_data,"\nHow about a nice game of chess instead?\r\n");
            PC_send_string(incoming_data, strlen(incoming_data));            
            return 0;
        }
        if (strstr(data_string,"help games"))
        {
            strcpy(incoming_data,"\n\r\nCHESS\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"POKER\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"FIGHTER COMBAT\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"GUERRILLA ENGAGEMENT\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"DESERT WARFARE\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"AIR-TO-GROUND ACTIONS\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"THEATERWIDE TACTICAL WARFARE\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"THEATERWIDE BIOTOXIC AND CHEMICAL WARFARE\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            strcpy(incoming_data,"\r\nGLOBAL THERMONUCLEAR WAR\r\n\r\n");
            PC_send_string(incoming_data, strlen(incoming_data)); 
            return 0;
        }    
        if (strstr(data_string,star) == 0) strcpy(incoming_data,"\nSyntax error\r\n");            
        PC_send_string(incoming_data, strlen(incoming_data));
        return 0;
}

unsigned short int extract_16bit_ID(char *data_string)
{
    unsigned short int decimal_ID;
    char buffer_string[8];
    
    decimal_ID = 0;
        if (strstr(data_string,"0x"))
        {
            strcpy(buffer_string,strstr(data_string,"0x") + 2);            
            decimal_ID = (unsigned short int)strtol(buffer_string, NULL, 16);
        }
        return decimal_ID;
}

void process_bank_sensor_commands(char *data_string, unsigned short int ID)
{
    unsigned char sensor_search, parameter_name, setting_change, timeout_seconds, bank_voltage;
    signed char parameter_found;
    signed char sensor_found;
    char buffer_string[64];
    char buffer_string2[8];    
        sensor_found = -1;
        for (sensor_search = 0; sensor_search < bank_sensor_max; sensor_search++)
        {
            if ((bank_data[sensor_search].ID_number != 0) && (bank_data[sensor_search].ID_number == ID)) sensor_found = sensor_search;
        }     
        if (sensor_found == -1)
        {
            strcpy(buffer_string,"\nInvalid sensor ID\r\n");
            PC_send_string(buffer_string, strlen(buffer_string));  
            return;
        }
        
        if (strstr(data_string, "read_bank_settings"))   
        {                  
            if (sensor_found != -1)
            {               
                if (bank_temp_storage[sensor_found].complete_parameter_set == bank_parameter_message_complete)
                {                                      
                    sprintf(buffer_string,"\n\r\nSensor 0x%04X parameters:\r\n",bank_temp_storage[sensor_found].ID_number);   
                    PC_send_string(buffer_string, strlen(buffer_string)); 
                    
                    for (parameter_name = 0; parameter_name < bank_parameter_strings; parameter_name++)
                    {
                        PC_send_string(bank_parameter_string[parameter_name], strlen(bank_parameter_string[parameter_name])); 
                        strcpy(buffer_string,"=0\r\n");
                        if (parameter_name == 0) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].bank_Ah_capacity);
                        if (parameter_name == 1) sprintf(buffer_string,"=%d\r\n",(unsigned char)bank_temp_storage[sensor_found].charge_efficiency);
                        if (parameter_name == 2) sprintf(buffer_string,"=%d\r\n",(unsigned char)bank_temp_storage[sensor_found].discharge_efficiency);                    
                        if (parameter_name == 3) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].battery_chemistry);
                        if (parameter_name == 4) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].nominal_block_voltage);                    
                        if (parameter_name == 5) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].rated_discharge_time);
                        if (parameter_name == 6) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].absorption_voltage);                    
                        if (parameter_name == 7) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].float_voltage);
                        if (parameter_name == 8) sprintf(buffer_string,"=%.03f\r\n",bank_temp_storage[sensor_found].Peukerts_constant);
                        if (parameter_name == 9) sprintf(buffer_string,"=%.03f\r\n",bank_temp_storage[sensor_found].temperature_compensation);
                        if (parameter_name == 10) sprintf(buffer_string,"=%d\r\n",(unsigned char)bank_temp_storage[sensor_found].min_SoC);
                        if (parameter_name == 11) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].lifetime_kWh);
                        if (parameter_name == 12) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].time_before_OCV_reset);
                        if (parameter_name == 13) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].max_time_at_absorption);
                        if (parameter_name == 14) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].max_bulk_current);
                        if (parameter_name == 15) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].full_charge_current);
                        if (parameter_name == 16) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].min_datum_temperature);
                        if (parameter_name == 17) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].max_datum_temperature);
                        if (parameter_name == 18) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].OCV_current_limit);
                        if (parameter_name == 19) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].pair_index);
                        if (parameter_name == 20) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].number_of_blocks);       
                        if (parameter_name == 21) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].minimum_absorption_temperature);                               
                        if (parameter_name == 22) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].maximum_absorption_temperature);                               
                        if (parameter_name == 23) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].minimum_float_temperature);                               
                        if (parameter_name == 24) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].maximum_float_temperature);                               
                        if (parameter_name == 25) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].minimum_absorption_block_voltage);                               
                        if (parameter_name == 26) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].maximum_absorption_block_voltage);                               
                        if (parameter_name == 27) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].minimum_float_block_voltage);                               
                        if (parameter_name == 28) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].maximum_float_block_voltage);   
                        if (parameter_name == 29) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].centre_block_voltage_trigger);                               
                        if (parameter_name == 30) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].current_threshold_trigger);   
                        if (parameter_name == 31) 
                        {                         
                            sprintf(buffer_string,"=%d, ",bank_temp_storage[sensor_found].sensor_name_index);    
                            bank_voltage = 48;
                            if (bank_temp_storage[sensor_found].device_class_set == bank_24V_device_class) bank_voltage = 24;
                            if (bank_temp_storage[sensor_found].device_class_set == bank_12V_device_class) bank_voltage = 12;
                            sprintf(buffer_string2,"%dV ",bank_voltage);                            
                            strcat(buffer_string, buffer_string2);
                            if (bank_temp_storage[sensor_found].sensor_name_index < number_of_names) strcat(buffer_string, device_names_string[bank_temp_storage[sensor_found].sensor_name_index]);
                            strcat(buffer_string,"\r\n");
                        }
                        if (parameter_name == 32) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].current_polarity);  
                        if (parameter_name == 37) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].device_class_set);  
                        if (parameter_name == 38) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].battery_manufacturer);  
                        if (parameter_name == 39) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].battery_model);  
                        if (parameter_name == 40) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_bulk_target_voltage);  
                        if (parameter_name == 41) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_float_target_voltage);
                        if (parameter_name == 42) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_min_charge_temperature);  
                        if (parameter_name == 43) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_max_charge_temperature);  
                        if (parameter_name == 44) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_min_discharge_temperature);  
                        if (parameter_name == 45) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_max_discharge_temperature);  
                        if (parameter_name == 46) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_min_voltage);  
                        if (parameter_name == 47) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_max_voltage);  
                        if (parameter_name == 48) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_max_discharge_current);  
                        if (parameter_name == 49) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_max_charge_current);  
                        if (parameter_name == 50) sprintf(buffer_string,"=%d\r\n",(unsigned char)bank_temp_storage[sensor_found].Lithium_cycle_reset_SoC);  
                        if (parameter_name == 51) sprintf(buffer_string,"=%d\r\n",(unsigned char)bank_temp_storage[sensor_found].Lithium_float_trip_accuracy);  
                        if (parameter_name == 52) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].Lithium_safe_fallback_voltage);  
                        if (parameter_name == 53) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].warning_voltage);  
                        if (parameter_name == 54) sprintf(buffer_string,"=%.02f\r\n",bank_temp_storage[sensor_found].disconnect_voltage);  
                        if (parameter_name == 55) sprintf(buffer_string,"=%d\r\n",(unsigned char)bank_temp_storage[sensor_found].warning_SoC);  
                        if (parameter_name == 56) sprintf(buffer_string,"=%d\r\n",(unsigned char)bank_temp_storage[sensor_found].disconnect_SoC);  
                        if (parameter_name == 57) sprintf(buffer_string,"=%d\r\n",bank_temp_storage[sensor_found].use_pair_index);  
                        PC_send_string(buffer_string, strlen(buffer_string));
                    }

                    
                    strcpy(buffer_string,"OK\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));               
                    return;
                } else {
                    strcpy(buffer_string,"\nSensor parameters incomplete. Please try again.\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                     
                    return;
                }
            }
        }
        
        if (strstr(data_string, "read_bank_data"))   
        {                  
            if (sensor_found != -1)
            {               
                if (bank_data[sensor_found].complete_data_set == bank_data_message_complete)
                {                                      
                    sprintf(buffer_string,"\n\r\nSensor 0x%04X data:\r\n",bank_data[sensor_found].ID_number);   
                    PC_send_string(buffer_string, strlen(buffer_string)); 
                    
                    for (parameter_name = 0; parameter_name < bank_data_strings; parameter_name++)
                    {
                        PC_send_string(bank_data_string[parameter_name], strlen(bank_data_string[parameter_name])); 
                    
                        if (parameter_name == 0) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].bank_voltage);
                        if (parameter_name == 1) sprintf(buffer_string,"=%.01f\r\n",bank_data[sensor_found].DC_current);
                        if (parameter_name == 2) sprintf(buffer_string,"=%d\r\n",bank_data[sensor_found].actual_SoC);                    
                        if (parameter_name == 3) sprintf(buffer_string,"=%d\r\n",bank_data[sensor_found].reference_SoC);
                        if (parameter_name == 4) sprintf(buffer_string,"=%d\r\n",bank_data[sensor_found].time_to_empty);                    
                        if (parameter_name == 5) sprintf(buffer_string,"=%.01f\r\n",bank_data[sensor_found].block_temperature1);
                        if (parameter_name == 6) sprintf(buffer_string,"=%.01f\r\n",bank_data[sensor_found].block_temperature2);                    
                        if (parameter_name == 7) sprintf(buffer_string,"=%.01f\r\n",bank_data[sensor_found].block_temperature3);
                        if (parameter_name == 8) sprintf(buffer_string,"=%.01f\r\n",bank_data[sensor_found].block_temperature4);
                        if (parameter_name == 9) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].block_voltage1);
                        if (parameter_name == 10) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].block_voltage2);
                        if (parameter_name == 11) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].block_voltage3);
                        if (parameter_name == 12) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].block_voltage4);
                        if (parameter_name == 13) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].absorption_charge_voltage);
                        if (parameter_name == 14) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].float_charge_voltage);
                        if (parameter_name == 15) sprintf(buffer_string,"=%.02f\r\n",bank_data[sensor_found].equalisation_voltage);
                        if (parameter_name == 16) sprintf(buffer_string,"=%d\r\n",bank_data[sensor_found].charge_mode);
                        if (parameter_name == 17) sprintf(buffer_string,"=%d\r\n",bank_data[sensor_found].cyclical_counter);
                        if (parameter_name == 18) sprintf(buffer_string,"=%d\r\n",bank_data[sensor_found].time_till_float);
                        if (parameter_name == 19) sprintf(buffer_string,"=%.03f\r\n",bank_data[sensor_found].firmware_version);     

                        PC_send_string(buffer_string, strlen(buffer_string));
                    }

                    
                    strcpy(buffer_string,"OK\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));               
                    return;
                } else {
                    strcpy(buffer_string,"\nSensor data set incomplete. Please try again.\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                     
                    return;
                }
            }
        }
        if (strstr(data_string, "reset_bank_settings"))   
        {                  
            if (sensor_found != -1)
            {               
                bank_temp_storage[sensor_found].complete_parameter_set = 0;
                strcpy(buffer_string,"\nOK\r\n");
                PC_send_string(buffer_string, strlen(buffer_string));               
                return;
            }
        }
        if (strstr(data_string, "write_bank_settings"))   
        {                  
            if (sensor_found != -1)
            {               
                if (bank_temp_storage[sensor_found].complete_parameter_set == bank_parameter_message_complete)
                {  
                    if (strstr(data_string,"="))
                    {
                        setting_change = 0;
                        for (parameter_name = 0; parameter_name < bank_parameter_strings; parameter_name++)
                        {
                            if (strstr(data_string, bank_parameter_string[parameter_name])) 
                            {
                                parameter_found = parameter_name;
                                setting_change = 1;
                            } else {
                                parameter_found = -1;
                            }
                            if ((parameter_found == 0) && (range_check(data_string, 1, 65535, 0) <= 65535)) bank_temp_storage[sensor_found].bank_Ah_capacity = (unsigned short int)range_check(data_string, 1, 65535, 1);
                            if ((parameter_found == 1) && (range_check(data_string, 1, 100, 0) <= 100)) bank_temp_storage[sensor_found].charge_efficiency = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 2) && (range_check(data_string, 1, 100, 0) <= 100)) bank_temp_storage[sensor_found].discharge_efficiency = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 3) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].battery_chemistry = (unsigned char)range_check(data_string, 0, 255, 1);
                            if ((parameter_found == 4) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].nominal_block_voltage = range_check(data_string, 1, 131.07, 1);
                            if ((parameter_found == 5) && (range_check(data_string, 1, 255, 0) <= 255)) bank_temp_storage[sensor_found].rated_discharge_time = (unsigned char)range_check(data_string, 1, 255, 1);
                            if ((parameter_found == 6) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].absorption_voltage = range_check(data_string, 1, 131.07, 1);
                            if ((parameter_found == 7) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].float_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 8) && (range_check(data_string, 0, 1.92075, 0) <= 1.92075)) bank_temp_storage[sensor_found].Peukerts_constant = range_check(data_string, 0, 1.92075, 1);                            
                            if ((parameter_found == 9) && (range_check(data_string, 0, 983.025, 0) <= 983.025)) bank_temp_storage[sensor_found].temperature_compensation = range_check(data_string, 0, 983.025, 1);                            
                            if ((parameter_found == 10) && (range_check(data_string, 0, 100, 0) <= 100)) bank_temp_storage[sensor_found].min_SoC = (unsigned char)range_check(data_string, 0, 100, 1);                            
                            if ((parameter_found == 11) && (range_check(data_string, 0, 65535, 0) <= 65535)) bank_temp_storage[sensor_found].lifetime_kWh = (unsigned short int)range_check(data_string, 0, 65535, 1);                            
                            if ((parameter_found == 12) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].time_before_OCV_reset = (unsigned char)range_check(data_string, 0, 255, 1);                            
                            if ((parameter_found == 13) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].max_time_at_absorption = (unsigned char)range_check(data_string, 0, 255, 1);                                                        
                            if ((parameter_found == 14) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].max_bulk_current = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                    
                            if ((parameter_found == 15) && (range_check(data_string, 0, 65.535, 0) <= 65.535)) bank_temp_storage[sensor_found].full_charge_current = range_check(data_string, 0, 65.535, 1);                            
                            if ((parameter_found == 16) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].min_datum_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 17) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].max_datum_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 18) && (range_check(data_string, 0, 10.027, 0) <= 10.027)) bank_temp_storage[sensor_found].OCV_current_limit = range_check(data_string, 0, 10.027, 1);                                                        
                            if ((parameter_found == 19) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].pair_index = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                    
                            if ((parameter_found == 20) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].number_of_blocks = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                                                
                            if ((parameter_found == 21) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].minimum_absorption_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 22) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].maximum_absorption_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 23) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].minimum_float_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 24) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].maximum_float_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 25) && (range_check(data_string, 0, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].minimum_absorption_block_voltage = range_check(data_string, 0, 131.07, 1);                                                        
                            if ((parameter_found == 26) && (range_check(data_string, 0, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].maximum_absorption_block_voltage = range_check(data_string, 0, 131.07, 1);                                                        
                            if ((parameter_found == 27) && (range_check(data_string, 0, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].minimum_float_block_voltage = range_check(data_string, 0, 131.07, 1);                                                        
                            if ((parameter_found == 28) && (range_check(data_string, 0, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].maximum_float_block_voltage = range_check(data_string, 0, 131.07, 1);                                                        
                            if ((parameter_found == 29) && (range_check(data_string, 0, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].centre_block_voltage_trigger = range_check(data_string, 0, 131.07, 1);                                                        
                            if ((parameter_found == 30) && (range_check(data_string, 0, 65.535, 0) <= 65.535)) bank_temp_storage[sensor_found].current_threshold_trigger = range_check(data_string, 0, 65.535, 1);                                                        
                            if ((parameter_found == 31) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].sensor_name_index = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                                                                            
                            if ((parameter_found == 32) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].current_polarity = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                                                
                            if ((parameter_found == 33) && (range_check(data_string, 0, 1, 0) <= 1)) bank_temp_storage[sensor_found].set_zero = range_check(data_string, 0, 1, 1);                                                        
                            if ((parameter_found == 34) && (range_check(data_string, 0, 1, 0) <= 1)) bank_temp_storage[sensor_found].reset_factory = range_check(data_string, 0, 1, 1);                                                        
                            if ((parameter_found == 35) && (range_check(data_string, 0, 1, 0) <= 1)) bank_temp_storage[sensor_found].reset_Ah = (unsigned char)range_check(data_string, 0, 1, 1);                                                                                                                                            
                            if ((parameter_found == 36) && (range_check(data_string, 0, 1, 0) <= 1)) bank_temp_storage[sensor_found].reset_kWh = (unsigned char)range_check(data_string, 0, 1, 1);                                                                                                                
                            if ((parameter_found == 37) && (range_check(data_string, 14, 18, 0) <= 18)) bank_temp_storage[sensor_found].device_class_set = (unsigned char)range_check(data_string, 14, 18, 1);                                                                                                                
                            if ((parameter_found == 38) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].battery_manufacturer = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                                                
                            if ((parameter_found == 39) && (range_check(data_string, 0, 255, 0) <= 255)) bank_temp_storage[sensor_found].battery_model = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                                                
                            if ((parameter_found == 40) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].Lithium_bulk_target_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 41) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].Lithium_float_target_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 42) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].Lithium_min_charge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 43) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].Lithium_max_charge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 44) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].Lithium_min_discharge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 45) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) bank_temp_storage[sensor_found].Lithium_max_discharge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 46) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].Lithium_min_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 47) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].Lithium_max_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 48) && (range_check(data_string, -819.2, 819.175, 0) <= 819.175)) bank_temp_storage[sensor_found].Lithium_max_discharge_current = range_check(data_string, -819.2, 819.175, 1);                            
                            if ((parameter_found == 49) && (range_check(data_string, -819.2, 819.175, 0) <= 819.175)) bank_temp_storage[sensor_found].Lithium_max_charge_current = range_check(data_string, -819.2, 819.175, 1);                            
                            if ((parameter_found == 50) && (range_check(data_string, 1, 100, 0) <= 100)) bank_temp_storage[sensor_found].Lithium_cycle_reset_SoC = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 51) && (range_check(data_string, 1, 100, 0) <= 100)) bank_temp_storage[sensor_found].Lithium_float_trip_accuracy = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 52) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].Lithium_safe_fallback_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 53) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].warning_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 54) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) bank_temp_storage[sensor_found].disconnect_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 55) && (range_check(data_string, 1, 100, 0) <= 100)) bank_temp_storage[sensor_found].warning_SoC = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 56) && (range_check(data_string, 1, 100, 0) <= 100)) bank_temp_storage[sensor_found].disconnect_SoC = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 57) && (range_check(data_string, 0, 1, 0) <= 1)) bank_temp_storage[sensor_found].use_pair_index = (unsigned char)range_check(data_string, 0, 1, 1);
                        }        
                        if (setting_change)
                        {             
                            return;
                        } else {
                            strcpy(buffer_string,"\nParameter not recognised\r\n");
                            PC_send_string(buffer_string, strlen(buffer_string));                              
                            return;
                        }
                    } else {
                        strcpy(buffer_string,"\nSensor parameter missing\r\n");
                        PC_send_string(buffer_string, strlen(buffer_string));                              
                        return;                        
                    }
                }   else {             
                    strcpy(buffer_string,"\nSensor parameter set incomplete. Please try again.\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                              
                    return;
                }
            }
        }
        if (strstr(data_string, "upload_bank_settings"))   
        {                  
            if (sensor_found != -1)
            {               
                if (bank_temp_storage[sensor_found].complete_parameter_set == bank_parameter_message_complete)
                {  
                    strcpy(buffer_string,"\nPlease wait...\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                              

                    timeout_seconds = 0;
                    timer_setup(1);  
                    do {
                        //send bank_temp_storage[sensor_found] to the TEM sensor]
                        if (receive_timer_flag)
                        {
                            timer_setup(1);                              
                            send_bank_sensor_parameters(bank_temp_storage[sensor_found], ID);
                            timeout_seconds++;
                        }
                        incoming_Argo_poll();                                           
                    } while ((bank_data[sensor_found].updated_parameter_status == 0) && (timeout_seconds < 30));
                    if (bank_data[sensor_found].updated_parameter_status == 0)
                    {
                        strcpy(buffer_string,"\nUpload failed. Please try again.\r\n");
                        PC_send_string(buffer_string, strlen(buffer_string));                                                      
                        return;
                    } else {
                        strcpy(buffer_string,"\nOK\r\n");
                        PC_send_string(buffer_string, strlen(buffer_string));                                         
                        bank_temp_storage[sensor_found].complete_parameter_set = 0;
                        bank_data[sensor_found].complete_parameter_set = 0;
                        return;
                    }
                }
            }
        }        
        strcpy(buffer_string,"\nSyntax error\r\n");
        PC_send_string(buffer_string, strlen(buffer_string));        
}

double range_check(char *value_string, double minimum_value, double maximum_value, unsigned char test_set)
{
    double extracted_value;
    char buffer_string[20];
    
        extracted_value = maximum_value + 1;
        if (strstr(value_string,"="))
        {
            strcpy(buffer_string,strstr(value_string,"=") + 1);            
            extracted_value = atof(buffer_string);
        }
        if ((extracted_value >= minimum_value) && (extracted_value <= maximum_value)) 
        {
            if (test_set == 0)
            {
                strcpy(buffer_string,"\nOK\r\n");
                PC_send_string(buffer_string, strlen(buffer_string));  
            }
            return extracted_value;
        }
        
        if ((test_set == 0) && ((extracted_value < minimum_value) || (extracted_value > maximum_value)))
        {
            strcpy(buffer_string,"\nInvalid value\r\n");
            PC_send_string(buffer_string, strlen(buffer_string));     
        }
        
        extracted_value = maximum_value + 1;
        return extracted_value;
}

void send_bank_sensor_parameters(battery_bank_parameters new_parameters, unsigned short int PGN_ID)
{
    double general_calculation;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } int_construct;

    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } int_construct_signed;
    
    unsigned char message_number, bank_device_class;
    
        if (new_parameters.device_class_set != new_parameters.old_device_class_set)
        {
            bank_device_class = new_parameters.old_device_class_set;
        } else {
            bank_device_class = new_parameters.device_class_set;
        }
        
        
        message_number = 0;         
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        int_construct.data_int = new_parameters.bank_Ah_capacity;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        new_parameters.charge_efficiency = new_parameters.charge_efficiency * 2;
        CAN_send_packet.CAN_byte[2] = (unsigned char)new_parameters.charge_efficiency;            
        new_parameters.discharge_efficiency = new_parameters.discharge_efficiency * 2;
        CAN_send_packet.CAN_byte[3] = (unsigned char)new_parameters.discharge_efficiency;

        CAN_send_packet.CAN_byte[4] = new_parameters.battery_chemistry;
        new_parameters.nominal_block_voltage = new_parameters.nominal_block_voltage * 500;
        int_construct.data_int = (unsigned short int)new_parameters.nominal_block_voltage;
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = int_construct.data_bytes[1];
        CAN_send_packet.CAN_byte[7] = new_parameters.rated_discharge_time;
        CAN_send(CAN_send_packet,Argo_CAN);
    
        message_number = 1;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        new_parameters.absorption_voltage = new_parameters.absorption_voltage * 500;
        int_construct.data_int = (unsigned short int)new_parameters.absorption_voltage;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        new_parameters.float_voltage = new_parameters.float_voltage * 500;
        int_construct.data_int = (unsigned short int)new_parameters.float_voltage;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        new_parameters.Peukerts_constant = new_parameters.Peukerts_constant * 33333.33;
        int_construct.data_int = (unsigned short int)new_parameters.Peukerts_constant;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];
        new_parameters.temperature_compensation = new_parameters.temperature_compensation * 66.67;
        int_construct.data_int = (unsigned short int)new_parameters.temperature_compensation;
        CAN_send_packet.CAN_byte[6] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 2;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        general_calculation = (double)new_parameters.min_SoC * 2.0;
        CAN_send_packet.CAN_byte[0] = (unsigned char)general_calculation;
        int_construct.data_int = new_parameters.lifetime_kWh;
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[1];
        CAN_send_packet.CAN_byte[3] = new_parameters.time_before_OCV_reset;
        CAN_send_packet.CAN_byte[4] = new_parameters.max_time_at_absorption;
        CAN_send_packet.CAN_byte[5] = new_parameters.max_bulk_current;
        general_calculation = new_parameters.full_charge_current * 1000;

        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct.data_bytes[1];

        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 3;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        new_parameters.min_datum_temperature = new_parameters.min_datum_temperature * 200;
        int_construct_signed.data_int = (signed short int)new_parameters.min_datum_temperature;
        CAN_send_packet.CAN_byte[0] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct_signed.data_bytes[1];
        new_parameters.max_datum_temperature = new_parameters.max_datum_temperature * 200;
        int_construct_signed.data_int = (signed short int)new_parameters.max_datum_temperature;
        CAN_send_packet.CAN_byte[2] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct_signed.data_bytes[1];
        general_calculation = new_parameters.OCV_current_limit * 6536;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];
        CAN_send_packet.CAN_byte[6] = new_parameters.pair_index;
        CAN_send_packet.CAN_byte[7] = new_parameters.number_of_blocks;     
        CAN_send(CAN_send_packet,Argo_CAN);

        
        message_number = 4;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  
            

        general_calculation = new_parameters.minimum_absorption_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct_signed.data_bytes[1];
        general_calculation = new_parameters.maximum_absorption_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct_signed.data_bytes[1];
        general_calculation = new_parameters.minimum_float_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct_signed.data_bytes[1];
        general_calculation = new_parameters.maximum_float_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct_signed.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);
        
        message_number = 5;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                  
      
        
        general_calculation = new_parameters.minimum_absorption_block_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        general_calculation = new_parameters.maximum_absorption_block_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = new_parameters.minimum_float_block_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];
        general_calculation = new_parameters.maximum_float_block_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct.data_bytes[1];        
        CAN_send(CAN_send_packet,Argo_CAN);
        
        message_number = 6;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                  
        general_calculation = new_parameters.centre_block_voltage_trigger * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        general_calculation = new_parameters.current_threshold_trigger * 1000;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        CAN_send_packet.CAN_byte[4] = new_parameters.sensor_name_index;
        CAN_send_packet.CAN_byte[5] = (unsigned char)new_parameters.current_polarity;
        CAN_send_packet.CAN_byte[6] = 0;//LVD suggested SoC
        CAN_send_packet.CAN_byte[7] = new_parameters.device_class_set;
        CAN_send(CAN_send_packet,Argo_CAN);      

        message_number = 7;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                  
        
        int_construct.data_int = (unsigned short int)new_parameters.battery_manufacturer;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        int_construct.data_int = (unsigned short int)new_parameters.battery_model;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_bulk_target_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_float_target_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct.data_bytes[1];        
        CAN_send(CAN_send_packet,Argo_CAN);        

        message_number = 8;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        general_calculation = (double)new_parameters.Lithium_min_charge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_charge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_min_discharge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_discharge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct_signed.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 9;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                  
        
        general_calculation = (double)new_parameters.Lithium_min_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_discharge_current * 40;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_charge_current * 40;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct_signed.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);
        
        message_number = 10;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        general_calculation = (double)new_parameters.Lithium_cycle_reset_SoC * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_float_trip_accuracy * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_safe_fallback_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.warning_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 11;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        general_calculation = (double)new_parameters.disconnect_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.warning_SoC * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.disconnect_SoC * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];        
        CAN_send_packet.CAN_byte[6] = (unsigned char)new_parameters.use_pair_index;
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet,Argo_CAN);

        
        message_number = 12;
        CAN_send_packet.CAN_PGN = PGN_ID + (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                         
        CAN_send_packet.CAN_byte[0] = (unsigned char)new_parameters.set_zero;
        CAN_send_packet.CAN_byte[1] = (unsigned char)new_parameters.reset_factory;
        CAN_send_packet.CAN_byte[2] = (unsigned char)new_parameters.reset_Ah;
        CAN_send_packet.CAN_byte[3] = (unsigned char)new_parameters.reset_kWh;
        CAN_send_packet.CAN_byte[4] = 0;
        CAN_send_packet.CAN_byte[5] = 0;
        CAN_send_packet.CAN_byte[6] = 0;
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet,Argo_CAN);           
        
}

void process_charger_commands(char *data_string, unsigned short int ID)
{
    unsigned char B2B_search, parameter_name, setting_change, timeout_seconds, charger_voltage;
    signed char parameter_found;
    signed char charger_found;
    char buffer_string[64];
    char buffer_string2[8];    
        charger_found = -1;
        for (B2B_search = 0; B2B_search < B2B_max; B2B_search++)
        {
            if ((B2B_data[B2B_search].ID_number != 0) && (B2B_data[B2B_search].ID_number == ID)) charger_found = B2B_search;
        }     
        if (charger_found == -1)
        {
            strcpy(buffer_string,"\nInvalid charger ID\r\n");
            PC_send_string(buffer_string, strlen(buffer_string));  
            return;
        }
        
        if (strstr(data_string, "read_charger_settings"))   
        {                  
            if (charger_found != -1)
            {               
                if (B2B_temp_storage[charger_found].complete_parameter_set == B2B_parameter_message_complete)
                {                                      
                    sprintf(buffer_string,"\n\r\nCharger 0x%04X parameters:\r\n",B2B_temp_storage[charger_found].ID_number);   
                    PC_send_string(buffer_string, strlen(buffer_string)); 
                    
                    for (parameter_name = 0; parameter_name < charger_parameter_strings; parameter_name++)
                    {
                        PC_send_string(B2B_parameter_string[parameter_name], strlen(B2B_parameter_string[parameter_name])); 
                        strcpy(buffer_string,"=0\r\n");
                        if (parameter_name == 0) sprintf(buffer_string,"=%d\r\n",B2B_temp_storage[charger_found].battery_manufacturer);  
                        if (parameter_name == 1) sprintf(buffer_string,"=%d\r\n",B2B_temp_storage[charger_found].battery_model);  
                        if (parameter_name == 2) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_bulk_target_voltage);  
                        if (parameter_name == 3) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_float_target_voltage);
                        if (parameter_name == 4) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_min_charge_temperature);  
                        if (parameter_name == 5) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_max_charge_temperature);  
                        if (parameter_name == 6) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_min_discharge_temperature);  
                        if (parameter_name == 7) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_max_discharge_temperature);  
                        if (parameter_name == 8) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_min_voltage);  
                        if (parameter_name == 9) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_max_voltage);  
                        if (parameter_name == 10) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_max_discharge_current);  
                        if (parameter_name == 11) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_max_charge_current);  
                        if (parameter_name == 12) sprintf(buffer_string,"=%d\r\n",(unsigned char)B2B_temp_storage[charger_found].Lithium_cycle_reset_SoC);  
                        if (parameter_name == 13) sprintf(buffer_string,"=%d\r\n",(unsigned char)B2B_temp_storage[charger_found].Lithium_float_trip_accuracy);  
                        if (parameter_name == 14) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].Lithium_safe_fallback_voltage);  
                        if (parameter_name == 15) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].fixed_charge_current);  
                        if (parameter_name == 16) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].fixed_charge_voltage);  
                        if (parameter_name == 17) sprintf(buffer_string,"=%d\r\n",B2B_temp_storage[charger_found].low_SoC);  
                        if (parameter_name == 18) sprintf(buffer_string,"=%d\r\n",B2B_temp_storage[charger_found].BMS_interface_to_use);  
                        if (parameter_name == 19) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].input_voltage_disconnect);  
                        if (parameter_name == 20) 
                        {
                            sprintf(buffer_string,"=%d, ",B2B_temp_storage[charger_found].name_index);    
                            charger_voltage = 12;
                            if (B2B_temp_storage[charger_found].CAN_device_class == B2B_master_24V_device_class) charger_voltage = 24;
                            if (B2B_temp_storage[charger_found].CAN_device_class == B2B_master_12V_device_class) charger_voltage = 12;
                            sprintf(buffer_string2,"%dV ",charger_voltage);                            
                            strcat(buffer_string, buffer_string2);
                            if (B2B_temp_storage[charger_found].name_index < number_of_names) strcat(buffer_string, device_names_string[B2B_temp_storage[charger_found].name_index]);
                            strcat(buffer_string,"\r\n");                            
                        }                                                           
                        if (parameter_name == 21) sprintf(buffer_string,"=%d\r\n",B2B_temp_storage[charger_found].operation_mode);
                        if (parameter_name == 22) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].default_safety_voltage);
                        if (parameter_name == 23) sprintf(buffer_string,"=%.02f\r\n",B2B_temp_storage[charger_found].maximum_chip_temperature);
                        PC_send_string(buffer_string, strlen(buffer_string));
                    }

                    
                    strcpy(buffer_string,"OK\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));               
                    return;
                } else {
                    strcpy(buffer_string,"\nCharger parameters incomplete. Please try again.\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                     
                    return;
                }
            }
        }
        
        if (strstr(data_string, "read_charger_data"))   
        {                  
            if (charger_found != -1)
            {               
                if (B2B_data[charger_found].complete_data_set == B2B_data_message_complete)
                {                                      
                    sprintf(buffer_string,"\n\r\nCharger 0x%04X data:\r\n",B2B_data[charger_found].ID_number);   
                    PC_send_string(buffer_string, strlen(buffer_string)); 
                    
                    for (parameter_name = 0; parameter_name < charger_data_strings; parameter_name++)
                    {
                        PC_send_string(B2B_data_string[parameter_name], strlen(B2B_data_string[parameter_name])); 
                    
                        if (parameter_name == 0) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].input_voltage);
                        if (parameter_name == 1) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].actual_output_voltage);
                        if (parameter_name == 2) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].highest_module_temperature);                    
                        if (parameter_name == 3) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].combined_output_current);
                        if (parameter_name == 4) sprintf(buffer_string,"=%d\r\n",B2B_data[charger_found].number_of_modules);                    
                        if (parameter_name == 5) sprintf(buffer_string,"=%d\r\n",B2B_data[charger_found].alarm);
                        if (parameter_name == 6) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].master_DCDC_output_voltage);                    
                        if (parameter_name == 7) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave1_DCDC_output_voltage);
                        if (parameter_name == 8) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave2_DCDC_output_voltage);
                        if (parameter_name == 9) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave3_DCDC_output_voltage);
                        if (parameter_name == 10) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave4_DCDC_output_voltage);
                        if (parameter_name == 11) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].master_max_chip_temperature);
                        if (parameter_name == 12) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave1_max_chip_temperature);
                        if (parameter_name == 13) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave2_max_chip_temperature);
                        if (parameter_name == 14) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave3_max_chip_temperature);
                        if (parameter_name == 15) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave4_max_chip_temperature);
                        if (parameter_name == 16) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].master_module_current);                    
                        if (parameter_name == 17) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave1_module_current);
                        if (parameter_name == 18) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave2_module_current);
                        if (parameter_name == 19) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave3_module_current);
                        if (parameter_name == 20) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].slave4_module_current);
                        if (parameter_name == 21) sprintf(buffer_string,"=%d\r\n",B2B_data[charger_found].firmware_version);
                        if (parameter_name == 22) sprintf(buffer_string,"=%d\r\n",B2B_data[charger_found].name_index);
                        if (parameter_name == 23) sprintf(buffer_string,"=%d\r\n",B2B_data[charger_found].fan_active);
                        if (parameter_name == 24) sprintf(buffer_string,"=%d\r\n",B2B_data[charger_found].cyclical_counter);
                        if (parameter_name == 25) sprintf(buffer_string,"=%.02f\r\n",B2B_data[charger_found].target_voltage);     

                        PC_send_string(buffer_string, strlen(buffer_string));
                    }

                    
                    strcpy(buffer_string,"OK\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));               
                    return;
                } else {
                    strcpy(buffer_string,"\nCharger data set incomplete. Please try again.\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                     
                    return;
                }
            }
        }
        if (strstr(data_string, "reset_charger_settings"))   
        {                  
            if (charger_found != -1)
            {               
                B2B_temp_storage[charger_found].complete_parameter_set = 0;
                strcpy(buffer_string,"\nOK\r\n");
                PC_send_string(buffer_string, strlen(buffer_string));               
                return;
            }
        }
        if (strstr(data_string, "write_charger_settings"))   
        {                  
            if (charger_found != -1)
            {               
                if (B2B_temp_storage[charger_found].complete_parameter_set == B2B_parameter_message_complete)
                {  
                    if (strstr(data_string,"="))
                    {
                        setting_change = 0;
                        for (parameter_name = 0; parameter_name < charger_parameter_strings; parameter_name++)
                        {
                            if (strstr(data_string, B2B_parameter_string[parameter_name])) 
                            {
                                parameter_found = parameter_name;
                                setting_change = 1;
                            } else {
                                parameter_found = -1;
                            }                                                                                                              
                            if ((parameter_found == 0) && (range_check(data_string, 0, 65535, 0) <= 65535)) B2B_temp_storage[charger_found].battery_manufacturer = (unsigned char)range_check(data_string, 0, 65535, 1);                                                                                                                
                            if ((parameter_found == 1) && (range_check(data_string, 0, 65535, 0) <= 65535)) B2B_temp_storage[charger_found].battery_model = (unsigned char)range_check(data_string, 0, 65535, 1);                                                                                                                
                            if ((parameter_found == 2) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].Lithium_bulk_target_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 3) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].Lithium_float_target_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 4) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) B2B_temp_storage[charger_found].Lithium_min_charge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 5) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) B2B_temp_storage[charger_found].Lithium_max_charge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 6) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) B2B_temp_storage[charger_found].Lithium_min_discharge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 7) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) B2B_temp_storage[charger_found].Lithium_max_discharge_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 8) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].Lithium_min_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 9) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].Lithium_max_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 10) && (range_check(data_string, -819.2, 819.175, 0) <= 819.175)) B2B_temp_storage[charger_found].Lithium_max_discharge_current = range_check(data_string, -819.2, 819.175, 1);                            
                            if ((parameter_found == 11) && (range_check(data_string, -819.2, 819.175, 0) <= 819.175)) B2B_temp_storage[charger_found].Lithium_max_charge_current = range_check(data_string, -819.2, 819.175, 1);                            
                            if ((parameter_found == 12) && (range_check(data_string, 1, 100, 0) <= 100)) B2B_temp_storage[charger_found].Lithium_cycle_reset_SoC = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 13) && (range_check(data_string, 1, 100, 0) <= 100)) B2B_temp_storage[charger_found].Lithium_float_trip_accuracy = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 14) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].Lithium_safe_fallback_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 15) && (range_check(data_string, -819.2, 819.175, 0) <= 819.175)) B2B_temp_storage[charger_found].fixed_charge_current = range_check(data_string, -819.2, 819.175, 1);                            
                            if ((parameter_found == 16) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].fixed_charge_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 17) && (range_check(data_string, 1, 100, 0) <= 100)) B2B_temp_storage[charger_found].low_SoC = (unsigned char)range_check(data_string, 1, 100, 1);
                            if ((parameter_found == 18) && (range_check(data_string, 0, 2, 0) <= 2)) B2B_temp_storage[charger_found].BMS_interface_to_use = (unsigned char)range_check(data_string, 0, 2, 1);
                            if ((parameter_found == 19) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].input_voltage_disconnect = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 20) && (range_check(data_string, 0, 255, 0) <= 255)) B2B_temp_storage[charger_found].name_index = (unsigned char)range_check(data_string, 0, 255, 1);                                                                                                                
                            if ((parameter_found == 21) && (range_check(data_string, 0, 2, 0) <= 2)) B2B_temp_storage[charger_found].operation_mode = (unsigned char)range_check(data_string, 0, 2, 1);
                            if ((parameter_found == 22) && (range_check(data_string, 1, 131.07, 0) <= 131.07)) B2B_temp_storage[charger_found].default_safety_voltage = range_check(data_string, 1, 131.07, 1);                            
                            if ((parameter_found == 23) && (range_check(data_string, -163.84, 163.835, 0) <= 163.835)) B2B_temp_storage[charger_found].maximum_chip_temperature = range_check(data_string, -163.84, 163.835, 1);                            
                            if ((parameter_found == 24) && (range_check(data_string, 0, 1, 0) <= 1)) B2B_temp_storage[charger_found].factory_reset = (unsigned char)range_check(data_string, 0, 1, 1);
                            if ((parameter_found == 25) && (range_check(data_string, 0, 1, 0) <= 1)) B2B_temp_storage[charger_found].stop_charging = (unsigned char)range_check(data_string, 0, 1, 1);
                            if ((parameter_found == 26) && (range_check(data_string, 0, 1, 0) <= 1)) B2B_temp_storage[charger_found].start_charging = (unsigned char)range_check(data_string, 0, 1, 1);
                            if ((parameter_found == 27) && (range_check(data_string, 0, 1, 0) <= 1)) B2B_temp_storage[charger_found].sleep_mode = (unsigned char)range_check(data_string, 0, 1, 1);
                    }        
                        if (setting_change)
                        {             
                            return;
                        } else {
                            strcpy(buffer_string,"\nParameter not recognised\r\n");
                            PC_send_string(buffer_string, strlen(buffer_string));                              
                            return;
                        }
                    } else {
                        strcpy(buffer_string,"\nCharger parameter missing\r\n");
                        PC_send_string(buffer_string, strlen(buffer_string));                              
                        return;                        
                    }
                }   else {             
                    strcpy(buffer_string,"\nCharger parameter set incomplete. Please try again.\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                              
                    return;
                }
            }
        }
        if (strstr(data_string, "upload_charger_settings"))   
        {                  
            if (charger_found != -1)
            {               
                if (B2B_temp_storage[charger_found].complete_parameter_set == B2B_parameter_message_complete)
                {  
                    strcpy(buffer_string,"\nPlease wait...\r\n");
                    PC_send_string(buffer_string, strlen(buffer_string));                              
                    if (charger_found == 0)
                    {

                        new_parameters = B2B_temp_storage[charger_found];
                        copy_new_parameters();
                        strcpy(buffer_string,"\nOK\r\n");
                        PC_send_string(buffer_string, strlen(buffer_string));                                         
                        B2B_temp_storage[charger_found].complete_parameter_set = 0;
                        B2B_data[charger_found].complete_parameter_set = 0;                        
                        return;
                    }
                    timeout_seconds = 0;
                    timer_setup(1);  
                    do {
                        //send B2B_temp_storage[charger_found] to the TEM sensor]
                        if (receive_timer_flag)
                        {
                            timer_setup(1);                              
                            send_charger_parameters(B2B_temp_storage[charger_found], ID);
                            timeout_seconds++;
                        }
                        incoming_Argo_poll();                                           
                    } while ((bank_data[charger_found].updated_parameter_status == 0) && (timeout_seconds < 30));
                    if (bank_data[charger_found].updated_parameter_status == 0)
                    {
                        strcpy(buffer_string,"\nUpload failed. Please try again.\r\n");
                        PC_send_string(buffer_string, strlen(buffer_string));                                                      
                        return;
                    } else {
                        strcpy(buffer_string,"\nOK\r\n");
                        PC_send_string(buffer_string, strlen(buffer_string));                                         
                        B2B_temp_storage[charger_found].complete_parameter_set = 0;
                        bank_data[charger_found].complete_parameter_set = 0;
                        return;
                    }
                }
            }
        }        
        strcpy(buffer_string,"\nSyntax error\r\n");
        PC_send_string(buffer_string, strlen(buffer_string));        
    
}

void send_charger_parameters(battery_to_battery_parameters new_parameters, unsigned short int PGN_ID)
{
    double general_calculation;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } int_construct;

    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } int_construct_signed;
    
    unsigned char message_number, device_class;
    
        
        device_class = new_parameters.CAN_device_class;


        message_number = 0;
        CAN_send_packet.CAN_PGN = PGN_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                  
        
        int_construct.data_int = (unsigned short int)new_parameters.battery_manufacturer;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        int_construct.data_int = (unsigned short int)new_parameters.battery_model;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_bulk_target_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_float_target_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct.data_bytes[1];        
        CAN_send(CAN_send_packet,Argo_CAN);        

        message_number = 1;
        CAN_send_packet.CAN_PGN = PGN_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        general_calculation = (double)new_parameters.Lithium_min_charge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_charge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_min_discharge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_discharge_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct_signed.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 2;
        CAN_send_packet.CAN_PGN = PGN_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                  
        
        general_calculation = (double)new_parameters.Lithium_min_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_discharge_current * 40;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct_signed.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_max_charge_current * 40;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct_signed.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);
        
        message_number = 3;
        CAN_send_packet.CAN_PGN = PGN_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        general_calculation = (double)new_parameters.Lithium_cycle_reset_SoC * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_float_trip_accuracy * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.Lithium_safe_fallback_voltage * 500;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];
        general_calculation = (double)new_parameters.fixed_charge_current * 40;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[6] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = int_construct_signed.data_bytes[1];
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 4;
        CAN_send_packet.CAN_PGN = PGN_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  

        general_calculation = (double)new_parameters.fixed_charge_voltage * 100;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];                
        CAN_send_packet.CAN_byte[2] = new_parameters.low_SoC;
        CAN_send_packet.CAN_byte[3] = new_parameters.BMS_interface_to_use;
        general_calculation = (double)new_parameters.input_voltage_disconnect * 100;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[4] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = int_construct.data_bytes[1];        
        CAN_send_packet.CAN_byte[6] = new_parameters.name_index;
        CAN_send_packet.CAN_byte[7] = new_parameters.operation_mode;
        CAN_send(CAN_send_packet,Argo_CAN);

        message_number = 5;
        CAN_send_packet.CAN_PGN = PGN_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  
        general_calculation = (double)new_parameters.default_safety_voltage * 100;
        int_construct.data_int = (unsigned short int)general_calculation;
        CAN_send_packet.CAN_byte[0] = int_construct.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = int_construct.data_bytes[1];                
        general_calculation = (double)new_parameters.maximum_chip_temperature * 200;
        int_construct_signed.data_int = (signed short int)general_calculation;
        CAN_send_packet.CAN_byte[2] = int_construct_signed.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = int_construct_signed.data_bytes[1];
        CAN_send_packet.CAN_byte[4] = 0;
        CAN_send_packet.CAN_byte[5] = 0;
        CAN_send_packet.CAN_byte[6] = 0;
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet,Argo_CAN);
        
        message_number = 6;
        CAN_send_packet.CAN_PGN = PGN_ID + (device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                         
        CAN_send_packet.CAN_byte[0] = (unsigned char)new_parameters.factory_reset;
        CAN_send_packet.CAN_byte[1] = (unsigned char)new_parameters.stop_charging;
        CAN_send_packet.CAN_byte[2] = (unsigned char)new_parameters.start_charging;
        CAN_send_packet.CAN_byte[3] = (unsigned char)new_parameters.sleep_mode;
        CAN_send_packet.CAN_byte[4] = 0;
        CAN_send_packet.CAN_byte[5] = 0;
        CAN_send_packet.CAN_byte[6] = 0;
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet,Argo_CAN);           
        
}

void extract_slave_parameters(CAN_data_packet PGN_parameter_extract, unsigned short int buffer_index)
{
    unsigned long message_number;
    union {
        unsigned short int data_short;
        unsigned char data_bytes[2];
    } int_unsigned;
    
        message_number = (PGN_parameter_extract.CAN_PGN & 0x7800000) >> 23;      
        if (message_number == 0)
        {        
            if (PGN_parameter_extract.CAN_byte[1]) factory_reset();
            B2B_local_data.stop_charging = PGN_parameter_extract.CAN_byte[2];
            B2B_local_data.sleep_mode = PGN_parameter_extract.CAN_byte[4];
            if (PGN_parameter_extract.CAN_byte[5])
            {
                FRAM_update_active(4,1);
                start_boot(boot_start);                
            }          
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[6];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[7];
            B2B_local_data.fixed_charge_voltage = (double)int_unsigned.data_short * 0.01;                        
            B2B_local_data.operation_mode = PGN_parameter_extract.CAN_byte[8];
        }          
}

void extract_slave_instant_parameters(CAN_data_packet PGN_parameter_extract, unsigned short int buffer_index)
{
    unsigned long message_number;
    union {
        unsigned short int data_short;
        unsigned char data_bytes[2];
    } int_unsigned;

    union {
        signed short int data_short;
        unsigned char data_bytes[2];
    } int_signed;
    
        message_number = (PGN_parameter_extract.CAN_PGN & 0x7800000) >> 23;        

        if (message_number == 0)
        {        
            int_unsigned.data_bytes[0] = PGN_parameter_extract.CAN_byte[1];
            int_unsigned.data_bytes[1] = PGN_parameter_extract.CAN_byte[2];
            B2B_local_data.fixed_charge_voltage = (double)int_unsigned.data_short * 0.01;
            int_signed.data_bytes[0] = PGN_parameter_extract.CAN_byte[3];
            int_signed.data_bytes[1] = PGN_parameter_extract.CAN_byte[4];            
            B2B_local_data.fixed_charge_current = (double)int_signed.data_short * 0.025;
            B2B_local_data.operation_mode = PGN_parameter_extract.CAN_byte[5];
            B2B_local_data.sleep_mode = PGN_parameter_extract.CAN_byte[6];
            B2B_local_data.stop_charging = PGN_parameter_extract.CAN_byte[7];
        }    
}

void send_test_serial(void)
{
    unsigned char parameter_name;
    char buffer_string[255];
    

        for (parameter_name = 0; parameter_name < 5; parameter_name++)
        {
            PC_send_string(B2B_data_string[parameter_name], strlen(B2B_data_string[parameter_name])); 

            if (parameter_name == 0) sprintf(buffer_string," = %.02f\r\n",B2B_local_data.input_voltage);
            if (parameter_name == 1) sprintf(buffer_string," = %.02f\r\n",B2B_local_data.actual_output_voltage);
            if (parameter_name == 2) sprintf(buffer_string," = %.02f\r\n",B2B_local_data.highest_module_temperature);                    
            if (parameter_name == 3) sprintf(buffer_string," = %.02f\r\n",B2B_local_data.combined_output_current);
            if (parameter_name == 4) sprintf(buffer_string," = %d\r\n",B2B_local_data.number_of_modules);                    

            PC_send_string(buffer_string, strlen(buffer_string));
        }
        sprintf(buffer_string,"DCDC output voltage = %.02f\r\n",B2B_local_data.master_DCDC_output_voltage);                    
        PC_send_string(buffer_string, strlen(buffer_string));    
        sprintf(buffer_string,"Temp 1 = %.02f\r\n",B2B_local_data.DCDC_temperature1);                    
        PC_send_string(buffer_string, strlen(buffer_string));
        sprintf(buffer_string,"Temp 2 = %.02f\r\n",B2B_local_data.DCDC_temperature2);                    
        PC_send_string(buffer_string, strlen(buffer_string));
        sprintf(buffer_string,"Temp 3 = %.02f\r\n",B2B_local_data.DCDC_temperature3);                    
        PC_send_string(buffer_string, strlen(buffer_string));
        sprintf(buffer_string,"Temp 4 = %.02f\r\n",B2B_local_data.DCDC_temperature4);                    
        PC_send_string(buffer_string, strlen(buffer_string));
        sprintf(buffer_string,"Temp 5 = %.02f\r\n",B2B_local_data.DCDC_temperature5);                    
        PC_send_string(buffer_string, strlen(buffer_string));
        sprintf(buffer_string,"Temp 6 = %.02f\r\n\r\n",B2B_local_data.DCDC_temperature6);                    
        PC_send_string(buffer_string, strlen(buffer_string));
    
}
void start_boot(uint16_t applicationAddress)
{
	asm("goto w0");
}


